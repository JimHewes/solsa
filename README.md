![Solsa1](Images/Solsa1.jpg)

Solsa is a sample management program for the John Bowen Solaris synthesizer.

The Solaris organizes samples into groups called sample pools. A sample pool is defined by a text file called a sample pool file. A sample pool file has a filename of the form "SamplePool-xxx.txt" where "xxx" represents a number such as "001", "002", "003" and so on. Solsa generates and manages these sample pools files.

## How To Use Solsa
In the upper left is a combo box where you can select a folder for your sample file and sample pool files. Start by selecting a folder where you already have sample pool files. When you do you'll see the sample pools appear in the tree view area on the left side, as in the picture above where it shows the pools for glockenspiel and harpsichord. Or, if you have no sample pools yet, select a folder where you want to generate the sample pool files. In this case the area to the left will still be empty.

### Creating a New Sample Pool
If you don't have any sample pools yet you can start by creating a new sample pool. To create a new sample pool you can either:

- Select Add from the Pool menu.
- Right-click on an empty spot in the tree view area on the left and select Pool Add from the context menu.
- Press Ctrl + A.<br>
This will create a new pool named "New Pool". You can then rename the pool by selecting it and then editing the name in the "Pool Name:" edit box in the info panel on the right side.

The sample pool file created will be given a name of the form "SamplePool-xxx.txt" where "xxx" will be the next available three digit number. (This filename cannot be edited within Solsa.)

After creating a new sample pool you can begin to add samples to the sample pool. 

### Removing a Sample Pool
Currently removing a sample pool will delete the sample pool file *and all the samples it references* to the recycle bin. *This may be changed in a future version to not remove the sample files, or at least not those that are referenced by other sample pools.*

To remove a sample pool you can either:

- Select a sample pool and then select Remove from the Pool menu.
- Right click on a sample pool and select Remove Pool from the sample menu.
- Select a sample pool and press Ctrl + R.

### Adding Samples to a Sample Pool
You can import samples that are in WAV file format. To add a sample to the pool first select the sample pool in the tree view area on the left. Then to add the sample you can either:

- Select Import from the Sample menu.
- Right-click on the sample pool in the tree view area to the left and select Sample Import from the context menu.
- Select the sample pool and press Ctrl + I.<br>
When you import the WAV file sample you'll be given the choice to convert the sample rate. You may want to convert the sample rate to 44,100 samples/second which is typical.

A copy of the sample file will be made and converted to raw format with a .raw filename extension. A raw file consists of just the sample data without any header information included.

### Removing Samples From a Sample Pool
To  remove a sample from a sample pool you can either:

- Select the sample and select Remove from the Sample menu.
- Right-click on the sample and select Remove Sample from the context menu.
- Select the sample and press Ctrl + M.

### Reordering Samples and Sample Pools

In the tree view area on the left you can reorder samples within a pool or move them between pools by selecting and dragging them.

You can also reorder sample pools by selecting and dragging them. This will cause the sample pool filenames to be renamed&mdash;their numbers reordered&mdash;so that they'll appear in the Solaris UI in that order.

### Saving Changes
There's no need to save the changes you make. The sample pool files are automatically updated as you make edits.

<br>

---------------------
### Editing Sample Information

To edit the information for a sample start by selecting the sample in the tree view on the left side.

You can edit information manually by using the edit boxes in the information panel to the right. This includes the raw filename which can be changed.

#### Root Key

The root key is indicated by the red key on the keyboard.

- Holding both Ctrl and Shift while clicking on the keyboard sets the root key.

#### Key Range

The key range is indicated by the blue rectangle on the keyboard.

- Holding Ctrl while clicking on the keyboard sets the low key of the key range.
- Holding Shift while clicking on the keyboard sets the high key of the key range.

#### Setting Loop Start and End

Aside from manually setting the loop start and end points in the edit boxes you can also set them using the waveform display.

- First define a *range* in the waveform display by holding Shift and then clicking and dragging in the waveform. The range is displayed by a light blue area in the waveform. (You can clear the range by holding Shift and clicking on the waveform without dragging.)
- With a range defined, select "Set Loop to Range" from the Edit menu or press Ctrl + L.

#### Playing the Sample

Before attempting to play a sample you'll need to set up the audio output device. You can do this by selecting Setup from the Audio menu. This brings up a dialog box allowing you to select your device. You can test that the audio is working by pressing the Test button. This plays a simple tone.

You can play the sample by pressing the Play button. You can set the start point of playback by clicking on the waveform. The start point is indicated by the vertical line in the display.

*Note that after playing the sample the start point is ***not*** reset to the beginning again. You must click at the start of the waveform again to reset the start point and play again. This may change in later versions to be more convenient.*

#### Waveform Display
- Holding Ctrl while using the mouse wheel zooms the waveform display.
- Holding Shift while using the mouse wheel scrolls the zooms the waveform display left or right.

## Building Solsa

Solsa is written in C++. Building the source code requires the JUCE library and also the free version of the Voxengo r8brain library v1.6. The current executable was built using Visual Studio 2022 v17.11.5 and JUCE v8.03. The only platform I've built and used this on is Windows. Although since JUCE is cross-platform it may not be difficult to build on other platforms as well.