/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#ifndef SAMPLEFOLDER_H_INCLUDED
#define SAMPLEFOLDER_H_INCLUDED
#include <vector>
#include <ostream>
#include "SamplePool.h"
#include "Notifier.h"

/**
@mainpage Overview

@ProgramName is a small utility for converting common audio format files into raw samples usable by the Solaris synthesizer.\n
\n
@ProgramName can import the following file types:   wav, aif, aiff, mp3, wmv, asf, wma, bwf, ogg, wm, flac\n
\n
@ProgramName can import mono or stereo files. Stereo files are split into two separate mono samples because the Solaris can
only use mono samples. (Audio files with more than two channels cannot be imported.)

*/


/** @brief	A class that represents a folder with one or more Solaris sample pool files.
			The SampleFolder class represent the model in a model-view paradigm.

	For more information on the @ProgramName model-view structure, see @ref DesignNotes "Design Notes".
*/
class SampleFolder final
{
public:

	SampleFolder(const std::string& folderPath);
	std::string GetFolderPath() const noexcept;
	void SetFolderPath(const std::string& folderPath);
	
	std::vector<PoolID> GetPoolIDs() const;
	std::string GetPoolName(PoolID poolID) const;
	void SetPoolName(PoolID poolID, const std::string& newPoolName);
	std::string GetPoolFilePath(PoolID poolID) const;
	uint16_t GetPoolIndex(PoolID poolID) const;
	void SetPoolIndex(PoolID poolID, uint16_t index);
	PoolID CreatePool(std::string poolName);
	void RemovePool(PoolID poolID);
	void MovePool(PoolID poolID, uint16_t newIndex);
	bool FixupPoolIndices();

	std::vector<SampleID> GetSampleIDs(PoolID poolID) const;
	SampleInfo GetSampleInfo(SampleID sampleID);
	void SetSampleInfo(SampleID sampleID, SampleInfo sampleInfo);
	SampleID CreateSample(PoolID poolID, const std::string& rawFilename, SampleInfo sampleInfo, bool overwriteRawFile = false, std::string restoreFilePath = "");
	bool RawFileIsReadable(SampleID);
	void RemoveSample(SampleID sampleID, bool removeRawFile = false);
	std::string GetSampleRawFilename(SampleID sampleID) const;
	void SetSampleRawFilename(SampleID sampleID, const std::string& newRawFilename);
	void MoveSample(SampleID sourceSampleID, PoolID destPoolID, uint16_t newIndex);
	bool SampleExists(const SampleID& sampleID) const;

	Notifier<void(SampleID)> SampleChangeEvent;
	using SampleChangeConnection = decltype(SampleChangeEvent)::ConnectionType;	///< SampleChangeEvent.Connect() returns this type
	Notifier<void(PoolID)> PoolChangeEvent;
	using PoolChangeConnection = decltype(PoolChangeEvent)::ConnectionType;		///< PoolChangeEvent.Connect() returns this type
	Notifier<void()> FolderChangeEvent;
	using FolderChangeConnection = decltype(FolderChangeEvent)::ConnectionType;	///< FolderChangeEvent.Connect() returns this type

private:
	void LoadPoolsFromDisk(const std::string& folderPath);
	SamplePool*	GetPoolFromID(PoolID poolID) const;
	bool PoolIDIsValid(PoolID poolID) const;
	uint16_t GetFirstAvailblePoolIndex();

	std::vector<std::unique_ptr < SamplePool >>	m_poolList;
	std::string	m_currentFolderPath;
};

using SampleFolderUPtr = std::unique_ptr < SampleFolder >;

#endif  // SAMPLEFOLDER_H_INCLUDED
