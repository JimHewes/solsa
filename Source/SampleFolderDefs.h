/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#ifndef SAMPLEFOLDERDEFS_H_INCLUDED
#define SAMPLEFOLDERDEFS_H_INCLUDED
#include <vector>
#include <string>
#include <cstdint>
#include "Utility.h"


struct SampleInfo
{
	//std::string	rawFilename;
	uint16_t	sampleIndex{ 0 };
	uint16_t	sampleRate{ 0 };
	uint32_t	sampleLength{ 0 };
	uint32_t	loopStart{ 0 };
	uint32_t	loopEnd{ 0 };
	uint8_t		rootKey{ 0 };
	uint8_t		fineTune{ 0 };
	uint8_t		lowKey{ 0 };
	uint8_t		highKey{ 0 };
};

inline bool operator==(const SampleInfo& lhs, const SampleInfo& rhs)
{
	return
		//lhs.rawFilename == rhs.rawFilename &&
		lhs.sampleIndex == rhs.sampleIndex &&
		lhs.sampleRate == rhs.sampleRate &&
		lhs.sampleLength == rhs.sampleLength &&
		lhs.loopStart == rhs.loopStart &&
		lhs.loopEnd == rhs.loopEnd &&
		lhs.rootKey == rhs.rootKey &&
		lhs.fineTune == rhs.fineTune &&
		lhs.lowKey == rhs.lowKey &&
		lhs.highKey == rhs.highKey;
}

inline bool operator!=(const SampleInfo& lhs, const SampleInfo& rhs)
{
	return !(lhs == rhs);
}

class SamplePool;
class Sample;

using PoolID = SamplePool*;
using SampID = Sample*;		///< SampID is the internal name name for sample ID that doesn't include the poolID
using SampleID = 
struct SampleIDStruct
{
public:
	PoolID poolID{ nullptr };
	SampID sampID{ nullptr };

	PoolID GetPoolID() const { return poolID; }

	std::string ToString()
	{
		return std::to_string(uintptr_t(poolID)) + ";" + std::to_string(uintptr_t(sampID));
	}
	void FromString(std::string str)
	{
		std::vector<std::string> list = util::SplitString(str, ";", true);
		if (list.size() == 2)
		{
			poolID = reinterpret_cast<PoolID>(std::stoull(list[0]));
			sampID = reinterpret_cast<SampID>(std::stoull(list[1]));
		}
	}
} ;


inline bool operator< (const SampleID& lhs, const SampleID& rhs) 
{
	return (lhs.poolID == rhs.poolID && lhs.sampID < rhs.sampID) || lhs.poolID < rhs.poolID;
}

inline bool operator==(const SampleID& lhs, const SampleID& rhs)
{
	return lhs.sampID == rhs.sampID && lhs.poolID == rhs.poolID;
}

inline bool operator!=(const SampleID& lhs, const SampleID& rhs)
{
	return !(lhs == rhs);
}

#endif  // SAMPLEFOLDERDEFS_H_INCLUDED
