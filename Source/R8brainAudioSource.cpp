/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#include <utility>
#include <memory>
#include <cassert>
#include "Utility.h"
#include "R8brainAudioSource.h"


#pragma warning(push)
#pragma warning(disable: 4373) // override only differs by const, volatile
#pragma warning(disable: 4127) // conditional expression is constant
#pragma warning(disable: 4100) // unreferenced parameter
#pragma warning(disable: 4701) // potentially uninitialized local variable used
#pragma warning(disable: 4703) // potentially uninitialized local pointer variable used
#pragma warning(disable: 4706) // assignment within conditional expression
#include "CDSPResampler.h"
#pragma warning(pop)

// Get rid of Windows definitions of min and max that came from CDSPResampler.h because they get confused with std::min and std::max.
#undef max
#undef min

R8brainAudioSource::R8brainAudioSource(AudioSource* const inputSource,
	const bool deleteInputWhenDeleted,
	const int numChannels)
	:m_pSource(inputSource,deleteInputWhenDeleted)
	,m_numChannels(numChannels)
{
	assert(m_pSource != nullptr);
}

R8brainAudioSource::~R8brainAudioSource()
{
	//pResampler.reset();
	m_sourceBuffer.setSize(m_numChannels,0);

	util::FreeVector(m_channels[0].unresampledBuffer);
	util::FreeVector(m_channels[0].resampledBuffer);
	m_channels[0].pResampler.reset();

}


void R8brainAudioSource::setResamplingRatio(double samplesOutPerInputSample)
{
	assert(samplesOutPerInputSample > 0);

	const SpinLock::ScopedLockType sl(m_ratioLock);
	m_ratio = std::max(0.0,samplesOutPerInputSample);
}


void R8brainAudioSource::setResamplingRatio(double inputSamplerate, double outputSamplerate)
{
	assert(inputSamplerate > 0);
	assert(outputSamplerate > 0);
	setResamplingRatio(outputSamplerate / inputSamplerate);
}


void R8brainAudioSource::prepareToPlay(int samplesPerBlockExpected,double sampleRate)
{
	const SpinLock::ScopedLockType sl(m_ratioLock);

	m_pSource->prepareToPlay(samplesPerBlockExpected,sampleRate);

	AllocateResources();

	m_lastRatio = m_ratio;
}

void R8brainAudioSource::AllocateResources()
{
	m_channels.resize(m_numChannels);

	for (size_t c = 0; c < m_channels.size(); c++)
	{
		// Create a new r8brain processor at the sample rate. 
		m_channels[c].pResampler = std::make_shared<r8b::CDSPResampler24>(1.0,m_ratio,DOUBLE_BUFFER_SIZE);

		m_channels[c].unresampledBuffer.resize(DOUBLE_BUFFER_SIZE);
		m_channels[c].resampledBuffer.resize(int(DOUBLE_BUFFER_SIZE * m_ratio + 0.5));	// This is just an estimate. The buffer can be resized later as needed.
	}

	m_sourceBuffer.setSize(m_numChannels,DOUBLE_BUFFER_SIZE,true,true);
	m_sourceBufferPointers.resize(m_numChannels);
	m_destBufferPointers.resize(m_numChannels);
}


void R8brainAudioSource::releaseResources()
{
	m_pSource->releaseResources();
	m_sourceBuffer.setSize(m_numChannels,0);
	util::FreeVector(m_channels);
}

/**

	There are two difficulties when using r8brain in the juce audio context.
	One, juce uses only float values for samples while r8brain uses only 
	double values. So we need extra copy steps to convert.
	Two, we don't know exactly how may samples will be ouput from the r8brain 
	resampling process. It doesn't tell us until after the processing is done.

*/
void R8brainAudioSource::getNextAudioBlock(const AudioSourceChannelInfo& info)
{

	double localRatio;

	{
		const SpinLock::ScopedLockType sl(m_ratioLock);
		localRatio = m_ratio;
	}

	int samplesNeededByCaller = info.numSamples;
	AudioSourceChannelInfo readInfo(m_sourceBuffer);	// Start with empty buffer.
	const int channelsToProcess = std::min(m_numChannels,info.buffer->getNumChannels());
	assert(channelsToProcess > 0);

	// Get the pointers to the caller's destination buffer that we're going to need for all channels.
	for (int channel = 0; channel < channelsToProcess; ++channel)
	{
		m_destBufferPointers[channel] = info.buffer->getWritePointer(channel,info.startSample);
	}

	while (samplesNeededByCaller > 0)
	{

		if (m_channels[0].resampledBufferSamplesLeft > 0)
		{
			// Copy samples from our resampled buffer to the caller's buffer, converting from double to float.
			// If samplesNeededByCaller goes to zero first, return.
			// If m_destBufferNumSamplesLeft goes to zero first, we're done and can continue on to process more.
			auto sampsNeeded = samplesNeededByCaller;	// temporary counter for number of samples the caller needs
			for (int channel = 0; channel < channelsToProcess; ++channel)
			{
				sampsNeeded = samplesNeededByCaller;
				while (m_channels[channel].resampledBufferSamplesLeft > 0 && sampsNeeded > 0)
				{
					assert(m_channels[channel].pResampledBuffer < &m_channels[channel].resampledBuffer[0] + m_channels[channel].resampledBuffer.size());
					*m_destBufferPointers[channel]++ = static_cast<float>(*m_channels[channel].pResampledBuffer++);

					m_channels[channel].resampledBufferSamplesLeft--;
					sampsNeeded--;
				}
			}
			samplesNeededByCaller = sampsNeeded;

			if (samplesNeededByCaller == 0)
			{
				continue;	// will exit outer while loop and then exit the function.
			}

		}

		// if we got this far, then the m_channels[].resampledBuffer should be empty.
		for (int channel = 0; channel < channelsToProcess; ++channel)
		{
			assert(m_channels[channel].resampledBufferSamplesLeft == 0);
		}

		// If the output sample rate changed, we need to reallocate the resampledBuffer for each channel to hold 
		// the different size.
		if (m_lastRatio != localRatio)
		{
			for (int channel = 0; channel < channelsToProcess; ++channel)
			{
				// Create a new r8brain processor at the new sample rate. It's OK to create it now now even if the 
				// rate was changed because we won't be calling on it until all the previous output data is gone anyway.
				m_channels[channel].pResampler = std::make_shared<r8b::CDSPResampler24>(1.0,localRatio,DOUBLE_BUFFER_SIZE);
				int maxOutLen = m_channels[channel].pResampler->getMaxOutLen(DOUBLE_BUFFER_SIZE);
				m_channels[channel].resampledBuffer.resize(maxOutLen);
			}
			m_lastRatio = localRatio;
		}


		// As far as I understand the juce documentation, this should always fill the buffer. (i.e. give 
		// us the exact amount of data we ask for.) If there isn't enough data left it will pad with zeroes.
		m_pSource->getNextAudioBlock(readInfo);

		// reset pointers to buffers
		for (int channel = 0; channel < channelsToProcess; channel++)
		{
			m_sourceBufferPointers[channel] = m_sourceBuffer.getReadPointer(channel);

			m_channels[channel].pUnresampledBuffer = &m_channels[channel].unresampledBuffer[0];
			m_channels[channel].pResampledBuffer = &m_channels[channel].resampledBuffer[0];
		}

		// Copy the float values we got from the source into the buffer for unresampled double values
		for (int channel = 0; channel < channelsToProcess; channel++)
		{
			for (int s = 0; s < m_sourceBuffer.getNumSamples(); s++)
			{
				assert(m_channels[channel].pUnresampledBuffer < &m_channels[channel].unresampledBuffer[0] + (m_channels[channel].unresampledBuffer.size()));
				*m_channels[channel].pUnresampledBuffer++ = *m_sourceBufferPointers[channel]++;
			}
		}

		// use r8brain to resample all channels
		for (int channel = 0; channel < channelsToProcess; channel++)
		{
			double* pOutDataDbl = nullptr;
			//m_channels[channel].pResampler->clear();

			int outputLength = m_channels[channel].pResampler->process(&m_channels[channel].unresampledBuffer[0], DOUBLE_BUFFER_SIZE,pOutDataDbl);
			assert(pOutDataDbl);

			if (outputLength > (int)m_channels[channel].resampledBuffer.size())
			{
				// To avoid	any reallocation in the middle of processing, make the initial allocation in AllocateResources() larger.
				m_channels[channel].resampledBuffer.resize(outputLength);
			}

			// The pOutDataDbl may or may not actually point to the input buffer m_channel[channel].unresampledBuffer.
			// So copy to a known safe place which is our m_channel[channel].resampledBuffer.
			for (int i = 0; i < outputLength; ++i)
			{
				m_channels[channel].resampledBuffer[i] = pOutDataDbl[i];
				m_channels[channel].resampledBufferSamplesLeft++;
			}
		}


		// Loop back up to copy more data into the caller's buffer as needed.
	}



}
