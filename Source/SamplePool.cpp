/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#include <string>
#include <algorithm> 
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cctype>
#include <cassert>
#include "SamplePool.h"
#include "Sample.h"
#include "../JuceLibraryCode/JuceHeader.h"

using std::string;
using std::endl;


inline void SamplePool::SortSampleList()
{
	std::sort(m_sampleList.begin(), m_sampleList.end(), [](SampleUPtr& p1, SampleUPtr& p2) { return p1->GetSampleIndex() < p2->GetSampleIndex(); });
}

/** Checks if the filename conforms to the name "SamplePool-???.txt" where '???' is a three-digit number.

This does not confirm that the file exists or is the valid format. It only checks the filename itself.
*/
bool VerifyPoolFilename(const std::string& filename)
{
	if (filename.length() != 18)
	{
		return false;
	}

	std::string beginning = filename.substr(0, 11);
	std::string middle = filename.substr(11, 3);
	std::string end = filename.substr(14, 4);

	if (beginning != "SamplePool-" || end != ".txt")
	{
		return false;
	}
	if (!isdigit(middle[0]) || !isdigit(middle[1]) || !isdigit(middle[2]) || middle == "000")
	{
		return false;
	}
	return true;
}

/** Constructor for a SamplePool used to construct an empty pool from scratch.

	@param[in] index		The intended index of the pool file. This value will be used to form the pool 
							filename, as in "SamplePool-001.txt". This number must be between 1 and 999 inclusive.
							It is up to the caller to ensure that this index is not the same as any other index in 
							a folder. If it is, am exception will occur because the pool file could not be created 
							with the same filename.
	@param[in] poolFolder	The full path to the pool folder in which to create the pool file. This folder must exist 
							and must be writable. If the pool file cannot be created in this folder an exception will occur.
	@param[in] poolName		The name of the pool. This is optional. If not supplied, the name will be "New Pool".
							The name can be changed later using the SetName() function.

	@throws std::runtime_exception	If the pool file cannot be created. This may be due to a conflict 
									in the index number or that the pool folder is not writable.
*/
SamplePool::SamplePool(uint16_t index, const std::string & poolFolder, const std::string & poolName)
	:m_index(index)
	, m_poolFolder(poolFolder)
	, m_name(poolName)
{

	File newFile(GetFilePath());
	if (newFile.existsAsFile())
	{
		throw std::runtime_error("A pool file with that name already exists so it can't be created.");
	}

	// Note that Save() may throw if the new pool file cannot be created.
	Save();
}


/** Constructor for a SamplePool used to construct a pool based on an existing pool file.

	@param[in]	filepath	The full path to the pool file. This file will be read and the SamplePool 
							based on the info found in this file. Samples will be created as needed.
*/
SamplePool::SamplePool(const std::string & filepath)
{
	SetFilePath(filepath);
	Load();
}

/** Renames the pool file by setting the index "xxx" in the filename "SamplePool-xxx.txt"
	
	@param[in] index	The new index used for the filename. This must be a number between 
						1 and 999 inclusive. The file with this index must not already exist.

	@throws std::runtime_error	If a file with that filename already exists in the current sample folder.
								Or, if the call to the platform's rename function fails.
*/
void SamplePool::SetPoolIndex(uint16_t index)
{
	assert(index > 0 && index < 1000);

	std::string sepChar(File::getSeparatorString().text);
	std::ostringstream newFilepath;
	newFilepath << m_poolFolder << sepChar << "SamplePool-" <<
		std::setw(3) << std::setfill('0') << (int)index << ".txt";
	File newFile(newFilepath.str());
	
	std::ostringstream oldFilepath;
	oldFilepath << m_poolFolder << sepChar << "SamplePool-" <<
		std::setw(3) << std::setfill('0') << (int)m_index << ".txt";
	File oldFile(oldFilepath.str());
	assert(oldFile.exists());

	if (newFile.exists())
	{
		std::ostringstream msg;
		msg << "Cannot rename the file " << oldFile.getFileName() << " to " << newFile.getFileName() << ". A file with that name already exists.";
		throw std::runtime_error("Cannot rename the file. A file with that name already exists.");
	}

	if (!oldFile.moveFileTo(newFile))
	{
		std::ostringstream msg;
		msg << "Failed to rename the file from " << oldFile.getFileName() << " to " << newFile.getFileName() << ".";
		throw std::runtime_error(msg.str());
	}

	m_index = index;
}

SampleID SamplePool::GetSampleID(size_t sampleIndex) 
{
	assert(sampleIndex < m_sampleList.size());

	SampleID result;
	result.poolID = GetPoolID();
	result.sampID = m_sampleList[sampleIndex]->GetSampID();
	return result;
}

std::vector<SampleID> SamplePool::GetSampleIDs() 
{
	std::vector<SampleID> sampleIDs;
	SampleID sampleID;
	sampleID.poolID = GetPoolID();
	for (auto& pSample : m_sampleList)
	{
		sampleID.sampID = pSample->GetSampID();
		sampleIDs.push_back(sampleID);
	}
	return sampleIDs;
}

void SamplePool::AddSample(SampleUPtr && pSample, uint16_t newIndex)
{
	if (pSample)
	{
		// Make room to move the sample.
		for (auto& pSamp : m_sampleList)
		{
			if (pSamp->GetSampleIndex() >= newIndex + 1)
			{
				pSamp->SetSampleIndex(pSamp->GetSampleIndex() + 1);
			}
		}
		pSample->SetSampleIndex(newIndex + 1);
		m_sampleList.push_back(std::move(pSample));
		FixupSampleIndices();
		Save();
	}
	return;
}

Sample*	SamplePool::GetSampleFromID(SampID sampID) const
{
	auto result = std::find_if(m_sampleList.begin(), m_sampleList.end(), [sampID](const SampleUPtr& upSample) { return sampID == upSample->GetSampID();});
	return (result != m_sampleList.end()) ? result->get() : nullptr;
}

SampleInfo SamplePool::GetSampleInfo(SampID sampID) const
{
	Sample* pSample = GetSampleFromID(sampID);
	assert(pSample);
	return pSample->GetInfo();
}

void SamplePool::SetSampleInfo(SampID sampID, SampleInfo sampleInfo)
{
	Sample* pSample = GetSampleFromID(sampID);
	assert(pSample);
	SampleInfo currentInfo = pSample->GetInfo();
	if (sampleInfo != currentInfo)
	{
		pSample->SetInfo(sampleInfo);
		Save();
	}
}

void SamplePool::SetSampleRawFilename(SampID sampID, const string& newRawFilename)
{
	Sample* pSample = GetSampleFromID(sampID);
	assert(pSample);
	string currentFilename = pSample->GetRawFilename();
	if (newRawFilename != currentFilename)
	{
		// rename raw file
		File poolFile(GetFilePath());
		string poolFilepath = poolFile.getParentDirectory().getFullPathName().toStdString();
		if (!poolFilepath.ends_with(File::getSeparatorString().text))
		{
			poolFilepath += File::getSeparatorString().text;
		}

		string newRawFilepath = poolFilepath + newRawFilename;
		File newRawFile(newRawFilepath);
		if (newRawFile.existsAsFile())
		{
			throw std::runtime_error("Cannot rename raw file. A file with that name already exists.");
		}

		string currentRawFilepath = poolFilepath + currentFilename;
		File currentRawFile(currentRawFilepath);

		if (!currentRawFile.moveFileTo(newRawFile))
		{
			throw std::runtime_error("Failed to rename raw file.");
		}

		pSample->SetRawFilename(newRawFile.getFileName().toStdString());
		Save();
	}

}

std::string SamplePool::GetSampleRawFilepath(SampID& sampID) const
{
	Sample* pSample = GetSampleFromID(sampID);
	assert(pSample);

	File poolFile(GetFilePath());
	if (!poolFile.existsAsFile())
	{
		return "";
	}

	std::string rawFilePath = poolFile.getParentDirectory().getFullPathName().toStdString();
	if (!rawFilePath.ends_with(File::getSeparatorString().text))
	{
		rawFilePath += File::getSeparatorString().text;
	}
	rawFilePath += pSample->GetRawFilename();
	return rawFilePath;
}

bool SamplePool::RawFileIsReadable(SampID sampID)
{
	assert(SampleIDIsValid(sampID));

	std::string rawFilePath = GetSampleRawFilepath(sampID);
	if (rawFilePath.empty())
	{
		return false;
	}

	std::ifstream m_rawFile;
	m_rawFile.open(rawFilePath, std::ios::in | std::ios::binary);
	if (!m_rawFile)
	{
		return false;
	}
	m_rawFile.close();
	return true;
}

std::string SamplePool::GetSampleRawFilename(SampID sampID) const
{
	Sample* pSample = GetSampleFromID(sampID);
	assert(pSample);
	return pSample->GetRawFilename();
}

bool SamplePool::SampleIDIsValid(SampID sampID)
{
	return GetSampleFromID(sampID) != nullptr;
}


/** Checks if the filename conforms to the name "SamplePool-???.txt" where '???' is a three-digit number.

	This does not confirm that the file exists or is the valid format. It only checks the filename itself.
*/
bool SamplePool::VerifyFilename(std::string filename)
{
	if (filename.length() != 18)
	{
		return false;
	}

	std::string beginning = filename.substr(0,11);
	std::string middle = filename.substr(11,3);
	std::string end = filename.substr(14,4);

	if (beginning != "SamplePool-" || end != ".txt")
	{
		return false;
	}
	if (!isdigit(middle[0]) || !isdigit(middle[1]) || !isdigit(middle[2]) || middle == "000")
	{
		return false;
	}
	return true;
}


void SamplePool::Load()
{
	File poolFile(GetFilePath());

	// Why not throw an exception if the pool file doesn't exist? Imagine the file DID exist when 
	// the directory was enumerated, but it got deleted between then and now. Do you want to 
	// throw up an error message to the user? It may be nicer to just not load anything, then 
	// the pool can still be used and saved if needed.
	// Another way to put it is, if the file doesn't exist then an empty SamplePool object 
	// is actually representing that state.
	assert(poolFile.existsAsFile());

	std::ifstream inFile;
	inFile.open(poolFile.getFullPathName().toStdString());
	if (!inFile)
	{
		assert(false);
		return;
	}

	bool wasModified = Load(inFile);
	inFile.close();

	// If the sample indices had to be fixed up, resave the pool file.
	if (wasModified)
	{
		Save();
	}
}

bool SamplePool::Load(std::istream& inStream)
{

	string poolFileDirectory = File(GetFilePath()).getParentDirectory().getFullPathName().toStdString();	// does not include trailing path separater
	m_sampleList.clear();

	std::string line;
	std::ostringstream sampleSettings;
	enum { ST_NONE,ST_POOL,ST_SAMPLE } state = ST_NONE;
	while (getline(inStream,line))
	{
		if (line.find("[Pool]") != std::string::npos)
		{
			// set state to expect the name.
			state = ST_POOL;
		}
		else if (line.find("[Sample]") != std::string::npos)
		{
			if (sampleSettings.str().length() > 0)
			{
				std::istringstream iss(sampleSettings.str());
				SampleUPtr pSample = std::make_unique<Sample>(iss);
				m_sampleList.push_back(std::move(pSample));
			}
			sampleSettings.str("");
			//sampleSettings.seekp(0, std::ios::beg);
			sampleSettings << line << endl;;
			state = ST_SAMPLE;
		}
		else
		{
			if (state == ST_POOL)
			{
				auto parts = util::SplitString(line,"=",true);
				if (parts.size() == 2)	// ignore blanks lines and any other line that doesn't have an equals sign in it.
				{
					// trim white space from left and right sides.
					if (util::Trim(parts[0]) == "name")
					{
						m_name = util::Trim(parts[1]);
					}
				}
			}
			else if (state == ST_SAMPLE)
			{
				sampleSettings << line << endl;;
			}
		}
	}

	if (sampleSettings.str().length() > 0)
	{
		std::istringstream iss(sampleSettings.str());
		SampleUPtr pSample = std::make_unique<Sample>(iss);
		m_sampleList.push_back(std::move(pSample));
	}

	// Make sure sample indices are right. Report if changes were made.
	return FixupSampleIndices();

}

/** Sorts the sample list according to sample index and then renumbers indexes if necessary.

	Note that sample indexes are not renumbered when the pool is saved. This is so that any 
	indexes held by the caller are not made invalid when saving the pool. When the calling code 
	is ready to do the final save, it should call this function to sort and renumber. Then it 
	should know that any indexes held by the caller may be invalid.\n
	I'm not sure it would help to just call this function from the SamplePool destructor. Calling 
	code might expect that sample indexed don't change if it just saves and reloads the pool.\n

	@returns true if any sample indices were changed, false otherwise.
*/
bool SamplePool::FixupSampleIndices()
{
	// Make sure sample indices are right
	bool modified = false;
	if (!m_sampleList.empty())
	{
		// Sort samples according to original sample index.
		//m_sampleList.sort([](SampleUPtr& p1,SampleUPtr& p2){ return p1->GetSampleIndex() < p2->GetSampleIndex(); });
		sort(m_sampleList.begin(), m_sampleList.end(), [](SampleUPtr& p1, SampleUPtr& p2) { return p1->GetSampleIndex() < p2->GetSampleIndex(); });

		// Then rewrite indexes to make sure they are consecutive and start at 1.
		int index = 1;
		for (auto& pSample : m_sampleList)
		{
			if (pSample->GetSampleIndex() != index)
			{
				pSample->SetSampleIndex(index);
				modified = true;
			}
			index++;
		}
	}
	return modified;
}

void SamplePool::Save()
{
	std::ofstream os;
	os.open(GetFilePath(),std::ios::out | std::ios::trunc);
	if (!os)
	{
		throw std::runtime_error("The pool file cannot be opened for writing.");
	}

	// Make sure sample indices are contiguous
	//FixupSampleIndices();

	// Sort samples according to original sample index.
	Save(os);

}

void SamplePool::Save(std::ostream& outStream)
{
	// Make sure sample indices are contiguous
	//FixupSampleIndices();

	// Sort samples according to original sample index.
	SortSampleList();

	outStream << "[Pool]" << endl;
	outStream << "name = " << m_name << endl;

	for (auto& pSample : m_sampleList)
	{
		pSample->Save(outStream);
	}
}


void SamplePool::SetFilePath(std::string poolFilePath)
{
	File f(poolFilePath);
	if (!f.getParentDirectory().exists() || !VerifyFilename(f.getFileName().toStdString()))
	{
		throw std::invalid_argument("Bad path in SamplePool::SetFilePath()");
	}
	m_poolFolder = f.getParentDirectory().getFullPathName().toStdString();

	// Get the index from the filename
	m_index = static_cast<decltype(m_index)>(f.getFileName().substring(11, 11 + 3).getIntValue());

}

/** Returns the full path of the pool file.

	Note that the pool filename is contructed from the index each time. 
	If the filename was kept separately we'd have to worry about them becoming unsynchronized.

	@returns The full path of the pool file.
*/
string SamplePool::GetFilePath() const
{
	std::ostringstream os;
	std::string sepChar(File::getSeparatorString().text);
	os << m_poolFolder << sepChar << "SamplePool-" <<
		std::setw(3) << std::setfill('0') << (int)m_index << ".txt";

	return os.str();
}

void SamplePool::SetName(std::string name)
{
	m_name = name;
	Save();
}

std::string SamplePool::GetName() const
{
	return m_name;
}

#if 0
int SamplePool::GetNumberOfSamples() const
{
	return m_sampleList.size();
}

/** Gets a native pointer to the Sample that has the given sample index

	This index is not the sampleIndex as specified in the pool file. Rather it's the index
	into the list of Samples that the SamplePool class has internally. This is useful for
	iterating over all the Samples in the SamplePool. First call GetNumberOfSamples()
	to get the number of samples in the SamplePool.

	@param[in] index	The index into the list of Samples in the SamplePool.
	@returns	A native pointer to the Sample, or nullptr if no sample has the given sample index.
	@see GetSampleFromSampleIndex, GetNumberOfSamples
*/
Sample* SamplePool::GetSample(int index) const
{
	int i = 0;
	for (auto iter = m_sampleList.begin(); iter != m_sampleList.end() && i <= index; iter++, i++)
	{
		if (i == index)
		{
			return iter->get();
		}
	}
	return nullptr;
}

/** Gets a native pointer to the Sample that has the given sample index

	This index is not the index to the place in the list of samples. It's the sample index
	as specified in the pool file. These are not necessarily in any order or consecutive.

	@param[in] sampleIndex	The sample index for the sample to get.
	@returns	A native pointer to the Sample, or nullptr if no sample has the given sample index.
	@see GetSample
*/
Sample* SamplePool::GetSampleFromSampleIndex(int sampleIndex) const
{
	for (auto& pSample : m_sampleList)
	{
		if (pSample->GetSampleIndex() == sampleIndex)
		{
			return pSample.get();
		}
	}
	return nullptr;

}

#endif // 0


int SamplePool::GetHighestSampleIndex()
{
	int highestIndex = 0;

	for (auto& sample : m_sampleList)
	{
		highestIndex = std::max(highestIndex,sample->GetSampleIndex());
	}
	return highestIndex;
}

/** Generates a new unique filename of the form "newrawfile-???.raw" in the folder specified where ??? is the next highest decimal number available.

	@returns	The name of the unique file. This does NOT include the full folder path, just the filename.
	@throws		runtime_error if a unique filename cannot be generated because there are already 1000 such files (not likely).
*/
std::string GenerateUniqueRawFilename(const string& folderPath)
{
	std::ostringstream os;
	File rawFile;
	int index = 1;
	std::string sepChar(File::getSeparatorString().text);
	do
	{
		os.str("");
		os << folderPath << sepChar << "newrawfile-" <<
			std::setw(4) << std::setfill('0') << index++ << ".raw";
		rawFile = File(os.str());
	} while (rawFile.existsAsFile() && index < 1000);

	if (index >= 1000)
	{
		throw std::runtime_error("Failed to generate a unique raw filename.");
	}
	return rawFile.getFileName().toStdString();
}



/* Creates a new Sample in the sample pool.

	If the raw file named in the sampleInfo structure already exists, the existing file is used.
	If the raw file parameter in the sampleInfo structure is empty, a new unique filename is
	generated and a new raw file is created with that filename. That filename can later be discovered
	using the GetSampleInfo() function.

	@param[in] rawFilename	The preferred raw filename to use. This is not the full path but just the filename 
							including extension. This may be the empty string in which case a unique filename 
							is generated. If you do specify a filename, then the useExistingRawFile parameter 
							idicates whether you want to use this file.
	@param[in] sampleInfo	The sample parameters used to initialize the new sample. The values 
							sampleInfo do not need to be all valid---they can be set defaults and then 
							set later using SetSampleInfo().
							The sampleInfo.sampleIndex member should contain the preferred index, meaning that 
							the new sample will be inserted into a position according to this and all 
							existing samples with indexes higher than this will be incremented by one.
							To put a sample at the end, set sampleInfo.sampleIndex to UINT16_MAX.
	@param[in] useExistingRawFile	If a raw filename is specified in the rawFilename parameter, then useExistingRawFile 
									indicates whether you want to use the existing raw file with the same name.
									The default is false.
	@param[in] restoreFilePath	This is used by undo commands that have deleted a Sample and the accompanying 
								raw file and the raw file was saved to a temp folder. The CreateSample()
								function can be used to restore the original sample by naming the path to 
								the file in the temp folder. This file will then be moved back and given its 
								original filename which is given in the sampleInfo structure.
								But if you're just creating a new sample and not restoring a deleted one, 
								then leave this at the default which is an empty string.
	@returns	The sampleID of the new sample that was created.
	@throws		std::runtime_exception if useExistingRawFile is false and a raw file exists with the name specified in the rawFilename parameter.\n
				std::runtime_exception if a new raw file could not be created (i.e., open() failed).\n
*/
SampleID SamplePool::CreateSample(string rawFilename, SampleInfo sampleInfo, bool useExistingRawFile, string restoreFilePath)
{
	File poolFile(GetFilePath());

	string poolFolder = poolFile.getParentDirectory().getFullPathName().toStdString();
	if (!poolFolder.ends_with(File::getSeparatorString().text))
	{
		poolFolder += File::getSeparatorString().text;
	}
	
	string samplePathname = poolFolder;
	if (rawFilename.empty())
	{
		// generate unique filename
		rawFilename = GenerateUniqueRawFilename(poolFolder);
	}
	samplePathname += rawFilename;

	bool rawFileExists = File(samplePathname).existsAsFile();

	if (rawFileExists && !useExistingRawFile)
	{
		throw std::runtime_error("A raw file with that name already exists and overwrite is not specified.");
	}

	if (!rawFileExists)
	{
		std::ofstream rawFile;
		rawFile.open(samplePathname, std::ios::out | std::ios::binary);
		if (!rawFile)
		{
			assert(false);
			throw std::runtime_error("Raw file could not be opened.");
		}
		rawFile.close();
	}
	auto highestSampleIndex = static_cast<uint16_t>(1 + GetHighestSampleIndex());
	assert(highestSampleIndex != UINT16_MAX);	// never want to get as many as 64K samples
	if (sampleInfo.sampleIndex == UINT16_MAX || sampleInfo.sampleIndex > highestSampleIndex)
	{
		sampleInfo.sampleIndex = highestSampleIndex;
	}
	else
	{
		for (auto &s : m_sampleList)
		{
			SampleInfo si = s->GetInfo();
			if (si.sampleIndex >= sampleInfo.sampleIndex)
			{
				si.sampleIndex++;
				s->SetInfo(si);
			}
		}
	}
	SampleUPtr pSample = std::make_unique<Sample>(rawFilename, sampleInfo, useExistingRawFile, restoreFilePath);
	SampleID result;
	result.sampID = pSample->GetSampID();
	result.poolID = GetPoolID();
	m_sampleList.push_back(std::move(pSample));
	SortSampleList();
	Save();
	return result;
}

/** Removes a sample from the pool and optionally deletes (to trash) the associated raw file from disk.

	The rawFile might not get deleted if the deletion failed for some reason. In that case, the function 
	still returns true if the sample was deleted from the pool file.

	@returns	A std::unique_ptr to the sample object that was removed. This may be useful for inserting 
				into a different pool (moving).
				The pointer is null if the sample was not removed because the sampleID was not found in this pool.
*/
SampleUPtr SamplePool::RemoveSample(SampID sampID, bool removeRawFile)
{
	bool result = false;
	SampleUPtr retVal;

	auto pos = find_if(m_sampleList.begin(), m_sampleList.end(), [sampID](SampleUPtr& pSample) { return sampID == pSample->GetSampID();});
	if (pos != m_sampleList.end())
	{
		if (removeRawFile)
		{
			File rawFile(GetSampleRawFilepath(sampID));
			rawFile.moveToTrash();
		}

		retVal.swap(*pos);	// take ownership of the sample

		// only erase after removing raw file so we can get the raw filename from the sample.
		m_sampleList.erase(pos);	// remove empty UPtr
		FixupSampleIndices();
		Save();
		result = true;
	}
	else
	{
		assert(false);
	}

	return retVal;
}


/** @brief Effectively moves a sample's order within a pool by changing the sample index.

	@param[in] sampleID		The ID of the sample whose index to change. This is the index as
							seen in the pool file.
	@param[in] newIndex		The index to change to. This is not the ID but is the zero-based
							index of the samples in the pool. Since the samples are always
							kept in sequential order, this should always be one less than the
							real sample index in the pool file, which starts at 1.
*/
void SamplePool::ChangeSampleIndex(SampID sampleID, uint16_t newIndex)
{

	Sample* pSrcSample = GetSampleFromID(sampleID);

	// Make room to move the sample.
	for (auto& pSample : m_sampleList)
	{
		if (pSample->GetSampleIndex() >= newIndex + 1)
		{
			pSample->SetSampleIndex(pSample->GetSampleIndex() + 1);
		}
	}
	pSrcSample->SetSampleIndex(newIndex + 1);
	FixupSampleIndices();
	Save();

	// We want to create a new sample before we remove the old one because the new index is 
	// based on the old sample still being there. If we remove it first, then the new position 
	// would be wrong.
//	CreateSample(filename, info, true);
//	RemoveSample(sampleID, false);

}

