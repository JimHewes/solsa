/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#include "PoolTreePanel.h"
#include <string>
#include <list>
#include <algorithm>
#include <cassert>

using std::string;

static std::list<std::string> LoadRecentFoldersList(PropertiesFile* pPropertiesFile);
static void SaveRecentFoldersList(PropertiesFile* pPropertiesFile, const std::list<std::string>& list);
static void AddRecentFolderToProperties(PropertiesFile* pPropertiesFile, const std::string& folder);


PoolTreePanel::PoolTreePanel(SampleFolder* pSampleFolder, PropertiesFile* pPropertiesFile, ViewModel* pViewModel)
	:m_pSampleFolder(pSampleFolder)
	,m_pPropertiesFile(pPropertiesFile)
{
	assert(pPropertiesFile);

	m_browseButton.setButtonText("...");
	m_browseButton.addListener(this);
	addAndMakeVisible(m_browseButton);

	m_pathView.setEditableText(false);
	addAndMakeVisible(m_pathView);
	m_pathView.addListener(this);

	m_pPoolTreeView = std::make_unique<PoolTreeView>(m_pSampleFolder, pViewModel);
	addAndMakeVisible(m_pPoolTreeView.get());

	m_folderChangeConnection = m_pSampleFolder->FolderChangeEvent.Connect([this]() { OnSampleFolderChanged(); });
	OnSampleFolderChanged();	// update the text of the m_pathView combo according to the SampleFolder data model.
	RefreshPathViewHistory();	// update the dropdown items of the m_pathView combo according to the history saved in the properties file.

	resized();
}

PoolTreePanel::~PoolTreePanel()
{
	m_pathView.removeListener(this);
	m_pPoolTreeView.reset();
}

void PoolTreePanel::resized()
{
	auto localBounds = getLocalBounds();
	auto topStripBounds = localBounds.removeFromTop(30).reduced(4);
	auto buttonBounds = topStripBounds.removeFromRight(40);
	buttonBounds.reduce(4,0);

	m_browseButton.setBounds(buttonBounds);
	m_pathView.setBounds(topStripBounds);
	m_pPoolTreeView->setBounds(localBounds);
}

void PoolTreePanel::ChoosePoolFolder()
{
	File lastDirectory(m_pPropertiesFile->getValue("pooldirectory"));
	
	FileChooser fc("Choose a folder...",
		lastDirectory,
		"*.solsa",
		true);

	if (fc.browseForDirectory())
	{
		File folderChosen = fc.getResult();
		String folderPathChosen = folderChosen.getFullPathName();

		m_pPropertiesFile->setValue("pooldirectory",folderPathChosen);
		//m_pathView.setText(folderPathChosen);
		//m_pathView.setTooltip(folderPathChosen);

		AddRecentFolderToProperties(m_pPropertiesFile, folderPathChosen.toStdString());
		RefreshPathViewHistory();
		m_pSampleFolder->SetFolderPath(folderPathChosen.toStdString());

		//FolderPathChangeEvent.Notify(folderPathChosen.toStdString());
	}
}

void PoolTreePanel::RefreshPathViewHistory()
{
	// clear() will also clear the text, not just the history items. So save and replace the text.
	auto saveText = m_pathView.getText();
	m_pathView.clear(juce::NotificationType::dontSendNotification);
	m_pathView.setText(saveText, juce::NotificationType::dontSendNotification);

	int id = 1;
	std::list<std::string> list = LoadRecentFoldersList(m_pPropertiesFile);
	for (auto& s : list)
	{
		m_pathView.addItem(s, id++);
	}
}

void PoolTreePanel::comboBoxChanged(ComboBox* comboBoxThatHasChanged)
{
	if (comboBoxThatHasChanged == &m_pathView)
	{
		auto folderPathChosen = m_pathView.getText();
		m_pPropertiesFile->setValue("pooldirectory", folderPathChosen);

		// Setting the FolderPath will trigger a notification that will call OnSampleFolderChanged().
		// But since JUCE already sets the text when the user selects from the history dropdown, 
		// OnSampleFolderChanged() won't have anything to do.
		m_pSampleFolder->SetFolderPath(folderPathChosen.toStdString());
	}
}

void PoolTreePanel::buttonClicked(Button* button)
{
	if (button == &m_browseButton)
	{
		ChoosePoolFolder();
	}
}

/** @brief OnSampleFolderChanged is a callback that is called when the Sample Folder in the data model changes.

	OnSampleFolderChanged is called when the user selects from the history dropdown OR when they use the browse 
	button to select a new sample folder. In the first case, the combobox was already set by JUCE so there isn't 
	anything to do here. But in the second case this function does need to set the text.

*/
void PoolTreePanel::OnSampleFolderChanged()
{
	string folderPath = m_pSampleFolder->GetFolderPath();
	auto oldFolderPath = m_pathView.getText();
	if (folderPath != oldFolderPath)
	{
		m_pathView.setText(folderPath, juce::NotificationType::dontSendNotification);
		m_pathView.setTooltip(folderPath);
	}
}


const juce::String RecentFoldersKey = "RecentsFoldersKey";
const juce::String RecentFolderListElementName = "RecentsFolders";
const juce::String RecentFolderElementName = "Folder";
std::list<std::string> LoadRecentFoldersList(PropertiesFile* pPropertiesFile)
{
	std::list<std::string> list;

	auto elem = std::unique_ptr<juce::XmlElement>(pPropertiesFile->getXmlValue(RecentFoldersKey));
	if (elem)
	{
		assert(elem->getTagName() == RecentFolderListElementName);
		if (elem->getTagName() == RecentFolderListElementName)
		{
			auto folderElem = elem->getFirstChildElement();
			assert(folderElem->getTagName() == RecentFolderElementName);

			while (folderElem != nullptr)
			{
				auto textElem = folderElem->getFirstChildElement();
				list.push_back(textElem->getText().toStdString());
				folderElem = folderElem->getNextElement();
			}
		}
	}
	return list;
}

void SaveRecentFoldersList(PropertiesFile* pPropertiesFile, const std::list<std::string>& list)
{
	// create an outer node
	XmlElement elem(RecentFolderListElementName);

	for (auto& s : list)
	{
		// create a child element.
		XmlElement* child = new XmlElement(RecentFolderElementName);
		child->addTextElement(s);

		// ..and add new child element to the parent
		elem.addChildElement(child);
	}
	//String str = elem.toString();
	//DBG(str);
	pPropertiesFile->setValue(RecentFoldersKey, &elem);
	pPropertiesFile->save();

}

void AddRecentFolderToProperties(PropertiesFile* pPropertiesFile, const std::string& folder)
{
	std::list<std::string> list = LoadRecentFoldersList(pPropertiesFile);

	auto pos = std::find(list.begin(), list.end(), folder);
	if (pos != list.end())	// if the folder path is already in the recent folders list...
	{
		// ... remove it from its current position.
		list.erase(pos);
	}

	// Add the folder path at the top.
	list.push_front(folder);

	SaveRecentFoldersList(pPropertiesFile, list);
}



