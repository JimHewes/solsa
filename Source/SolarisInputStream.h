/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#ifndef SOLARISINPUTSTREAM_H_INCLUDED
#define SOLARISINPUTSTREAM_H_INCLUDED

#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include "SampleFolder.h"
#include "../JuceLibraryCode/JuceHeader.h"

using HeaderBuffer = std::vector<uint32_t>;

/**	An input stream that reads a sample from a Solaris pool file and one associated raw sample file.

	@see InputStream
*/
class JUCE_API  SolarisInputStream : public InputStream
{
public:
	//==============================================================================
	/** Creates a SolarisInputStream to read from the given file.

		After creating a SolarisInputStream, you should use openedOk() or failedToOpen()
		to make sure that it's OK before trying to read from it! If it failed, you
		can call getStatus() to get more error information.\n
		\n
		The SolarisInputStream will first read the text information from the specified pool file 
		for the given sample index. Then immedately following that it will read the audio data 
		from the .raw file of the specified sampleID. To the user of the SolarisInputStream it will 
		seem to be one file.\n

		@param[in] pSampleFolder	A pointer to the SampleFolder model.
		@param[in] sampleID			The ID of the sample to associate with the input stream.
	*/
	explicit SolarisInputStream(SampleFolder* pSampleFolder, SampleID sampleID);

	/** Returns the status of the stream.
		The result will be ok if the file opened successfully. If an error occurs while
		opening or reading from the file, this will contain an error message.
	*/
	const Result& getStatus() const noexcept{ return m_status; }

	/** Returns true if the stream couldn't be opened for some reason.
		@see getResult()
	*/
	bool failedToOpen() const noexcept{ return m_status.failed(); }

	/** Returns true if the stream opened without problems.
		@see getResult()
	*/
	bool openedOk() const noexcept{ return m_status.wasOk(); }

	//==============================================================================
	/** Returns the total number of bytes available for reading in this stream.

		Note that this is the number of bytes available from the start of the
		stream, not from the current position.

		If the size of the stream isn't actually known, this will return -1.

		@see InputStream::getNumBytesRemaining
	*/
	int64 getTotalLength() override;

	/** Returns true if the stream has no more data to read. */
	bool isExhausted() override;

	/** Returns the offset of the next byte that will be read from the stream.
		@see setPosition
	*/
	int64 getPosition() override;

	/** Tries to move the current read position of the stream.

		The position is an absolute number of bytes from the stream's start.

		Some streams might not be able to do this, in which case they should do
		nothing and return false. Others might be able to manage it by resetting
		themselves and skipping to the correct position, although this is
		obviously a bit slow.

		@returns  true if the stream manages to reposition itself correctly
		@see getPosition
	*/
	bool setPosition(int64 newPosition) override;

	/** Reads some data from the stream into a memory buffer.

		@param destBuffer       the destination buffer for the data. This must not be null.
		@param maxBytesToRead   the maximum number of bytes to read - make sure the
								memory block passed in is big enough to contain this
								many bytes. This value must not be negative.

		@returns    the actual number of bytes that were read, which may be less than
					maxBytesToRead if the stream is exhausted before it gets that far
	*/
	int read(void* destBuffer,int maxBytesToRead) override;


private:
	int GetHeaderSize();

	Result				m_status;
	//std::stringstream	m_header;
	HeaderBuffer		m_header;
	//std::string		m_rawFilename;
	std::ifstream		m_rawFile;
	//int64				m_totalLength{ -1 };
	int64				m_currentPosition{ 0 };	// next byte to read
};


//==============================================================================
/**
	An output stream that writes a sample from a Solaris pool file and one associated raw sample file.

	@see OutputStream
*/
class JUCE_API  SolarisOutputStream : public OutputStream
{
public:
	//==============================================================================
	/** Creates a SolarisOutputStream.

		If the pool file doesn't exist, it will first be created. The pool will be given the
		name supplied in the poolName parameter. If the pool file can't be created or opened,
		the failedToOpen() method will return true.\n
		\n
		If the pool file already exists, poolName parameter will be ignored. If the sample 
		index doesn't exist, then a new sample section will be created using the raw sample 
		filename provided. Otherwise the raw sample filename is ignored. (There's no 
		provision by way of the SolarisOutputStream for _changing_ the raw filename of an 
		existing sample.)
		\n
		In any case, if a raw file exists will be overwritten.\n
		\n
		The section of the stream that represents the sample metadata is called the header.
		The header length is always assumed to be 300 bytes in length as far as file position
		goes. The SolarisOutputStream and SolarisAudioFormatWriter cooperate in that repect
		by both assuming the header is 300 bytes. If the header written by the SolarisAudioFormatWriter
		is actually less than 300 bytes, it must pad the remaining space with a blank line (all space
		characters followed by CR and LF). The SolarisOutputStream will actually discard this
		blank line when writing out to the pool text file. Also, the SolarisOutputStream will assume
		any data written past the 300-byte position goes to the ras audio file.

		@param[in] pSampleFolder	A pointer to the SampleFolder model.
		@param[in] sampleID			The ID of the sample to associate with the output stream.

		*/
	SolarisOutputStream(SampleFolder* sampleFolder, SampleID sampleID);

	/** Destructor. */
	~SolarisOutputStream();

	/** Returns the status of the stream.
		The result will be ok if the file opened successfully. If an error occurs while
		opening or reading from the file, this will contain an error message.
		*/
	const Result& getStatus() const noexcept{ return m_status; }

		/** Returns true if the stream couldn't be opened for some reason.
			@see getResult()
			*/
	bool failedToOpen() const noexcept{ return m_status.failed(); }

		/** Returns true if the stream opened without problems.
			@see getResult()
			*/
	bool openedOk() const noexcept{ return m_status.wasOk(); }


	/** Returns the offset of the next byte that will be written to the stream.
		@see setPosition
	*/
	int64 getPosition() override;

	/** Tries to move the current write position of the stream.

		The position is an absolute number of bytes from the stream's start.

		@returns  true if the stream manages to reposition itself correctly
		@see getPosition
	*/
	bool setPosition(int64 newPosition) override;


	/** Writes a block of data to the stream.

		@param dataToWrite      the target buffer to receive the data. This must not be null.
		@param numberOfBytes    the number of bytes to write.
		@returns false if the write operation fails for some reason
	*/
	bool write(const void* dataToWrite,size_t numberOfBytes) override;

	/** If the stream is using a buffer, this will ensure it gets written
		out to the destination. */
	void flush() override;

private:
	Result				m_status;
	SampleFolder*		m_pSampleFolder;
	SampleID			m_sampleID;
	std::ofstream		m_rawFile;
	int64				m_currentPosition{ 0 };	// next byte to read
	HeaderBuffer		m_header;
};


#endif  // SOLARISINPUTSTREAM_H_INCLUDED
