/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#include "MainMenu.h"
#include "AppCmdMgr.h"
#include "MainComponent.h"

StringArray MainMenuBarModel::getMenuBarNames()
{
	const char* const names[] = { "Pool","Sample", "Edit", "Audio","Help",nullptr };

	return StringArray(names);
}

PopupMenu MainMenuBarModel::getMenuForIndex(int menuIndex,const String& /*menuName*/)
{
	PopupMenu menu;
	ApplicationCommandManager* pAppCmdMgr = g_pAppCommandManager.get();

	if (menuIndex == 0)
	{
		menu.addCommandItem(pAppCmdMgr, PoolAdd);
		menu.addCommandItem(pAppCmdMgr, PoolRemove);
		menu.addSeparator();
		menu.addCommandItem(pAppCmdMgr, PoolChooseFolder);
	}
	else if (menuIndex == 1)
	{
		menu.addCommandItem(pAppCmdMgr, SampleImport);
		menu.addCommandItem(pAppCmdMgr, SampleRemove);
		//menu.addCommandItem(pAppCmdMgr, SampleResample);
	}
	else if (menuIndex == 2)
	{
		menu.addCommandItem(pAppCmdMgr, EditSetLoopToRange);
	}
	else if (menuIndex == 3)
	{
		menu.addCommandItem(pAppCmdMgr, AudioSetup);
	}
	else if (menuIndex == 4)
	{
		menu.addCommandItem(pAppCmdMgr, HelpAbout);
	}

	return menu;
}

void MainMenuBarModel::menuItemSelected(int menuItemID,int /*topLevelMenuIndex*/)
{
	int nothing;
	nothing = menuItemID;
}
