/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#include <sstream>
#include <iomanip>
#include "MainComponent.h"
#include "AppCmdMgr.h"
#include "MainMenu.h"
#include "SolarisAudioFormat.h"
#include "SolarisInputStream.h"
#include "R8brainAudioSource.h"
#include "TransportPanel.h"
#include "InfoPanel.h"

using std::make_unique;
using std::string;

std::unique_ptr<AudioDeviceManager> MainContentComponent::m_pSharedAudioDeviceManager;
void AdjustLookAndFeelColors(LookAndFeel& lookAndFeel);

class FixedHorizontalSplitPanel : public Component
{
public:
	FixedHorizontalSplitPanel(Component& topComponent, Component& bottomComponent)
		:m_pTopComponent(&topComponent)
		,m_pBottomComponent(&bottomComponent)
	{
		addAndMakeVisible(m_pTopComponent);
		addAndMakeVisible(m_pBottomComponent);
	}

	void resized() override
	{
		auto rect = getLocalBounds();
		m_pTopComponent->setBounds(rect.removeFromTop(230));
		m_pBottomComponent->setBounds(rect);
	}

private:
	Component* m_pTopComponent;
	Component* m_pBottomComponent;
};



//==============================================================================
MainContentComponent::MainContentComponent()
{
    setSize (1000, 600);

	AdjustLookAndFeelColors(m_lookAndFeel);
	LookAndFeel::setDefaultLookAndFeel(&m_lookAndFeel);

	m_propertiesOptions.applicationName = "Solsa";
	m_propertiesOptions.folderName = "Solsa";
	m_propertiesOptions.filenameSuffix = "config";
	m_propertiesOptions.storageFormat = PropertiesFile::storeAsXML;

	m_pPropertiesFile = make_unique<PropertiesFile>(m_propertiesOptions);
	DBG("Configuration file:");
	DBG(m_pPropertiesFile->getFile().getFullPathName());
	m_defaultProperties.setValue(String("pooldirectory"),File::getCurrentWorkingDirectory().getFullPathName());
	m_defaultProperties.setValue(String("importdirectory"),File::getCurrentWorkingDirectory().getFullPathName());
	m_defaultProperties.setValue(String("importsamplerate"), String(unchangedText));
	m_defaultProperties.setValue(String("volume"), 100);
	m_pPropertiesFile->setFallbackPropertySet(&m_defaultProperties);


	m_pMainMenuModel = make_unique<MainMenuBarModel>();
	m_pMainMenuComponent = make_unique<MenuBarComponent>(m_pMainMenuModel.get());

	Rectangle<int> area(getLocalBounds());
	m_pMainMenuComponent->setBounds(area.removeFromTop(LookAndFeel::getDefaultLookAndFeel().getDefaultMenuBarHeight()));
	addAndMakeVisible(m_pMainMenuComponent.get());

	if (!m_pSharedAudioDeviceManager)
	{
		m_pSharedAudioDeviceManager = std::make_unique<AudioDeviceManager>();
		
		std::unique_ptr<XmlElement> pElem(m_pPropertiesFile->getXmlValue("audiosettings"));
		m_pSharedAudioDeviceManager->initialise(2, 2, pElem.get(), true, String(), 0);
	}

	m_pSampleFolder = make_unique<SampleFolder>(m_pPropertiesFile->getValue("pooldirectory").toStdString());
	m_pViewModel = make_unique<ViewModel>(m_pSampleFolder.get());
	m_pPoolTreePanel = make_unique<PoolTreePanel>(m_pSampleFolder.get(), m_pPropertiesFile.get(), m_pViewModel.get());

	m_pInfoPanel = make_unique<InfoPanel>(m_pSampleFolder.get(), m_pViewModel.get());
	m_pTransportPanel = make_unique<TransportPanel>(m_pSampleFolder.get(), m_pSharedAudioDeviceManager.get(), m_audioFormatManager, m_pViewModel.get(), m_pPropertiesFile.get());
	m_pInfoTransportPanel = make_unique<FixedHorizontalSplitPanel>(*m_pInfoPanel, *m_pTransportPanel);

	m_splitComponent.SetFitToParent(false);
	m_splitComponent.SetTopLeftComponent(*m_pPoolTreePanel);
	m_splitComponent.SetBottomRightComponent(*m_pInfoTransportPanel);
	addAndMakeVisible(m_splitComponent);
	resized();

	// The split component has no bounds until we call resized(), so setting the split 
	// bar position won't work until after we call resized().
	m_splitComponent.SetSplitBarPosition(int(getWidth() * 0.30)); 

	int numFormats = m_audioFormatManager.getNumKnownFormats();
	m_audioFormatManager.registerBasicFormats();
	numFormats = m_audioFormatManager.getNumKnownFormats();
	m_audioFormatManager.registerFormat(new SolarisAudioFormat(), false);
	assert(m_audioFormatManager.getNumKnownFormats() == numFormats + 1);
}

MainContentComponent::~MainContentComponent()
{
	// MainMenuComponent has to be deleted before MainMenuModel otherwise a crash will occur when the model tries to 
	// remove a listener (the MenuComponent) and it no longer exists.
	m_pMainMenuComponent.reset();

	m_pSharedAudioDeviceManager.reset();
	m_pPoolTreePanel.reset();
}

void MainContentComponent::paint (Graphics& g)
{
	g;
    //g.fillAll (Colour (0xff001F36));
	//
    //g.setFont (Font (16.0f));
    //g.setColour (Colours::white);
    //g.drawText ("Hello World!", getLocalBounds(), Justification::centred, true);
}

void MainContentComponent::resized()
{
    // This is called when the MainContentComponent is resized.
    // If you add any child components, this is where you should
    // update their positions.
	if (m_pMainMenuComponent)
	{
		Rectangle<int> area(getLocalBounds());
		m_pMainMenuComponent->setBounds(area.removeFromTop(LookAndFeel::getDefaultLookAndFeel().getDefaultMenuBarHeight()));

		Rectangle<int> splitterArea(getLocalBounds());
		splitterArea.removeFromTop(LookAndFeel::getDefaultLookAndFeel().getDefaultMenuBarHeight());
		m_splitComponent.setBounds(splitterArea);

	}

}


//==============================================================================
// The following methods implement the ApplicationCommandTarget interface, allowing
// this window to publish a set of actions it can perform, and which can be mapped
// onto menus, keypresses, etc.

ApplicationCommandTarget* MainContentComponent::getNextCommandTarget()
{
	// this will return the next parent component that is an ApplicationCommandTarget (in this
	// case, there probably isn't one, but it's best to use this method in your own apps).
	return findFirstTargetParentComponent();
}

void MainContentComponent::getAllCommands(Array<CommandID>& commands)
{
	// this returns the set of all commands that this target can perform..
	const CommandID ids[] = {
		PoolAdd,
		PoolRemove,
		PoolChooseFolder,
		SampleImport,
		SampleRemove,
		SampleResample,
		EditSetLoopToRange,
		EditCut,
		EditCopy,
		EditPaste,
		AudioSetup,
		HelpAbout
	};
	commands.addArray(ids,numElementsInArray(ids));
}

void MainContentComponent::getCommandInfo(CommandID commandID,juce::ApplicationCommandInfo& result)
{
	const String poolCategory("Pool");
	const String waveCategory("Sample");
	const String editCategory("Edit");
	const String audioCategory("Audio");

	switch (commandID)
	{
	case PoolAdd:
		result.setInfo("Add","Adds a new empty pool to the folder.",poolCategory,0);
		result.addDefaultKeypress('a',ModifierKeys::commandModifier);
		break;
	case PoolRemove:
		result.setInfo("Remove","Removes the selected pool and all it's waves from the folder (to the trash).",poolCategory,0);
		result.addDefaultKeypress('r',ModifierKeys::commandModifier);

		// The PoolRemove command is only enabled when a pool is selected.
		result.setActive(m_pViewModel->HasPoolSelection());
		break;
	case PoolChooseFolder:
		result.setInfo("Choose Folder","Select the folder for polls to work with.",poolCategory,0);
		result.addDefaultKeypress('f',ModifierKeys::commandModifier);
		break;
	case SampleImport:
		result.setInfo("Import","Imports a sample from an audio file to the currently selected pool.",waveCategory,0);
		result.addDefaultKeypress('i',ModifierKeys::commandModifier);
		break;
	case SampleRemove:
		result.setInfo("Remove","Removes the selected sample from the folder (to the trash)", waveCategory,0);
		result.addDefaultKeypress('m', ModifierKeys::commandModifier);

		// The SampleRemove command is only enabled when a sample is selected.
		result.setActive(m_pViewModel->HasSampleSelection());
		break;
	case SampleResample:
		result.setInfo("Resample","Convert the wave data to a different sample rate.", waveCategory,0);
		result.addDefaultKeypress('v',ModifierKeys::commandModifier);
		result.setActive(false);
		break;
	case EditCut:
		result.setInfo("Cut","Removes the selected region from the wave.", editCategory,0);
		result.addDefaultKeypress('x',ModifierKeys::commandModifier);
		break;
	case EditCopy:
		result.setInfo("Copy","Copies the selected region of the wave.", editCategory,0);
		result.addDefaultKeypress('c',ModifierKeys::commandModifier);
		break;
	case EditPaste:
		result.setInfo("Paste","Pastes the cut or copied data to the selected region of the wave.", editCategory,0);
		result.addDefaultKeypress('x',ModifierKeys::commandModifier);
		break;
	case EditSetLoopToRange:
		result.setInfo("Set Loop to Range", "Set the loop start and end points based on the range selected.", editCategory, 0);
		result.addDefaultKeypress('l', ModifierKeys::commandModifier);

		// The EditSetLoopToRange command is only enabled when a sample is selected and the selection length is nonzero.
		{
			bool active = false;
			if (m_pViewModel->HasSampleSelection())
			{
				auto start = m_pTransportPanel->GetSelectionRangeStart();
				auto end = m_pTransportPanel->GetSelectionRangeEnd();
				if (start < end)
				{
					active = true;
				}
			}
			result.setActive(active);
		}
		break;
	case AudioSetup:
		result.setInfo("Setup", "Choose the audio output device.", audioCategory, 0);
		result.addDefaultKeypress('d', ModifierKeys::commandModifier);
		break;
	case HelpAbout:
		result.setInfo("About","About Solsa.","Help",0);
		result.addDefaultKeypress('h',ModifierKeys::commandModifier);
		break;
	default:
		break;
	};
}

/** @brief Called by JUCE when a menu commands occurs.

	perform() overrides juce::CommandAppllicationarget::perform(). \n
	It just dispatches execution to the appropriate sub-function that actually does the job.

	@param[in] info		Contains the commandID of the command that was generated.
	@returns			true if the command was handled. false, otherwise.
	@exception std::runtime_exception	If an exception occurs in any of the sub-functions called.
	@see DoPoolAdd, DoPoolRemove, PoolTreePanel::ChoosePoolFolder, DoSampleImport, DoSampleRemove, DoAudioSetup, DoHelpAbout
*/
bool MainContentComponent::perform(const InvocationInfo& info)
{
	bool result = true;
	try
	{
		switch (info.commandID)
		{
		case PoolAdd:
			DoPoolAdd();
			break;
		case PoolRemove:
			DoPoolRemove();
			break;
		case PoolChooseFolder:
			assert(m_pPoolTreePanel);
			m_pPoolTreePanel->ChoosePoolFolder();
			break;
		case SampleImport:
			DoSampleImport();
			break;
		case SampleRemove:
			DoSampleRemove();
			break;
		case EditSetLoopToRange:
			DoRangeSetLoop();
			break;
		case AudioSetup:
			DoAudioSetup();
			break;
		case HelpAbout:
			DoHelpAbout();
			break;
		default:
			result = false;
			break;
		};
	}
	catch (std::runtime_error e)
	{
		AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon, "Solsa Error", e.what(), "OK");
	}

	return result;
}

/** @brief Adds a new, empty pool to the SampleFolder.
*/
void MainContentComponent::DoPoolAdd()
{
	m_pSampleFolder->CreatePool("New Pool");
}

/** @brief Removes the currently selected pool from the SampleFolder.

	This will delete the pool file and ALL associated RAW files to the trash/recycle bin.
	@see SampleFolder::RemovePool.
*/
void MainContentComponent::DoPoolRemove()
{

	if (m_pViewModel->HasPoolSelection())
	{
		PoolID selectedPoolID = m_pViewModel->GetSelectedPoolID();

		string msg = "Are you sure you want to remove the pool named ";
		msg += m_pSampleFolder->GetPoolName(selectedPoolID);
		msg += "? This will delete the pool file and ALL associated RAW files to the recycle bin.";
		AlertWindow w("Remove Pool", msg, AlertWindow::NoIcon);
		w.addButton("Ok", 1, KeyPress(KeyPress::returnKey, 0, 0));
		w.addButton("Cancel", 0, KeyPress(KeyPress::escapeKey, 0, 0));

		if (w.runModalLoop() != 0) // if 'Ok' button was clicked
		{
			m_pSampleFolder->RemovePool(selectedPoolID);
		}
	}
}

std::string MainContentComponent::ChooseAudioFile()
{
	File lastDirectory(m_pPropertiesFile->getValue("importdirectory"));
	juce::String formats = m_audioFormatManager.getWildcardForAllFormats();

	// We don't want to import the Solaris format which is a .txt file. So remove that.
	formats = formats.replace(";*.txt", "", true);

	FileChooser fc("Choose a file...",
		lastDirectory,
		formats,
		true);
	
	String folderPathChosen = "";

	if (fc.browseForFileToOpen())
	{
		File folderChosen = fc.getResult();
		folderPathChosen = folderChosen.getFullPathName();

		m_pPropertiesFile->setValue("importdirectory",folderPathChosen);
	}
	return folderPathChosen.toStdString();
}

bool MainContentComponent::ChooseImportSampleRate(AudioFormat& audioFormat, int numChannels, double sourceSampleRate,double& destSampleRate)
{
	std::ostringstream os;
	os << "This is a " << ((numChannels == 1) ? "mono" : "stereo");
	os << " file and the original sample rate is " << sourceSampleRate << " samples / second.\n\nWhat sample rate would you like ? ";
	AlertWindow w("Import Options",os.str(),AlertWindow::QuestionIcon);

	StringArray arr;
	arr.add(unchangedText);

	String preferredSampleRate = m_pPropertiesFile->getValue("importsamplerate");
	auto sampleRateList = audioFormat.getPossibleSampleRates();
	int selectedItemIndex = 0;
	for (auto& s : sampleRateList)
	{
		if (s != sourceSampleRate)
		{
			os.str("");
			os << s;
			arr.add(os.str());
			if (os.str() == preferredSampleRate)
			{
				selectedItemIndex = arr.size() - 1;
			}
		}
	}
	w.addComboBox("SampleRateCombo",arr,"");

	w.getComboBoxComponent("SampleRateCombo")->setSelectedItemIndex(selectedItemIndex);

	w.addButton("OK",1,KeyPress(KeyPress::returnKey,0,0));
	w.addButton("Cancel",0,KeyPress(KeyPress::escapeKey,0,0));

	bool resultValid = false;
	if (w.runModalLoop() != 0) // if they picked 'ok'
	{
		// this is the item they chose in the drop-down list..
		const int optionIndexChosen = w.getComboBoxComponent("SampleRateCombo")->getSelectedItemIndex();
		string itemText = w.getComboBoxComponent("SampleRateCombo")->getItemText(optionIndexChosen).toStdString();
		if (optionIndexChosen > 0)	// If item chosen is not "Unchanged", convert sample rate chosen to int
		{
			destSampleRate = atoi(itemText.c_str());
		}
		m_pPropertiesFile->setValue("importsamplerate",String(itemText));
		resultValid = true;
	}

	return resultValid;
}

string GetRawFilenameFromImportFilepath(string importFilepath, string folder, bool preferNumbering)
{
	File f(importFilepath);

	if (!folder.ends_with(File::getSeparatorString().text))
	{
		folder += File::getSeparatorString().text;
	}

	String filepathBase = folder;
	filepathBase += f.getFileNameWithoutExtension();

	int index = 1;
	string indexStr = preferNumbering ? "-1" : "";
	String newFilepath;
	File newFile;
	do
	{
		newFilepath = filepathBase;
		newFilepath += indexStr.c_str();
		newFilepath += ".raw";
		indexStr = "-" + std::to_string(++index);

		newFile = File(newFilepath);

	} while (newFile.existsAsFile());

	return newFile.getFileName().toStdString();
}

/**	@brief Imports a sample from a common audio file to the currently selected pool.

	The user will be prompted for the audio file to import. Then the user will be prompted to optionally 
	convert the sample rate or leave the sample rate unchanged.
	The audio file imported may be a mono or stereo file. If it is a stereo file, the left and 
	right sides will be split up into two mono samples because the Solaris can only handle mono samples.
*/
void MainContentComponent::DoSampleImport()
{
	SolarisAudioFormat solarisFormat;
	AudioFormatManager formatManager;
	
	if (m_pViewModel->HasSelection())
	{

		std::string audioFilePath = ChooseAudioFile();
		if (!audioFilePath.empty())	// This may be empty if the user canceled the file choose operation.
		{
			File audioFile(audioFilePath);

			std::unique_ptr<AudioFormatReader> reader(m_audioFormatManager.createReaderFor(audioFile));

			if (reader != nullptr)
			{
				auto sourceSampleRate = reader->sampleRate;
				auto destSampleRate = sourceSampleRate;
				auto numChannels = reader->numChannels;

				bool canceled = !ChooseImportSampleRate(solarisFormat, numChannels, sourceSampleRate, destSampleRate);
				
				if (!canceled)
				{
					PoolID poolID = m_pViewModel->HasPoolSelection() ?
									m_pViewModel->GetSelectedPoolID() :
									m_pViewModel->GetSelectedSampleID().GetPoolID();

					SampleInfo sampleInfo;

					for (unsigned channel = 1; channel <= numChannels; ++channel)
					{
						string newFilename = GetRawFilenameFromImportFilepath(audioFilePath, m_pSampleFolder->GetFolderPath(), numChannels > 1);
						sampleInfo.sampleIndex = UINT16_MAX;	// add the new sample to the end
						SampleID newSampleID = m_pSampleFolder->CreateSample(poolID, newFilename, sampleInfo);

						SolarisOutputStream* pSolarisOutputStream = new SolarisOutputStream(m_pSampleFolder.get(), newSampleID);

						// pSolarisOutputStream will deleted by the writer.
						// Note that we're only telling the writer to write one channel. Since the writer only writes lowest numbered channels (i.e. it cannot skip 
						// over channel one to write only channel 2), we will need to use a channel remapper below if we want to write channel 2.
						std::unique_ptr<AudioFormatWriter> pWriter(solarisFormat.createWriterFor(pSolarisOutputStream, destSampleRate, 1, 16, SolarisAudioFormat::createMetadata(0,0,60,0,0,127), 0));
						const int blockSize = 0x0800;

						// We need to create a new reader for the second channel since there is no way to rewind it after the first channel.
						if (channel == 2)
						{
							reader.reset(m_audioFormatManager.createReaderFor(audioFile));
						}
						// The second argument, false, means that currentAudioFileSource will not delete the reader passed in the first argument.
						auto currentAudioFileSource = std::make_unique<AudioFormatReaderSource>(reader.get(),false);

						// Get a native pointer that we may change to point to the channel remapper if we're working on the second channel.
						AudioSource* pAudioSource = currentAudioFileSource.get();

						// For the second channel, we need to insert a stage that will remap the second channel (right side) to the first channel (left side) 
						// because we only want the writer to write one channel at a time and the writer cannot skip over the first channel to write the second.
						std::unique_ptr<ChannelRemappingAudioSource> pRemappedSource;
						if (channel == 2)
						{
							// The second argument, false, means that pRemappedSource will not delete the AudioSource passed in the first argument.
							pRemappedSource = std::make_unique<ChannelRemappingAudioSource>(pAudioSource, false);
							pRemappedSource->setOutputChannelMapping(0, 1);	// map the second channel to the first
							pRemappedSource->setOutputChannelMapping(1, 0);	// map the second channel to the first
							pAudioSource = pRemappedSource.get();
						}


						pAudioSource->prepareToPlay(blockSize, sourceSampleRate);
						int totalLength = static_cast<int>(currentAudioFileSource->getTotalLength());

						if (sourceSampleRate == destSampleRate)
						{
							pWriter->writeFromAudioSource(*pAudioSource, totalLength, blockSize);
						}
						else
						{
							R8brainAudioSource ras(pAudioSource, false, 1);
							ras.setResamplingRatio(sourceSampleRate, destSampleRate);
							ras.prepareToPlay(blockSize, destSampleRate);
							pWriter->writeFromAudioSource(ras, int(totalLength * (float)destSampleRate / sourceSampleRate), blockSize);
						}
						
						// delete writer to cause the header to be written out with the correct sample length, and also so it won't
						// try to write out the loopStart as zero anymore. We're going to change that below.
						pWriter.reset();

						// Initialize loop start so that it works as a one-shot.
						SampleInfo info = m_pSampleFolder->GetSampleInfo(newSampleID);
						info.loopStart = info.sampleLength - 1;
						info.loopEnd = 0;
						m_pSampleFolder->SetSampleInfo(newSampleID, info);
					}
				}
			}
		}
	}
}

void MainContentComponent::DoSampleRemove()
{
	if (m_pViewModel->HasSampleSelection())
	{
		SampleID selectedSampleID = m_pViewModel->GetSelectedSampleID();

		string msg = "Are you sure you want to remove the sample with filename ";
		msg += m_pSampleFolder->GetSampleRawFilename(selectedSampleID);
		msg += "? This will delete the raw sample file to the recycle bin.";
		AlertWindow w("Remove Sample", msg, AlertWindow::NoIcon);
		w.addButton("Ok", 1, KeyPress(KeyPress::returnKey, 0, 0));
		w.addButton("Cancel", 0, KeyPress(KeyPress::escapeKey, 0, 0));
		if (w.runModalLoop() != 0) // if 'Ok' button was clicked
		{
			m_pSampleFolder->RemoveSample(selectedSampleID, true);
		}
	}
}

void MainContentComponent::DoSampleRenameRawfile(const std::string& newRawfileName)
{
	newRawfileName;
}

void MainContentComponent::DoRangeSetLoop()
{
	if (m_pViewModel->HasSampleSelection())
	{
		auto start = m_pTransportPanel->GetSelectionRangeStart();
		auto end = m_pTransportPanel->GetSelectionRangeEnd();
		if (start < end)
		{
			SampleID selectedSample = m_pViewModel->GetSelectedSampleID();
			SampleInfo info = m_pSampleFolder->GetSampleInfo(selectedSample);
			info.loopStart = start;
			info.loopEnd = end - 1;
			m_pSampleFolder->SetSampleInfo(selectedSample, info);
		}
	}
}


class AudioSettingsComponent :	public Component,
								private Button::Listener
{
public:
	AudioSettingsComponent(AudioDeviceManager& audioDeviceManager)
	{
		m_audioSetupComp = std::make_unique<AudioDeviceSelectorComponent>(audioDeviceManager, 0, 0, 0, 256, false, false, true, false);
		addAndMakeVisible(m_audioSetupComp.get());

		m_okButton.setButtonText("OK");
		m_cancelButton.setButtonText("Cancel");
		
		m_okButton.addListener(this);
		m_cancelButton.addListener(this);

		addAndMakeVisible(m_okButton);
		addAndMakeVisible(m_cancelButton);
	}

	~AudioSettingsComponent()
	{}

	void resized() override
	{
		//Rectangle<int> r(getLocalBounds().reduced(4));
		m_audioSetupComp->setBounds(getLocalBounds().removeFromTop(getHeight() - 40));

		m_okButton.setBounds(getWidth() / 2 - 90 - 10, getHeight() - 35, 90, 24);
		m_cancelButton.setBounds(getWidth() / 2 + 10, getHeight() - 35, 90, 24);
	}

private:
	void buttonClicked(Button* button) override;

	std::unique_ptr<AudioDeviceSelectorComponent> m_audioSetupComp;
	TextButton	m_okButton;
	TextButton	m_cancelButton;
	
	JUCE_DECLARE_NON_COPYABLE(AudioSettingsComponent) \
	JUCE_LEAK_DETECTOR(AudioSettingsComponent)
};

void AudioSettingsComponent::buttonClicked(Button* button)
{
	getParentComponent()->exitModalState((button == &m_okButton) ? 1 : 0);
}


void MainContentComponent::AudioSetupCallbackStatic(int modalresult, MainContentComponent* pMainContentComponent)
{
	if (pMainContentComponent)
	{
		pMainContentComponent->AudioSetupCallback(modalresult);
	}
	else
	{
		assert(false);
	}
}

void MainContentComponent::AudioSetupCallback(int modalresult)
{
	if (modalresult == 1)
	{
		std::unique_ptr<XmlElement> pElem(m_pSharedAudioDeviceManager->createStateXml());
		if (pElem)
		{
			m_pPropertiesFile->setValue("audiosettings", pElem.get());
		}
	}
	else
	{
		std::unique_ptr<XmlElement> pElem(m_pPropertiesFile->getXmlValue("audiosettings"));
		m_pSharedAudioDeviceManager->initialise(2, 2, pElem.get(), true, String(), 0);
	}
}

void MainContentComponent::DoAudioSetup()
{

	DialogWindow::LaunchOptions options;
	options.content.setOwned(new AudioSettingsComponent(*m_pSharedAudioDeviceManager));

	Rectangle<int> area(0, 0, 580, 340);
	options.content->setSize(area.getWidth(), area.getHeight());

	options.dialogTitle = "Audio Settings";
	//options.dialogBackgroundColour = Colour(0xff0e345a);
	options.escapeKeyTriggersCloseButton = true;
	options.useNativeTitleBar = false;
	options.resizable = false;

	DialogWindow* dw = options.launchAsync();

	ModalComponentManager::getInstance()->attachCallback(dw, ModalCallbackFunction::forComponent( AudioSetupCallbackStatic, this));

	dw->centreWithSize(580, 340);
	
}


void MainContentComponent::DoHelpAbout()
{
	AlertWindow::showMessageBoxAsync(AlertWindow::InfoIcon,
		"About Solsa",
		"Solsa version 0.2 by Jim Hewes. Copyright 2016, 2024.",
		"OK");
}



void AdjustLookAndFeelColors(LookAndFeel& lookAndFeel)
{
	// initialise the standard set of colours..0xff87cefa
	//const uint32 textButtonColour = 0xffbbbbff;
	const uint32 textButtonColour = 0xffa0a0a0;	// lightskyblue
												//const uint32 textHighlightColour = 0x401111ee;
	const uint32 textHighlightColour = 0xffc0c0c0;
	//const uint32 standardOutlineColour = 0xb2808080;
	const uint32 standardOutlineColour = 0xffa0a0a0;

	static const uint32 standardColours[] =
	{
		TextButton::buttonColourId, 0xe6f0f0ff,
		TextButton::buttonOnColourId, 0xff4444ff,
		TextButton::textColourOnId, 0xff000000,
		TextButton::textColourOffId, 0xff000000,

		ToggleButton::textColourId, 0xff000000,

		TextEditor::backgroundColourId, 0xffffffff,
		TextEditor::textColourId, 0xff000000,
		TextEditor::highlightColourId, textHighlightColour,
		TextEditor::highlightedTextColourId, 0xff000000,
		TextEditor::outlineColourId, 0x00000000,
		TextEditor::focusedOutlineColourId, textButtonColour,
		TextEditor::shadowColourId, 0x38000000,

		CaretComponent::caretColourId, 0xff000000,

		Label::backgroundColourId, 0x00000000,
		Label::textColourId, 0xff000000,
		Label::outlineColourId, 0x00000000,

		ScrollBar::backgroundColourId, 0x00000000,
		//ScrollBar::thumbColourId, 0xffffffff,
		ScrollBar::thumbColourId, standardOutlineColour,

		TreeView::linesColourId, 0x4c000000,
		TreeView::backgroundColourId, 0x00000000,
		TreeView::dragAndDropIndicatorColourId, 0x80ff0000,
		TreeView::selectedItemBackgroundColourId, 0x00000000,

		PopupMenu::backgroundColourId, 0xffffffff,
		PopupMenu::textColourId, 0xff000000,
		PopupMenu::headerTextColourId, 0xff000000,
		PopupMenu::highlightedTextColourId, 0xffffffff,
		//PopupMenu::highlightedBackgroundColourId, 0x991111aa,
		PopupMenu::highlightedBackgroundColourId, 0xff87cefa,

		//ComboBox::buttonColourId, 0xffbbbbff,
		ComboBox::buttonColourId, 0xff87cefa,
		ComboBox::outlineColourId, standardOutlineColour,
		ComboBox::textColourId, 0xff000000,
		ComboBox::backgroundColourId, 0xffffffff,
		ComboBox::arrowColourId, 0x99000000,

		PropertyComponent::backgroundColourId, 0x66ffffff,
		PropertyComponent::labelTextColourId, 0xff000000,

		TextPropertyComponent::backgroundColourId, 0xffffffff,
		TextPropertyComponent::textColourId, 0xff000000,
		TextPropertyComponent::outlineColourId, standardOutlineColour,

		BooleanPropertyComponent::backgroundColourId, 0xffffffff,
		BooleanPropertyComponent::outlineColourId, standardOutlineColour,

		ListBox::backgroundColourId, 0xffffffff,
		ListBox::outlineColourId, standardOutlineColour,
		ListBox::textColourId, 0xff000000,

		Slider::backgroundColourId, 0x00000000,
		Slider::thumbColourId, textButtonColour,
		Slider::trackColourId, 0x7fffffff,
		Slider::rotarySliderFillColourId, 0x7f0000ff,
		Slider::rotarySliderOutlineColourId, 0x66000000,
		Slider::textBoxTextColourId, 0xff000000,
		Slider::textBoxBackgroundColourId, 0xffffffff,
		Slider::textBoxHighlightColourId, textHighlightColour,
		Slider::textBoxOutlineColourId, standardOutlineColour,

		ResizableWindow::backgroundColourId, 0xff777777,
		//DocumentWindow::textColourId,               0xff000000, // (this is deliberately not set)

		AlertWindow::backgroundColourId, 0xffededed,
		AlertWindow::textColourId, 0xff000000,
		AlertWindow::outlineColourId, 0xff666666,

		ProgressBar::backgroundColourId, 0xffeeeeee,
		//ProgressBar::foregroundColourId, 0xffaaaaee,
		ProgressBar::foregroundColourId, textButtonColour,

		TooltipWindow::backgroundColourId, 0xffeeeebb,
		TooltipWindow::textColourId, 0xff000000,
		TooltipWindow::outlineColourId, 0x4c000000,

		TabbedComponent::backgroundColourId, 0x00000000,
		TabbedComponent::outlineColourId, 0xff777777,
		TabbedButtonBar::tabOutlineColourId, 0x80000000,
		TabbedButtonBar::frontOutlineColourId, 0x90000000,

		Toolbar::backgroundColourId, 0xfff6f8f9,
		Toolbar::separatorColourId, 0x4c000000,
		Toolbar::buttonMouseOverBackgroundColourId, 0x4c0000ff,
		Toolbar::buttonMouseDownBackgroundColourId, 0x800000ff,
		Toolbar::labelTextColourId, 0xff000000,
		Toolbar::editingModeOutlineColourId, 0xffff0000,

		DrawableButton::textColourId, 0xff000000,
		DrawableButton::textColourOnId, 0xff000000,
		DrawableButton::backgroundColourId, 0x00000000,
		DrawableButton::backgroundOnColourId, 0xaabbbbff,

		HyperlinkButton::textColourId, 0xcc1111ee,

		GroupComponent::outlineColourId, 0x66000000,
		GroupComponent::textColourId, 0xff000000,

		BubbleComponent::backgroundColourId, 0xeeeeeebb,
		BubbleComponent::outlineColourId, 0x77000000,

		DirectoryContentsDisplayComponent::highlightColourId, textHighlightColour,
		DirectoryContentsDisplayComponent::textColourId, 0xff000000,

		0x1000440, /*LassoComponent::lassoFillColourId*/        0x66dddddd,
		0x1000441, /*LassoComponent::lassoOutlineColourId*/     0x99111111,

		0x1005000, /*MidiKeyboardComponent::whiteNoteColourId*/               0xffffffff,
		0x1005001, /*MidiKeyboardComponent::blackNoteColourId*/               0xff000000,
		0x1005002, /*MidiKeyboardComponent::keySeparatorLineColourId*/        0x66000000,
		0x1005003, /*MidiKeyboardComponent::mouseOverKeyOverlayColourId*/     0x80ffff00,
		0x1005004, /*MidiKeyboardComponent::keyDownOverlayColourId*/          0xffb6b600,
		0x1005005, /*MidiKeyboardComponent::textLabelColourId*/               0xff000000,
		0x1005006, /*MidiKeyboardComponent::upDownButtonBackgroundColourId*/  0xffd3d3d3,
		0x1005007, /*MidiKeyboardComponent::upDownButtonArrowColourId*/       0xff000000,
		0x1005008, /*MidiKeyboardComponent::shadowColourId*/                  0x4c000000,

		0x1004500, /*CodeEditorComponent::backgroundColourId*/                0xffffffff,
		0x1004502, /*CodeEditorComponent::highlightColourId*/                 textHighlightColour,
		0x1004503, /*CodeEditorComponent::defaultTextColourId*/               0xff000000,
		0x1004504, /*CodeEditorComponent::lineNumberBackgroundId*/            0x44999999,
		0x1004505, /*CodeEditorComponent::lineNumberTextId*/                  0x44000000,

		0x1007000, /*ColourSelector::backgroundColourId*/                     0xffe5e5e5,
		0x1007001, /*ColourSelector::labelTextColourId*/                      0xff000000,

		0x100ad00, /*KeyMappingEditorComponent::backgroundColourId*/          0x00000000,
		0x100ad01, /*KeyMappingEditorComponent::textColourId*/                0xff000000,

		FileSearchPathListComponent::backgroundColourId, 0xffffffff,

		FileChooserDialogBox::titleTextColourId, 0xff000000,
	};

	for (int i = 0; i < numElementsInArray(standardColours); i += 2)
		lookAndFeel.setColour((int)standardColours[i], Colour((uint32)standardColours[i + 1]));

}
