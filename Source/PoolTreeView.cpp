/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/
#include "AppCmdMgr.h"
#include "PoolTreeView.h"
#include "ViewModel.h"
#include <cassert>

using std::vector;

bool PoolTreeRootItem::isInterestedInDragSource(const DragAndDropTarget::SourceDetails & dragSourceDetails)
{
	bool result = false;
	if (dragSourceDetails.description.isString())
	{
		std::vector<std::string> list = util::SplitString(
			dragSourceDetails.description.toString().toStdString(), ":", true);
		if (list[0] == "PoolItem")
		{
			result = true;
		}
	}
	return result;
}

void PoolTreeRootItem::itemDropped(const DragAndDropTarget::SourceDetails & dragSourceDetails, int insertIndex)
{
	if (dragSourceDetails.description.isString())
	{
		std::vector<std::string> list = util::SplitString(
			dragSourceDetails.description.toString().toStdString(), ":", true);
		if (list[0] == "PoolItem")
		{
			assert(list.size() > 1);
			PoolID poolID = reinterpret_cast<PoolID>(std::stoull(list[1]));
			DBG("Dropped " << std::to_string(uintptr_t(poolID)));
			
			m_pSampleFolder->MovePool(poolID, static_cast<uint16_t>(insertIndex));
		}
	}
}

PoolTreeView::PoolTreeView(SampleFolder* pSampleFolder, ViewModel* pViewModel)
	:m_pSampleFolder(pSampleFolder)
	, m_rootItem(pSampleFolder)
	, m_pViewModel(pViewModel)
{
	setRootItem(&m_rootItem);
	setRootItemVisible(false);

	setColour(ColourIds::backgroundColourId,Colours::whitesmoke);

	// Register for events from the sample folder
	m_folderChangeConnection = m_pSampleFolder->FolderChangeEvent.Connect([this]() {OnSampleFolderChanged();});
	m_poolChangeConnection = m_pSampleFolder->PoolChangeEvent.Connect([this](PoolID poolID) {OnPoolChanged(poolID);});

	OnSampleFolderChanged();
}

PoolTreeView::~PoolTreeView()
{
	m_rootItem.clearSubItems();	// clear all current items
	setRootItem(nullptr);
	m_pViewModel->ClearSelection();
}

/** OnFolderChanged is called when one or more of the pools in the folder have been moved, added or removed.
*/
void  PoolTreeView::OnSampleFolderChanged()
{
	std::vector<PoolID> newPoolIDs = m_pSampleFolder->GetPoolIDs();
#if 0

	int numPoolItems = m_rootItem.getNumSubItems();

	std::vector<PoolID> poolsToRemove;

	for (int i = 0; i < numPoolItems; ++i)
	{
		PoolTreePoolItem* pPoolItem = dynamic_cast<PoolTreePoolItem*>(m_rootItem.getSubItem(i));
		if (pPoolItem)
		{
			PoolID oldPoolID = pPoolItem->GetPoolID();
			bool found = (newPoolIDs.end() != std::find_if(newPoolIDs.begin(), newPoolIDs.end(),
				[oldPoolID](PoolID& newPoolID) { return newPoolID == oldPoolID; }));
			if (!found)
			{
				// delete "old" pool item not found in model.
				poolsToRemove.push_back(oldPoolID);
			}
		}
	}

	std::vector<PoolID> poolsToAdd;

	for (auto& newPoolID : newPoolIDs)
	{
		PoolTreePoolItem* pPoolItem = GetPoolItemWithPoolID(newPoolID);
		if (pPoolItem == nullptr)
		{
			// Add new pool not found in pool tree view
			poolsToAdd.push_back(newPoolID);
		}
	}

	RemovePools(poolsToRemove);

#endif // 0

	m_rootItem.clearSubItems();

	AddPools(newPoolIDs);
	repaint();

	// Update selection
	if (m_rootItem.getNumSubItems() > 0)
	{
		TreeViewItem* pSelectedItem = m_rootItem.getSubItem(0);
		pSelectedItem->setSelected(true, true);
	}
	else // tree is empty
	{
		m_pViewModel->ClearSelection();
	}
}

/** Returns the selected TreeViewItem if one is selected anywhere in the tree.

	There can be at most one item selected.

	@returns	A native pointer to any selected PoolTreePoolItem or PoolTreeSampleItem in the tree, 
				or nullptr if no item is selected or the tree is empty.
*/
TreeViewItem* PoolTreeView::GetSelectedItem()
{

	TreeViewItem* pPoolItem{ nullptr };
	TreeViewItem* pSampleItem { nullptr };
	for (int i = 0; i < m_rootItem.getNumSubItems(); ++i)
	{
		pPoolItem = m_rootItem.getSubItem(i);
		if (pPoolItem->isSelected())
		{
			return pPoolItem;
		}
		else
		{
			for (int j = 0; j < pPoolItem->getNumSubItems(); ++j)
			{
				pSampleItem = pPoolItem->getSubItem(j);
				if (pSampleItem->isSelected())
				{
					return pSampleItem;
				}
			}
		}
	}
	return nullptr;
}

void PoolTreeView::AddPools(const vector<PoolID>& poolsToAdd)
{
	for (auto& poolID : poolsToAdd)
	{
		PoolTreePoolItem* pPoolItem = new PoolTreePoolItem(m_pSampleFolder, poolID);
		pPoolItem->RebuildView();
		m_rootItem.addSubItem(pPoolItem);	// pPoolItem pointer is owned by the root item
	}
}

void PoolTreeView::RemovePools(const vector<PoolID>& poolsToRemove)
{
	for (auto& poolIDToRemove : poolsToRemove)
	{
		bool found = false;
		for (int i = 0; i < m_rootItem.getNumSubItems() && !found; ++i)
		{
			PoolTreePoolItem* pPoolItem = dynamic_cast<PoolTreePoolItem*>(m_rootItem.getSubItem(i));
			if (pPoolItem)
			{
				if (pPoolItem->GetPoolID() == poolIDToRemove)
				{
					m_rootItem.removeSubItem(i);
					found = true;
				}
			}
		}
	}
}

void PoolTreeView::OnPoolChanged(PoolID poolID)
{
	PoolTreePoolItem* pPoolItem = GetPoolItemWithPoolID(poolID);
	if (pPoolItem)
	{
		pPoolItem->OnPoolChanged();
	}

	// If a sample was previously selected and that sample no longer exists, clear the selection.
	if (m_pViewModel->HasSampleSelection())
	{
		if (!m_pSampleFolder->SampleExists(m_pViewModel->GetSelectedSampleID()))
		{
			m_pViewModel->ClearSelection();
		}
	}
}

void PoolTreeView::mouseDown(const MouseEvent & event)
{
	if (event.mods.isPopupMenu())
	{
		PopupMenu menu;
		menu.addCommandItem(g_pAppCommandManager.get(), CommandIDs::PoolAdd, "Pool Add");
//		menu.addCommandItem(g_pAppCommandManager.get(), CommandIDs::SampleImport, "Import Sample");
		const int result = menu.show();
		result;
	}
}

PoolTreePoolItem* PoolTreeView::GetPoolItemWithPoolID(PoolID poolIDToFind)
{
	PoolTreePoolItem* pPoolItem = nullptr;
	int numPoolItems = m_rootItem.getNumSubItems();
	bool found = false;
	for (int i = 0; i < numPoolItems && !found; ++i)
	{
		pPoolItem = dynamic_cast<PoolTreePoolItem*>(m_rootItem.getSubItem(i));
		if (pPoolItem)
		{
			if (pPoolItem->GetPoolID() == poolIDToFind)
			{
				found = true;
			}
		}
	}
	return found ? pPoolItem : nullptr;
}

ViewModel* PoolTreeView::GetSelectionModel()
{
	return m_pViewModel;
}

void PoolTreePoolItem::RebuildView()
{
	if (m_pSampleFolder)
	{
		clearSubItems();
		AddSamples(m_pSampleFolder->GetSampleIDs(m_poolID));
	}
}

void PoolTreePoolItem::AddSamples(const vector<SampleID>& samplesToAdd)
{
	for (auto& sampleID : samplesToAdd)
	{
		PoolTreeSampleItem* pSampleItem = new PoolTreeSampleItem(m_pSampleFolder, sampleID);
		addSubItem(pSampleItem);	// pSampleItem pointer is owned by the pool item
	}
}

void PoolTreePoolItem::RemoveSamples(const vector<SampleID>& samplesToRemove)
{
	for (auto& sampleIDToRemove : samplesToRemove)
	{
		bool found = false;
		for (int i = 0; i < getNumSubItems() && !found; ++i)
		{
			PoolTreeSampleItem* pSampleItem = dynamic_cast<PoolTreeSampleItem*>(getSubItem(i));
			if (pSampleItem)
			{
				if (pSampleItem->GetSampleID() == sampleIDToRemove)
				{
					removeSubItem(i);
					found = true;
				}
			}
		}
	}
}

bool PoolTreePoolItem::isInterestedInDragSource(const DragAndDropTarget::SourceDetails & dragSourceDetails)
{
	bool result = false;
	if (dragSourceDetails.description.isString())
	{
		std::vector<std::string> list = util::SplitString(
			dragSourceDetails.description.toString().toStdString(), ":", true);
		if (list[0] == "SampleItem")
		{
			//SampleID sid;
			//sid.FromString(list[1]);
			//DBG(sid.ToString());
			result = true;
		}
	}
	return result;
}

void PoolTreePoolItem::itemDropped(const DragAndDropTarget::SourceDetails & dragSourceDetails, int insertIndex)
{

	if (dragSourceDetails.description.isString())
	{
		std::vector<std::string> list = util::SplitString(
			dragSourceDetails.description.toString().toStdString(), ":", true);
		if (list[0] == "SampleItem")
		{
			SampleID sampleID;
			sampleID.FromString(list[1]);
			DBG("Dropped " << sampleID.ToString());
			m_pSampleFolder->MoveSample(sampleID, m_poolID, static_cast<uint16_t>(insertIndex));
		}
	}
}

void PoolTreePoolItem::OnPoolChanged()
{
	// See if any samples were added or removed.
	std::vector<SampleID> newSampleIDs = m_pSampleFolder->GetSampleIDs(m_poolID);

#if 0
	std::vector<SampleID> samplesToRemove = newSampleIDs;
	int numSampleItems = getNumSubItems();
	for (int i = 0; i < numSampleItems; ++i)
	{
		PoolTreeSampleItem* pSampleItem = dynamic_cast<PoolTreeSampleItem*>(getSubItem(i));
		if (pSampleItem)
		{
			SampleID oldSampleID = pSampleItem->GetSampleID();
			bool found = (newSampleIDs.end() != std::find_if(newSampleIDs.begin(), newSampleIDs.end(),
				[oldSampleID](SampleID& newSampleID) { return newSampleID == oldSampleID; }));
			if (!found)
			{	// delete "old" sample item not found in model.
				samplesToRemove.push_back(oldSampleID);
			}
		}
	}

	std::vector<SampleID> samplesToAdd = newSampleIDs;

	for (auto& newSampleID : newSampleIDs)
	{
		PoolTreeSampleItem* pSampleItem = GetSampleItemWithSampleID(newSampleID);
		if (pSampleItem == nullptr)
		{
			// Add new pool not found in pool tree view
			samplesToAdd.push_back(newSampleID);
		}
	}

	RemoveSamples(samplesToRemove);
#endif // 0
	clearSubItems();
	AddSamples(newSampleIDs);

	repaintItem(); // in case the pool name changed. paintItem() will get the new name.
}

void PoolTreePoolItem::paintItem(Graphics & g,int width,int height)
{
	g.setFont(height * 0.7f);
	g.setColour( Colours::black );
	g.drawText(m_pSampleFolder->GetPoolName(m_poolID),0,0,width,height,Justification::centredLeft);
}

void PoolTreePoolItem::itemClicked(const MouseEvent & event)
{
#if 1
	// Not sure... could context menu be confusing here?
	if (event.mods.isPopupMenu())
	{
		PopupMenu menu;
		menu.addCommandItem(g_pAppCommandManager.get(),CommandIDs::SampleImport,"Sample Import");
		menu.addCommandItem(g_pAppCommandManager.get(), CommandIDs::PoolRemove, "Pool Remove");
		const int result = menu.showMenu(PopupMenu::Options().withInitiallySelectedItem(20));
		result;
	}
#else
	event;
#endif // 0

}

void PoolTreePoolItem::itemSelectionChanged(bool isNowSelected)
{
	PoolTreeView* pPoolTreeView = dynamic_cast<PoolTreeView*>(getOwnerView());
	assert(pPoolTreeView);
	if (pPoolTreeView)
	{
		ViewModel* pViewModel = pPoolTreeView->GetSelectionModel();
		assert(pViewModel);
		assert(m_pSampleFolder);
		if (pViewModel && m_pSampleFolder)
		{
			if (isNowSelected)
			{
				//DBG("Pool selected");
				pViewModel->Select(m_poolID);
			}
			else
			{
				//DBG("Pool DEselected");
				pViewModel->ClearSelection();
			}
		}
	}
}

std::string PoolTreePoolItem::GetPoolFilePath()
{
	return m_pSampleFolder ? m_pSampleFolder->GetPoolFilePath(m_poolID) : "";
}

PoolTreeSampleItem* PoolTreePoolItem::GetSampleItemWithSampleID(SampleID sampleIDToFind)
{
	PoolTreeSampleItem* pSampleItem = nullptr;
	bool found = false;
	for (int i = 0; i < getNumSubItems() && !found; ++i)
	{
		pSampleItem = dynamic_cast<PoolTreeSampleItem*>(getSubItem(i));
		if (pSampleItem)
		{
			if (pSampleItem->GetSampleID() == sampleIDToFind)
			{
				found = true;
			}
		}
	}
	return found ? pSampleItem : nullptr;
}

PoolTreeSampleItem::PoolTreeSampleItem(SampleFolder* pSampleFolder, SampleID sampleID)
	:m_pSampleFolder(pSampleFolder), m_sampleID(sampleID) 

{
	m_sampleChangedConnection = m_pSampleFolder->SampleChangeEvent.Connect([this](SampleID sampleID) {OnSampleChanged(sampleID);});
}


std::unique_ptr<Component> PoolTreeSampleItem::createItemComponent()
{
	SampleInfo info = m_pSampleFolder->GetSampleInfo(m_sampleID);
	m_pItemLabel = std::make_unique<Label>("SampleItemLabel", m_pSampleFolder->GetSampleRawFilename(m_sampleID));
	m_pItemLabel->setColour(Label::ColourIds::backgroundWhenEditingColourId, Colours::lightgrey);
	m_pItemLabel->setInterceptsMouseClicks(false, false);
	m_pItemLabel->setColour(Label::ColourIds::textColourId, (m_pSampleFolder->RawFileIsReadable(m_sampleID) ? Colours::black : Colours::darkred));
	m_pItemLabel->addListener(this);
	return std::move(m_pItemLabel);
}

void PoolTreeSampleItem::itemSelectionChanged(bool isNowSelected)
{
	PoolTreeView* pPoolTreeView = dynamic_cast<PoolTreeView*>(getOwnerView());
	assert(pPoolTreeView);
	if (pPoolTreeView)
	{
		ViewModel* pViewModel = pPoolTreeView->GetSelectionModel();
		assert(pViewModel);
		assert(m_pSampleFolder);
		if (pViewModel && m_pSampleFolder)
		{
			if (isNowSelected)
			{
				//DBG("Sample selected");
				//PoolTreePoolItem* pParentItem = dynamic_cast<PoolTreePoolItem*>(getParentItem());
				//assert(pParentItem);
				SampleInfo info = m_pSampleFolder->GetSampleInfo(m_sampleID);
				pViewModel->Select(m_sampleID);
			}
			else
			{
				//DBG("Sample DEselected");
				pViewModel->ClearSelection();
			}
		}
	}
}

void PoolTreeSampleItem::itemClicked(const MouseEvent & event)
{
	if (event.mods.isPopupMenu())
	{
		PopupMenu menu;
		menu.addCommandItem(g_pAppCommandManager.get(), CommandIDs::SampleRemove, "Sample Remove");
		const int result = menu.show();
		result;
	}
}

void PoolTreeSampleItem::itemDoubleClicked(const MouseEvent & event)
{
	event;
	assert(m_pItemLabel != nullptr);
	m_pItemLabel->setInterceptsMouseClicks(true, true);
	m_pItemLabel->showEditor();

}

void PoolTreeSampleItem::labelTextChanged(Label * labelThatHasChanged)
{
	if (labelThatHasChanged == m_pItemLabel.get())
	{
		try 
		{
			m_pSampleFolder->SetSampleRawFilename(m_sampleID, m_pItemLabel->getText().toStdString());
		}
		catch (...)
		{
			// If renaming fails, restore whatever the old name was (and still is).
			m_pItemLabel->setText(m_pSampleFolder->GetSampleRawFilename(m_sampleID), juce::NotificationType::dontSendNotification);
		}
	}
}

void PoolTreeSampleItem::OnSampleChanged(SampleID sampleID)
{
	// It's possible that we get a notification for a sample changing before the JUCE library 
	// calls createItemComponent() to create the item component. So m_pItemLabel might be null here.
	if (m_sampleID == sampleID && m_pItemLabel)
	{
		m_pItemLabel->setText(m_pSampleFolder->GetSampleRawFilename(m_sampleID), juce::NotificationType::dontSendNotification);
	}
}

