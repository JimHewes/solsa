/*
  ==============================================================================

    Utility.h
    Created: 1 Nov 2014 2:03:02pm
    Author:  Jim

  ==============================================================================
*/

#ifndef UTILITY_H_INCLUDED
#define UTILITY_H_INCLUDED

#include <string>
#include <vector>
#include <cstdint>
#include <iostream>

namespace util {

	std::vector<std::string> SplitString(const std::string stringToSplit,const std::string separators,bool removeEmptyEntries);
	bool StartsWith(const std::string& searchString,const std::string& startString);
	bool EndsWith(const std::string& searchString,const std::string& endString);


	// trim from start
	static inline std::string& TrimLeft(std::string &s,const char* t = " \t\n\r\f\v")
	{
		s.erase(0,s.find_first_not_of(t));
		return s;
	}

	// trim from end
	static inline std::string& TrimRight(std::string &s,const char* t = " \t\n\r\f\v")
	{
		s.erase(s.find_last_not_of(t) + 1);
		return s;
	}

	// trim from both ends
	static inline std::string& Trim(std::string &s,const char* t = " \t\n\r\f\v")
	{
		return TrimLeft(TrimRight(s,t),t);
	}


	// Free up the memory a vector is holding without destroying it.
	template<typename T>
	inline void FreeVector(std::vector<T>& v)
	{
		std::vector<T> t; t.swap(v);
	}

	bool base64_encode(const std::vector<uint8_t>& input,std::string& output);
	bool base64_decode(const std::string& input,std::vector<uint8_t>& output);


	template <typename T> class Property {
		T value;
	public:
		T & operator = (const T &i) {
			::std::cout << i << ::std::endl;
			return value = i;
		}
		// This template class member function template serves the purpose to make
		// typing more strict. Assignment to this is only possible with exact identical
		// types.
		template <typename T2> T2 & operator = (const T2 &i) {
			::std::cout << "T2: " << i << ::std::endl;
			T2 &guard = value;
			throw guard; // Never reached.
		}

		// Implicit conversion back to T. 
		operator T const & () const {
			return value;
		}
	};

	std::vector<std::string> GetFilesInDirectory(std::string directoryPath);
} // namespace util

#endif  // UTILITY_H_INCLUDED
