/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#ifndef R8BRAINAUDIOSOURCE_H_INCLUDED
#define R8BRAINAUDIOSOURCE_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

// We don't want to include the r8brain file CDSPResampler.h here because it will pull in 
// Windows definitions that interfere with other definitions. And anyway, we don't need to include it.
namespace r8b
{
	class CDSPResampler24;
}
using ResamplerUPtr = std::unique_ptr < r8b::CDSPResampler24 >;
using ResamplerSPtr = std::shared_ptr < r8b::CDSPResampler24 >;




class R8brainAudioSource : public AudioSource
{

public:
	//==============================================================================
	/** Creates an R8brainAudioSource object that is used for resampling audio to different sample rates.

		@param inputSource              the input source to read from
		@param deleteInputWhenDeleted   if true, the input source will be deleted when
										this object is deleted
		@param numChannels              the number of channels to process
	*/
	R8brainAudioSource(AudioSource* inputSource,
		bool deleteInputWhenDeleted,
		int numChannels = 2);

	/** Destructor. */
	~R8brainAudioSource();


	/** Changes the resampling ratio.

	(This value can be changed at any time, even while the source is running).

	@param samplesOutPerInputSample     If set to 1.0, the input is passed through; higher
										values will speed it up; lower values will slow it
										down. The ratio must be greater than 0.\n
										\n
										Note: I think the JUCE library mis-named this parameter 
										as sampleInPerOutputSample in the ResamplingAudioSource class 
										because the name doesn't match the description. If higher values 
										result in faster sample rate, then the ratio is samples out per 
										input sample.
	*/
	void setResamplingRatio(double samplesOutPerInputSample);


	/** Changes the resampling ratio.

		(This value can be changed at any time, even while the source is running).
		This does the same thing as the overloaded setResamplingRatio() with one parameter 
		except that it lets you specify input and output rates if that's more convenient 
		than setting a ratio.

		@param inputSamplerate
		@param outputSamplerate

	*/
	void setResamplingRatio(double inputSamplerate, double outputSamplerate);


	/** Returns the current resampling ratio.

		This is the value that was set by setResamplingRatio().
	*/
	double getResamplingRatio() const noexcept { return m_ratio; }


	//==============================================================================
	/** @copydoc AudioSource::prepareToPlay() */
	void prepareToPlay(int samplesPerBlockExpected,double sampleRate) override;

	/** @copydoc AudioSource::releaseResources() */
	void releaseResources() override;

	/** @copydoc AudioSource::getNextAudioBlock() */
	void getNextAudioBlock(const AudioSourceChannelInfo&) override;

private:
	//==============================================================================
	void AllocateResources();
	
	//==============================================================================
	OptionalScopedPointer<AudioSource> m_pSource;
	const int m_numChannels;
	SpinLock m_ratioLock;
	double m_ratio{ 0 };
	double m_lastRatio{ 0 };
	AudioSampleBuffer m_sourceBuffer;					///< The buffer used to get data from the input audo source.
	std::vector<const float*> m_sourceBufferPointers;
	std::vector<float*> m_destBufferPointers;

	const int DOUBLE_BUFFER_SIZE = 0x2000;
	struct R8channel
	{
		std::vector<double>	unresampledBuffer;
		std::vector<double>	resampledBuffer;
		double* pUnresampledBuffer;
		double* pResampledBuffer;
		int resampledBufferSamplesLeft{ 0 };

		// This was originally a unique pointer. But to put this structure into a vector means 
		// that it has to be copyable/moveable. Rather than go to the trouble of writing the extra functions
		// I just switched it to a shared pointer for convenience. It's only a slight performance hit 
		// if to caller changes the resampling ratio in the middle, which I don't expect to do.
		ResamplerSPtr pResampler;
	};

	std::vector<R8channel>	m_channels;

};

#endif  // R8BRAINAUDIOSOURCE_H_INCLUDED
