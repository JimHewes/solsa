/*
/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#ifndef INFOPANEL_H_INCLUDED
#define INFOPANEL_H_INCLUDED

#include "SampleFolderDefs.h"
#include "ViewModel.h"
#include "SampleFolder.h"
#include "../JuceLibraryCode/JuceHeader.h"


class MidiKeyboardComponentSolaris : public MidiKeyboardComponent
{
public:

	MidiKeyboardComponentSolaris(SampleFolder* pSampleFolder, ViewModel* pViewModel,
									MidiKeyboardState& state,	Orientation orientation);

	void paint(Graphics& g) override;
	void SetRootKey(uint8_t rootKey) {	m_rootKey = rootKey;	}

protected:
	bool mouseDownOnKey(int midiNoteNumber, const MouseEvent& e) override;
	int GetKeyWidth(uint8_t midiNoteNumber);
	void drawWhiteNote(int midiNoteNumber,
		Graphics& g,
		Rectangle<float> area,
		bool isDown, bool isOver,
		Colour lineColour,
		Colour textColour) override;

	void drawBlackNote(int midiNoteNumber,
		Graphics& g,
		Rectangle<float> area,
		bool isDown, bool isOver,
		Colour noteFillColour) override;
private:
	SampleFolder*		m_pSampleFolder;
	ViewModel*			m_pViewModel;
	ViewModel::SelectionChangeConnection	m_pSelectionConnection;
	SampleFolder::SampleChangeConnection	m_sampleChangeConnection;
	void OnSampleChanged(SampleID sampleID);	///< Called when a sample is modified, including raw file name change..
	void OnSelectionChanged();


	const uint8_t	KEYWIDTH{ 12 };
	uint8_t			m_rootKey{};	// Cached during paint
	uint8_t			m_lowKey{};		// Cached during drag
	uint8_t			m_highKey{};	// Cached during drag

};

class ELabel : public Label
{
public:
	ELabel()
	{
		setColour(Label::ColourIds::backgroundColourId, Colour(230, 230, 230));
		setEditable(true);
	}
};

class IndexLabel : public ELabel
{
public:
	void editorShown(TextEditor *ptextEditor) override
	{
		ptextEditor->setInputRestrictions(3, "0123456789");
	}
};

class InfoPanel :	public Component,
					public Label::Listener
{
public:
	InfoPanel(SampleFolder* pSampleFolder, ViewModel* pViewModel);

	void paint(Graphics& g) override;
	void resized() override;

private:
	SampleFolder*		m_pSampleFolder;
	ViewModel*			m_pViewModel;
	ViewModel::SelectionChangeConnection	m_pSelectionConnection;
	SampleFolder::SampleChangeConnection	m_sampleChangeConnection;
	void OnSampleChanged(SampleID sampleID);
	void OnSelectionChanged();

	// Inherited via Label::Listener
	virtual void labelTextChanged(Label * labelThatHasChanged) override;

	Label	m_poolNameLabel;
	ELabel	m_poolNameEdit;

	Label	m_rawFilenameLabel;
	ELabel	m_rawFilenameEdit;
	Label	m_highKeyLabel;
	ELabel	m_highKeyEdit;
	Label	m_lowKeyLabel;
	ELabel	m_lowKeyEdit;

	Label	m_rootKeyLabel;
	ELabel	m_rootKeyEdit;
	Label	m_fineTuneLabel;
	ELabel	m_fineTuneEdit;

	Label	m_loopStartLabel;
	ELabel	m_loopStartEdit;
	Label	m_loopEndLabel;
	ELabel	m_loopEndEdit;

	//Label	m_sampleIndexLabel;
	Label	m_sampleRateLabel;
	Label	m_sampleLengthLabel;

	MidiKeyboardState m_keyboardState;	// Must appear before m_keyboard so it get constructed first.
	MidiKeyboardComponentSolaris m_keyboard;

	const int leftIndent{ 30 };
	const int rightIndent{ 350 };
	const int labelHeight{ 22 };

};




#endif  // INFOPANEL_H_INCLUDED
