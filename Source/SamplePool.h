/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#ifndef SAMPLEPOOL_H_INCLUDED
#define SAMPLEPOOL_H_INCLUDED

#include <memory>
#include <list>
#include <vector>
#include "SampleFolderDefs.h"
#include "Sample.h"


class SamplePool
{
public:
	static bool VerifyFilename(std::string filename);

	SamplePool(uint16_t index, const std::string& poolFolder, const std::string& poolName = "New Pool");

	SamplePool(const std::string& filepath);
	void Load();
	bool Load(std::istream& inStream);
	void Save();
	void Save(std::ostream& outStream);

	void SetFilePath(std::string poolFilePath);
	std::string GetFilePath() const;
	void SetName(std::string name);
	std::string GetName() const;

//	int GetNumberOfSamples() const;
	PoolID GetPoolID() { return this; }
	uint16_t GetPoolIndex() const { return m_index; }
	void SetPoolIndex(uint16_t index);
	SampleID GetSampleID(size_t sampleIndex);
	std::vector<SampleID> GetSampleIDs() ;
	void AddSample(SampleUPtr&& pSample, uint16_t newIndex);
//	void RemoveSample(Sample* pSample);
//	Sample* GetSample(int index) const;
//	Sample* GetSampleFromSampleIndex(int sampleIndex) const;
	int GetHighestSampleIndex();
	SampleID CreateSample(std::string rawFilename, SampleInfo sampleInfo, bool overwriteRawFile = false, std::string restoreFilePath = "");
	SampleUPtr RemoveSample(SampID sampleID, bool removeRawFile = false);
	void ChangeSampleIndex(SampID sampleID, uint16_t newIndex);

	bool FixupSampleIndices();

	SampleInfo GetSampleInfo(SampID sampID) const;
	void SetSampleInfo(SampID sampID, SampleInfo sampleInfo);
	void SetSampleRawFilename(SampID sampID, const std::string& newRawFilename);
	bool RawFileIsReadable(SampID sampID);
	std::string GetSampleRawFilename(SampID sampID) const;
	std::string GetSampleRawFilepath(SampID& sampID) const;

	Sample*	GetSampleFromID(SampID sampID) const;
private:
	bool SampleIDIsValid(SampID sampID);
	inline void SortSampleList();

	std::string				m_poolFolder;	///< Full path of the pool folder without a trailing separator.
	std::string				m_name;			///< The name of the pool, found in the pool file at the top.
	uint16_t				m_index;		///< The number that is part of the filename:  SamplePool-???.txt
	std::vector<SampleUPtr>	m_sampleList;

};

using SamplePoolUPtr = std::unique_ptr < SamplePool > ;

bool VerifyPoolFilename(const std::string& filename);

#endif  // SAMPLEPOOL_H_INCLUDED
