/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED


#include "SplitComponent.h"
#include <memory>
#include "PoolTreePanel.h"
#include "SampleFolder.h"
#include "ViewModel.h"
#include "../JuceLibraryCode/JuceHeader.h"

class TransportPanel;
using TransportPanelUPtr = std::unique_ptr<TransportPanel>;
class InfoPanel;
using InfoPanelUPtr = std::unique_ptr<InfoPanel>;
class FixedHorizontalSplitPanel;
using FixedHorizontalSplitPanelUPtr = std::unique_ptr<FixedHorizontalSplitPanel>;



//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainContentComponent   :	public Component,
								public ApplicationCommandTarget

{
public:
    //==============================================================================
    MainContentComponent();
    ~MainContentComponent();

    void paint (Graphics&);
    void resized();

	// The following methods support the ApplicationCommandTarget interface
	ApplicationCommandTarget* getNextCommandTarget() override;
	void getAllCommands(Array<CommandID>& commands) override;
	void getCommandInfo(CommandID commandID,ApplicationCommandInfo& result) override;
	bool perform(const InvocationInfo& info) override;

private:
	std::string ChooseAudioFile();
	bool ChooseImportSampleRate(AudioFormat& audioFormat, int numChannels, double sourceSampleRate, double& destSampleRate);
	void DoPoolAdd();
	void DoPoolRemove();
	void DoSampleImport();
	void DoSampleRemove();
	void DoSampleRenameRawfile(const std::string& newRawfileName);
	void DoRangeSetLoop();
	void DoAudioSetup();
	void DoHelpAbout();
	void AudioSetupCallback(int modalresult);
	static void AudioSetupCallbackStatic(int modalresult, MainContentComponent* pMainContentComponent);

	LookAndFeel_V3	m_lookAndFeel;

	std::unique_ptr<MenuBarComponent>	m_pMainMenuComponent;
	std::unique_ptr<MenuBarModel>		m_pMainMenuModel;
	SplitComponent		m_splitComponent;
	PoolTreePanelUPtr	m_pPoolTreePanel;
	TransportPanelUPtr	m_pTransportPanel;
	InfoPanelUPtr		m_pInfoPanel;
	FixedHorizontalSplitPanelUPtr	m_pInfoTransportPanel;
	SampleFolderUPtr	m_pSampleFolder;	///< SampleFolder is essentially the data model
	ViewModelUPtr		m_pViewModel;
	AudioFormatManager	m_audioFormatManager;
	std::unique_ptr<PropertiesFile>	m_pPropertiesFile;
	PropertySet						m_defaultProperties;
	PropertiesFile::Options			m_propertiesOptions;

	const std::string unchangedText = "Unchanged";

	static std::unique_ptr<AudioDeviceManager> m_pSharedAudioDeviceManager;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
