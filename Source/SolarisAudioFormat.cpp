/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#include <string>
#include <memory>
#include <vector>
#include <cassert>
#include "SolarisAudioFormat.h"
#include "SampleFolderDefs.h"

using std::string;
using std::endl;


static const char* const solarisFormatName = "SOLARIS file";
static const int g_headerSizeInBytes{ 32 };

//const char* const SolarisAudioFormat::metaSampleIndexName = "sol sample index";
const char* const SolarisAudioFormat::metaLoopStartName = "sol loop start";
const char* const SolarisAudioFormat::metaLoopEndName = "sol loop end";
const char* const SolarisAudioFormat::metaRootKeyName = "sol root key";
const char* const SolarisAudioFormat::metaFineTuneName = "sol fine tune";
const char* const SolarisAudioFormat::metaLowKeyName = "sol low key";
const char* const SolarisAudioFormat::metaHighKeyName = "sol high key";

StringPairArray SolarisAudioFormat::createMetadata(
	//int sampleIndex,
	int loopStart,
	int loopEnd,
	int rootKey,
	int fineTune,
	int lowKey,
	int highKey)
{
	StringPairArray m;

	//m.set(metaSampleIndexName,String(sampleIndex));
	m.set(metaLoopStartName,String(loopStart));
	m.set(metaLoopEndName,String(loopEnd));
	m.set(metaRootKeyName,String(rootKey));
	m.set(metaFineTuneName,String(fineTune));
	m.set(metaLowKeyName,String(lowKey));
	m.set(metaHighKeyName,String(highKey));

	return m;
}

enum class HeaderIndex : std::int8_t {
	SampleRate = 0,
	SampleLength,
	LoopStart,
	LoopEnd,
	RootKey,
	FineTune,
	LowKey,
	HighKey
};

using HeaderBuffer = std::vector<uint32_t>;

/* Copies the data from a SampleInfo structure to a fixed size header buffer.

Since the header buffer is always 9 * 4  = 36 bytes the stream never changes size from the
point of view of the JUCE AduioFormatReader or AudioFormatWriter classes.
*/
static void CopySampleInfoToHeader(const SampleInfo& sampleInfo, HeaderBuffer& header)
{
	header.resize(g_headerSizeInBytes);	// ensure size is correct
	header[(int)HeaderIndex::SampleRate] = sampleInfo.sampleRate;
	header[(int)HeaderIndex::SampleLength] = sampleInfo.sampleLength;
	header[(int)HeaderIndex::LoopStart] = sampleInfo.loopStart;
	header[(int)HeaderIndex::LoopEnd] = sampleInfo.loopEnd;
	header[(int)HeaderIndex::RootKey] = sampleInfo.rootKey;
	header[(int)HeaderIndex::FineTune] = sampleInfo.fineTune;
	header[(int)HeaderIndex::LowKey] = sampleInfo.lowKey;
	header[(int)HeaderIndex::HighKey] = sampleInfo.highKey;

}

static void CopyHeaderToSampleInfo(const HeaderBuffer& header, SampleInfo& sampleInfo)
{
	sampleInfo.sampleRate = static_cast<uint16_t>(header[(int)HeaderIndex::SampleRate]);
	sampleInfo.sampleLength = header[(int)HeaderIndex::SampleLength];
	sampleInfo.loopStart = header[(int)HeaderIndex::LoopStart];
	sampleInfo.loopEnd = header[(int)HeaderIndex::LoopEnd];
	sampleInfo.rootKey = static_cast<uint8_t>(header[(int)HeaderIndex::RootKey]);
	sampleInfo.fineTune = static_cast<uint8_t>(header[(int)HeaderIndex::FineTune]);
	sampleInfo.lowKey = static_cast<uint8_t>(header[(int)HeaderIndex::LowKey]);
	sampleInfo.highKey = static_cast<uint8_t>(header[(int)HeaderIndex::HighKey]);
}


//==============================================================================
class SolarisAudioFormatReader : public AudioFormatReader
{
public:
	SolarisAudioFormatReader(InputStream* const in)
		: AudioFormatReader(in,solarisFormatName)
	{
		HeaderBuffer header;
		header.resize(g_headerSizeInBytes);
		input->read(&header[0], g_headerSizeInBytes);
		CopyHeaderToSampleInfo(header, m_sampleInfo);

		sampleRate = m_sampleInfo.sampleRate;			// sampleRate is a base class value that must be set.
		lengthInSamples = m_sampleInfo.sampleLength;	// lengthInSamples is a base class value that must be set.
		numChannels = 1;								// numChannels is a base class value that must be set.
		bitsPerSample = 16;								// bitsPerSample is a base class value that must be set.
		metadataValues = SolarisAudioFormat::createMetadata(
			m_sampleInfo.loopStart,
			m_sampleInfo.loopEnd,
			m_sampleInfo.rootKey,
			m_sampleInfo.fineTune,
			m_sampleInfo.lowKey,
			m_sampleInfo.highKey);
		m_sampleDataStart = input->getPosition();
	}

	//==============================================================================
	bool readSamples(int* const* destSamples,int numDestChannels,int startOffsetInDestBuffer,
		int64 startSampleInFile,int numSamples) override
	{
		
		clearSamplesBeyondAvailableLength(destSamples,numDestChannels,startOffsetInDestBuffer,
			startSampleInFile,numSamples,lengthInSamples);

		if (numSamples <= 0)
			return true;

		input->setPosition(m_sampleDataStart + startSampleInFile * bytesPerFrame);

		while (numSamples > 0)
		{
			const int tempBufSize = 480 * 3 * 4; // (keep this a multiple of 3)
			char tempBuffer[tempBufSize];

			const int numThisTime = jmin(tempBufSize / bytesPerFrame,numSamples);
			const int bytesRead = input->read(tempBuffer,numThisTime * bytesPerFrame);

			if (bytesRead < numThisTime * bytesPerFrame)
			{
				jassert(bytesRead >= 0);
				zeromem(tempBuffer + bytesRead,(size_t)(numThisTime * bytesPerFrame - bytesRead));
			}


			ReadHelper<AudioData::Int32,AudioData::Int16,AudioData::LittleEndian>::read(destSamples,startOffsetInDestBuffer,numDestChannels,tempBuffer,numChannels,numThisTime);

			startOffsetInDestBuffer += numThisTime;
			numSamples -= numThisTime;
		}

		return true;
	}

	const int	bytesPerFrame = 2;
	int64		m_sampleDataStart{ 0 };

private:
	SampleInfo	m_sampleInfo;

};

class SolarisAudioFormatWriter : public AudioFormatWriter
{
public:
	SolarisAudioFormatWriter(OutputStream* const out,const double rate,
		const unsigned int numChans,const unsigned int bits,
		const StringPairArray& metadataValues)
		: AudioFormatWriter(out,solarisFormatName,rate,numChans,bits)
	{
		assert(numChans == 1);
		assert(bits == 16);

		headerPosition = out->getPosition();

		//m_sampleIndex = metadataValues[SolarisAudioFormat::metaSampleIndexName].getIntValue();
		m_sampleInfo.sampleRate = static_cast<uint16_t>(rate);
		m_sampleInfo.loopStart = metadataValues[SolarisAudioFormat::metaLoopStartName].getIntValue();
		m_sampleInfo.loopEnd = metadataValues[SolarisAudioFormat::metaLoopEndName].getIntValue();
		m_sampleInfo.rootKey = static_cast<uint8_t>(metadataValues[SolarisAudioFormat::metaRootKeyName].getIntValue());
		m_sampleInfo.fineTune = static_cast<uint8_t>(metadataValues[SolarisAudioFormat::metaFineTuneName].getIntValue());
		m_sampleInfo.lowKey = static_cast<uint8_t>(metadataValues[SolarisAudioFormat::metaLowKeyName].getIntValue());
		m_sampleInfo.highKey = static_cast<uint8_t>(metadataValues[SolarisAudioFormat::metaHighKeyName].getIntValue());

		writeHeader();
	}

	~SolarisAudioFormatWriter()
	{
		writeHeader();
	}

	bool write(const int** data,int numSamples) override
	{
		assert(numSamples >= 0);
		assert(data != nullptr && *data != nullptr); // the input must contain at least one channel!

		if (writeFailed)
			return false;

		MemoryBlock tempBlock;
		const size_t bytes = numChannels * (unsigned int)numSamples * bitsPerSample / 8;
		tempBlock.ensureSize(bytes,false);

		// Convert to 16 bits
		WriteHelper<AudioData::Int16,AudioData::Int32,AudioData::LittleEndian>::write(tempBlock.getData(),(int)numChannels,data,numSamples);
		
		if (!output->write(tempBlock.getData(),bytes))
		{
			// failed to write to disk.
			writeFailed = true;
			return false;
		}

		bytesWritten += bytes;
		m_sampleInfo.sampleLength += (uint64)numSamples;
		return true;
	}

	bool flush() override
	{
		const int64 lastWritePos = output->getPosition();
		writeHeader();

		if (output->setPosition(lastWritePos))
			return true;

		// if this fails, you've given it an output stream that can't seek! It needs
		// to be able to seek back to write the header
		jassertfalse;
		return false;
	}

private:

	void writeHeader()
	{
		output->setPosition(headerPosition);

		HeaderBuffer header;
		CopySampleInfoToHeader(m_sampleInfo, header);
		output->write(&header[0], g_headerSizeInBytes);
	}

	int64 headerPosition;
	uint64 bytesWritten{ 0 };
	bool writeFailed{ false };

	SampleInfo	m_sampleInfo;
};


SolarisAudioFormat::SolarisAudioFormat() : AudioFormat(solarisFormatName,".txt") {}
SolarisAudioFormat::~SolarisAudioFormat() {}

Array<int> SolarisAudioFormat::getPossibleSampleRates()
{
	const int rates[] = { 8000,11025,12000,16000,22050,32000,44100,	48000,88200,96000};

	return Array<int>(rates,numElementsInArray(rates));
}

Array<int> SolarisAudioFormat::getPossibleBitDepths()
{
	const int depths[] = { 16 };

	return Array<int>(depths,numElementsInArray(depths));
}

bool SolarisAudioFormat::canDoStereo()
{
	return false;
}

bool SolarisAudioFormat::canDoMono()
{
	return true;
}

//==============================================================================
AudioFormatReader* SolarisAudioFormat::createReaderFor(InputStream* sourceStream,
	bool deleteStreamIfOpeningFails)
{
	std::unique_ptr<SolarisAudioFormatReader> r = std::make_unique< SolarisAudioFormatReader>(sourceStream);

	if (r->sampleRate > 0 && r->numChannels > 0 && r->bytesPerFrame > 0)
	{
		return r.release();
	}

	if (!deleteStreamIfOpeningFails)
	{
		r->input = nullptr;
	}

	return nullptr;

}

AudioFormatWriter* SolarisAudioFormat::createWriterFor(OutputStream* streamToWriteTo,
	double sampleRateToUse,
	unsigned int numberOfChannels,
	int bitsPerSample,
	const StringPairArray& metadataValues,
	int qualityOptionIndex)
{
	qualityOptionIndex;
	if (getPossibleBitDepths().contains(bitsPerSample))
	{
		return new SolarisAudioFormatWriter(streamToWriteTo,sampleRateToUse,numberOfChannels,
			bitsPerSample,metadataValues);
	}
	return nullptr;
}