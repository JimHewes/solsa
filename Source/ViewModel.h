/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#ifndef VIEWMODEL_H_INCLUDED
#define VIEWMODEL_H_INCLUDED

#include <memory>
#include <map>
#include "Notifier.h"
#include "SampleFolderDefs.h"

class SampleFolder;

struct ViewData
{
	double thumbnailScrollPosition{ 0 };
	double zoomLevel{ 0 };
	double playPosition{ 0 };
	bool followTransport{ false };
	bool loopOn{ false };
};

class ViewModel
{
public:
	ViewModel(SampleFolder* pSampleFolder)
		:m_pSampleFolder(pSampleFolder) {}

	void SetThumbnailScrollPosition(SampleID sampleID, double position);
	void SetThumbnailZoomLevel(SampleID sampleID, double level);
	void SetPlayPosition(SampleID sampleID, double position);
	void SetFollowTransport(SampleID sampleID, bool follow);
	void SetLoopOn(SampleID sampleID, bool loopOn);

	double GetThumbnailScrollPosition(SampleID sampleID);
	double GetThumbnailZoomLevel(SampleID sampleID);
	double GetPlayPosition(SampleID sampleID);
	bool GetFollowTransport(SampleID sampleID);
	bool GetLoopOn(SampleID sampleID);

	void VerifyData();


	/** @name Pool/Sample Selection Functions
		These functions are related to selecting pools and samples.
	*/
	///@{
	void Select(PoolID poolID);
	void Select(SampleID sampleID);
	void ClearSelection();
	bool HasPoolSelection() const throw();
	bool HasSampleSelection() const throw();
	bool HasSelection() const throw();

	PoolID GetSelectedPoolID() const throw()
	{
		return m_selectedPoolID;
	}
	SampleID GetSelectedSampleID() const throw()
	{
		return m_selectedSampleID;
	}

	Notifier<void(void)> SelectionChangeEvent;
	using SelectionChangeConnection = decltype(SelectionChangeEvent)::ConnectionType;	///< SelectionChangeEvent.Connect() returns this type
	///@}

private:

	std::map<SampleID, ViewData> m_viewdata;
	SampleFolder* m_pSampleFolder;


	/** @name Pool/Sample Selection members.
		These members are related to selecting pools and samples.
	*/
	///@{	std::string m_selectedPoolFilePath;
	int			m_selectedSampleIndex{ -1 };

	PoolID		m_selectedPoolID{ nullptr };
	SampleID	m_selectedSampleID;
	enum class  SelectionType : uint8_t
	{
		None, Pool, Sample
	} m_selectionType{ SelectionType::None };
	///@}

};
using ViewModelUPtr = std::unique_ptr < ViewModel >;


#endif  // VIEWMODEL_H_INCLUDED
