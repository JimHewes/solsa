/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#include <algorithm>
#include "ViewModel.h"
#include "SampleFolder.h"

void ViewModel::SetThumbnailScrollPosition(SampleID sampleID, double position)
{
	//m_viewdata.insert(std::make_pair(sampleID, position));
	m_viewdata[sampleID].thumbnailScrollPosition = position;
}

void ViewModel::SetThumbnailZoomLevel(SampleID sampleID, double level)
{
	m_viewdata[sampleID].zoomLevel = level;
}

void ViewModel::SetPlayPosition(SampleID sampleID, double position)
{
	m_viewdata[sampleID].playPosition = position;
}

void ViewModel::SetFollowTransport(SampleID sampleID, bool follow)
{
	m_viewdata[sampleID].followTransport = follow;
}

void ViewModel::SetLoopOn(SampleID sampleID, bool loopOn)
{
	m_viewdata[sampleID].loopOn = loopOn;
}

double ViewModel::GetThumbnailScrollPosition(SampleID sampleID)
{
	return m_viewdata[sampleID].thumbnailScrollPosition;
}

double ViewModel::GetThumbnailZoomLevel(SampleID sampleID)
{
	return m_viewdata[sampleID].zoomLevel;
}

double ViewModel::GetPlayPosition(SampleID sampleID)
{
	return m_viewdata[sampleID].playPosition;
}

bool ViewModel::GetFollowTransport(SampleID sampleID)
{
	return m_viewdata[sampleID].followTransport;
}

bool ViewModel::GetLoopOn(SampleID sampleID)
{
	return m_viewdata[sampleID].loopOn;
}


/** Removes data (so far only sample) that no longer exist in the model*/
void ViewModel::VerifyData()
{
	// It would be nice if we could use the standard algorithm std::remove_if but doesn't work for maps. 
	// It wants to do an assignment to an element. But in a map, elements are pairs where the key is const. 
	// So you cannot assign to an element. This won't compile:
	// std::remove_if(m_viewdata.begin(), m_viewdata.end(), [this](auto& vd) { return  !m_pSampleFolder->SampleExists(vd.first);   });

	// So you have to do it this way, which seems to be an accepted methos on newsgroups:
	for (auto iter = m_viewdata.begin(); iter != m_viewdata.end(); ) 
	{
		if (!m_pSampleFolder->SampleExists(iter->first))
		{
			// Only iterators referencing the erased element are invalidated. Other iterators are still valid.
			iter = m_viewdata.erase(iter);	// in C++11, erase returns the next iter.
		}
		else 
		{
			++iter;
		}
	}
}



void ViewModel::Select(PoolID poolID)
{
	if (m_selectionType != SelectionType::Pool || m_selectedPoolID != poolID)
	{
		m_selectedPoolID = poolID;
		m_selectionType = SelectionType::Pool;
		SelectionChangeEvent.Notify();
	}
}

void ViewModel::Select(SampleID sampleID)
{
	if (m_selectionType != SelectionType::Sample || m_selectedSampleID != sampleID)
	{
		m_selectedSampleID = sampleID;
		m_selectionType = SelectionType::Sample;
		SelectionChangeEvent.Notify();
	}
}

void ViewModel::ClearSelection()
{
	if (m_selectionType != SelectionType::None)	// if not already clear
	{
		m_selectionType = SelectionType::None;
		SelectionChangeEvent.Notify();
	}
}

bool ViewModel::HasPoolSelection() const throw()
{
	return m_selectionType == SelectionType::Pool;
}

bool ViewModel::HasSampleSelection() const throw()
{
	return m_selectionType == SelectionType::Sample;
}

bool ViewModel::HasSelection() const throw()
{
	return m_selectionType != SelectionType::None;
}

