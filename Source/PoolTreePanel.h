/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#ifndef POOLTREEPANEL_H_INCLUDED
#define POOLTREEPANEL_H_INCLUDED
#include "../JuceLibraryCode/JuceHeader.h"
#include <memory>
#include "PoolTreeView.h"
#include "SampleFolder.h"
#include "Notifier.h"

class PoolSelectionModel;

class PoolTreePanel :	public Component,
						public Button::Listener,
						public ComboBox::Listener
{
public:

	PoolTreePanel(SampleFolder* pSampleFolder, PropertiesFile* pPropertiesFile, ViewModel* pViewModel);
	~PoolTreePanel();
	void resized() override;
	void OnSampleFolderChanged();

	void ChoosePoolFolder();

	/** Called when a button is clicked. */
	void buttonClicked(Button*) override;

	/** Called when a ComboBox has its selected item changed. */
	void comboBoxChanged(ComboBox* comboBoxThatHasChanged) override;

	//Notifier<void(const std::string&)> FolderPathChangeEvent;
	//using FolderPathChangeConnection = decltype(FolderPathChangeEvent)::ConnectionType;	///< FolderPathChangeEvent.Connect() returns this type

private:
	void RefreshPathViewHistory();

	PoolTreeViewUPtr		m_pPoolTreeView;
	TextButton				m_browseButton;
	ComboBox				m_pathView;
	PropertiesFile*			m_pPropertiesFile{ nullptr };
	SampleFolder*			m_pSampleFolder{ nullptr };

	SampleFolder::FolderChangeConnection m_folderChangeConnection;
};

using PoolTreePanelUPtr = std::unique_ptr < PoolTreePanel >;


#endif  // POOLTREEPANEL_H_INCLUDED
