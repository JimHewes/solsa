/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#ifndef APPCMDMGR_H_INCLUDED
#define APPCMDMGR_H_INCLUDED


#include "../JuceLibraryCode/JuceHeader.h"
#include <memory>

extern std::unique_ptr < ApplicationCommandManager > g_pAppCommandManager;

enum CommandIDs
{
	PoolAdd = 0x2000,
	PoolRemove = 0x2001,
	PoolChooseFolder = 0x2002,
	SampleImport = 0x2010,
	SampleRemove = 0x2011,
	SampleResample = 2012,
	EditSetLoopToRange = 2020,
	EditCut = 0x2021,
	EditCopy = 0x2022,
	EditPaste = 0x2023,
	AudioSetup = 2030,
	HelpAbout = 0x2040
};



#endif  // APPCMDMGR_H_INCLUDED
