/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#include <algorithm>
#include <cassert>
#include <cstdint>
#include <iomanip>
#include <string>
#include <sstream>
#include <utility>
#include "InfoPanel.h"
#include "SampleFolder.h"

using std::string;

static const char* pPoolFilenameLabelText = "Pool Filename: SamplePool-";
static const char* pPoolFilenameLabelText2 = ".txt";
static const char* pPoolNameLabelText = "Pool Name:";
static const char* pRawFilenameLabelText = "Raw Filename:";
static const char* pSampleIndexLabelText = "Sample index: ";
static const char* pSampleRateLabelText = "Sample rate: ";
static const char* pSampleLengthLabelText = "Sample length: ";
static const char* pHighKeyLabelText = "High Key:";
static const char* pLowKeyLabelText = "Low Key:";
static const char* pRootKeyLabelText = "Root Key:";
static const char* pFineTuneLabelText = "Fine Tune:";
static const char* pLoopStartLabelText = "Loop Start:";
static const char* pLoopEndLabelText = "Loop End:";

InfoPanel::InfoPanel(SampleFolder* pSampleFolder, ViewModel* pViewModel)
	: m_pSampleFolder(pSampleFolder)
	, m_pViewModel(pViewModel)
	, m_keyboard(pSampleFolder, pViewModel, m_keyboardState, MidiKeyboardComponent::horizontalKeyboard)
{
	int ypos = 30;
	// Add 4 because it looks better (like the old call to getStringWidthFloat()) and add 0.5 for rounding up.
	int w = static_cast<int>(TextLayout::getStringWidth(m_rawFilenameLabel.getFont(), pRawFilenameLabelText) + 4.5);
	m_rawFilenameLabel.setBounds(leftIndent, ypos, w, labelHeight);
	m_rawFilenameLabel.setText(pRawFilenameLabelText, NotificationType::dontSendNotification);
	addAndMakeVisible(m_rawFilenameLabel);

	m_rawFilenameEdit.setBounds(m_rawFilenameLabel.getWidth() + 30, ypos, 184, labelHeight);
	m_rawFilenameEdit.addListener(this);
	addAndMakeVisible(m_rawFilenameEdit);

	// Pool controls
	w = static_cast<int>(TextLayout::getStringWidth(m_poolNameLabel.getFont(), pPoolNameLabelText) + 4.5);
	m_poolNameLabel.setBounds(leftIndent, ypos, w, labelHeight);
	m_poolNameLabel.setText(pPoolNameLabelText, NotificationType::dontSendNotification);
	addAndMakeVisible(m_poolNameLabel);

	m_poolNameEdit.setBounds(m_poolNameLabel.getRight(), ypos, 160, labelHeight);
	m_poolNameEdit.addListener(this);
	addAndMakeVisible(m_poolNameEdit);

	ypos += 36;

	w = static_cast<int>(TextLayout::getStringWidth(m_lowKeyLabel.getFont(), pLowKeyLabelText) + 4.5);
	m_lowKeyLabel.setBounds(leftIndent, ypos, w, labelHeight);
	m_lowKeyLabel.setText(pLowKeyLabelText, NotificationType::dontSendNotification);
	addAndMakeVisible(m_lowKeyLabel);
	m_lowKeyEdit.setBounds(100, ypos, 50, labelHeight);
	m_lowKeyEdit.addListener(this);
	addAndMakeVisible(m_lowKeyEdit);

	w = static_cast<int>(TextLayout::getStringWidth(m_highKeyLabel.getFont(), pHighKeyLabelText) + 4.5);
	m_highKeyLabel.setBounds(190, ypos, w, labelHeight);
	m_highKeyLabel.setText(pHighKeyLabelText, NotificationType::dontSendNotification);
	addAndMakeVisible(m_highKeyLabel);
	m_highKeyEdit.setBounds(260, ypos, 50, labelHeight);
	m_highKeyEdit.addListener(this);
	addAndMakeVisible(m_highKeyEdit);

	m_sampleRateLabel.setBounds(rightIndent, ypos, 160, labelHeight);
	m_sampleRateLabel.setText(pSampleRateLabelText, NotificationType::dontSendNotification);
	addAndMakeVisible(m_sampleRateLabel);

	ypos += 26;

	w = static_cast<int>(TextLayout::getStringWidth(m_rootKeyLabel.getFont(), pRootKeyLabelText) + 4.5);
	m_rootKeyLabel.setBounds(leftIndent, ypos, w, labelHeight);
	m_rootKeyLabel.setText(pRootKeyLabelText, NotificationType::dontSendNotification);
	addAndMakeVisible(m_rootKeyLabel);
	m_rootKeyEdit.setBounds(100, ypos, 50, labelHeight);
	m_rootKeyEdit.addListener(this);
	addAndMakeVisible(m_rootKeyEdit);

	w = static_cast<int>(TextLayout::getStringWidth(m_fineTuneLabel.getFont(), pFineTuneLabelText) + 4.5);
	m_fineTuneLabel.setBounds(190, ypos, w, labelHeight);
	m_fineTuneLabel.setText(pFineTuneLabelText, NotificationType::dontSendNotification);
	addAndMakeVisible(m_fineTuneLabel);
	m_fineTuneEdit.setBounds(260, ypos, 50, labelHeight);
	m_fineTuneEdit.addListener(this);
	addAndMakeVisible(m_fineTuneEdit);

	m_sampleLengthLabel.setBounds(rightIndent, ypos, 180, labelHeight);
	m_sampleLengthLabel.setText(pSampleLengthLabelText, NotificationType::dontSendNotification);
	addAndMakeVisible(m_sampleLengthLabel);


	ypos += 26;

	w = static_cast<int>(TextLayout::getStringWidth(m_loopStartLabel.getFont(), pLoopStartLabelText) + 4.5);
	m_loopStartLabel.setBounds(leftIndent, ypos, w, labelHeight);
	m_loopStartLabel.setText(pLoopStartLabelText, NotificationType::dontSendNotification);
	addAndMakeVisible(m_loopStartLabel);
	m_loopStartEdit.setBounds(100, ypos, 80, labelHeight);
	m_loopStartEdit.addListener(this);
	addAndMakeVisible(m_loopStartEdit);

	w = static_cast<int>(TextLayout::getStringWidth(m_loopEndLabel.getFont(), pLoopEndLabelText) + 4.5);
	m_loopEndLabel.setBounds(190, ypos, w, labelHeight);
	m_loopEndLabel.setText(pLoopEndLabelText, NotificationType::dontSendNotification);
	addAndMakeVisible(m_loopEndLabel);
	m_loopEndEdit.setBounds(260, ypos, 80, labelHeight);
	m_loopEndEdit.addListener(this);
	addAndMakeVisible(m_loopEndEdit);

	//m_keyboard.setBounds(leftIndent, 150, 9 * 7 * 10 + 6, 56);

	addAndMakeVisible(m_keyboard);

	m_pSelectionConnection = m_pViewModel->SelectionChangeEvent.Connect([this]() {	OnSelectionChanged(); });

	m_sampleChangeConnection = m_pSampleFolder->SampleChangeEvent.Connect([this](SampleID sampleID) {	OnSampleChanged(sampleID); });

	OnSelectionChanged();
}

void InfoPanel::paint(Graphics& g)
{
	g.setColour(Colours::darkgrey);
	g.drawRect(getLocalBounds(), 1);
}

void InfoPanel::resized()
{
	int width = getWidth() - 2 * leftIndent;
	width = std::min(width, (10 * 7 + 5) * (int)m_keyboard.getKeyWidth());
	width = std::max(width, (2 * 7 + 1) * (int)m_keyboard.getKeyWidth());
	m_keyboard.setBounds(leftIndent, 150, width, 56);
}

static string g_keyNames[12] = { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" };


/** Converts a standard MIDI note number to a key name such as C4 or G#5.

	Based on looking at the pool files that John Bowen created, the Solaris apparently uses the less common 
	notation of C4 for middle C instead of the more common C3. It doesn't matter much though because MIDI 
	note number 60 is always middle C (whatever you want to call that) and middle C is always the C key under 
	the graphical display on the Solaris.

	@param[in] noteNumber	The MIDI note number in the range 0 to 127 inclusive.
	@returns				The text representation of the key name.
*/
static string MidiNoteNumberToKeyName(uint8_t noteNumber)
{

	assert(noteNumber < 128);
	std::ostringstream os;
	os << g_keyNames[noteNumber % 12] << ((int)noteNumber / 12) - 1;
	return os.str();
}


/** @brief Convert a key name, such as C4 or G#5, to its standard MIDI note number.
	@param[in] key	A string representing the key name. To be parsed correctly, it must consist of 
					two to four characters not including spaces. The first character must be an upper 
					or lowercase letter in the range A to G. The second character is an optional # or b. 
					The third character is an optional negative sign (or plus sign). The fourth character 
					is an integer in the range 1 to 8.
	@returns		a Midi note number in the range 0 to 127, or 255 if the string could not be parsed.
*/
static uint8_t KeyNameToMidiNoteNumber(string keyName)
{
	// remove white space
	keyName.erase(std::remove_if(keyName.begin(), keyName.end(), ::isspace), keyName.end());
	//assert(keyName.length() < 5);

	size_t index = 0;
	int midiNote = 0;
	std::transform(keyName.begin(), keyName.end(), keyName.begin(), [](unsigned char c) { return static_cast<char>(std::toupper(c)); });
	if (keyName[index] >= 'A' || keyName[index] <= 'G')
	{
		int i;
		for (i = 0; i < sizeof(g_keyNames); ++i)
		{
			if (keyName[index] == g_keyNames[i][0])
				break;
		}
		midiNote = i;
	}
	else
	{
		return 255;
	}
	index++;
	if (keyName[index] == '#')
	{
		++midiNote;
		++index;
	}
	else if (keyName[index] == 'b')
	{
		--midiNote;
		++index;
	}
	if (index >= keyName.size())
	{
		return 255;
	}
	try
	{
		// stoi may throw an exception
		int val = std::stoi(keyName.substr(index));
		if (val < -2 || val > 8)
		{
			return 255;
		}
		midiNote += (val + 1) * 12;
	}
	catch (...)
	{
		return 255;
	}
	if (midiNote < 0 || midiNote > 127)
	{
		return 255;
	}

	return static_cast<uint8_t>(midiNote);
}

void InfoPanel::OnSelectionChanged()
{
	bool poolIsSelected = m_pViewModel->HasPoolSelection();
	bool sampleIsSelected = m_pViewModel->HasSampleSelection();
	m_poolNameLabel.setVisible(poolIsSelected);
	m_poolNameEdit.setVisible(poolIsSelected);

	m_rawFilenameLabel.setVisible(sampleIsSelected);
	m_rawFilenameEdit.setVisible(sampleIsSelected);
	m_highKeyLabel.setVisible(sampleIsSelected);
	m_highKeyEdit.setVisible(sampleIsSelected);
	m_lowKeyLabel.setVisible(sampleIsSelected);
	m_lowKeyEdit.setVisible(sampleIsSelected);
	m_rootKeyLabel.setVisible(sampleIsSelected);
	m_rootKeyEdit.setVisible(sampleIsSelected);
	m_fineTuneLabel.setVisible(sampleIsSelected);
	m_fineTuneEdit.setVisible(sampleIsSelected);
	m_loopStartLabel.setVisible(sampleIsSelected);
	m_loopStartEdit.setVisible(sampleIsSelected);
	m_loopEndLabel.setVisible(sampleIsSelected);
	m_loopEndEdit.setVisible(sampleIsSelected);
	m_sampleRateLabel.setVisible(sampleIsSelected);
	m_sampleLengthLabel.setVisible(sampleIsSelected);
	m_keyboard.setVisible(sampleIsSelected);

	// The selection could be either a sample or a pool. So check for Sample.
	if (sampleIsSelected)
	{
		SampleID selectedSampleID = m_pViewModel->GetSelectedSampleID();
		SampleInfo info = m_pSampleFolder->GetSampleInfo(selectedSampleID);

		m_rawFilenameEdit.setText(m_pSampleFolder->GetSampleRawFilename(selectedSampleID), NotificationType::dontSendNotification);

		std::stringstream os;
		os << pSampleRateLabelText << (int)info.sampleRate;
		m_sampleRateLabel.setText(os.str(), NotificationType::dontSendNotification);

		os.str("");
		os << pSampleLengthLabelText << (int)info.sampleLength;
		m_sampleLengthLabel.setText(os.str(), NotificationType::dontSendNotification);

		m_highKeyEdit.setText(MidiNoteNumberToKeyName(info.highKey), NotificationType::dontSendNotification);
		m_lowKeyEdit.setText(MidiNoteNumberToKeyName(info.lowKey), NotificationType::dontSendNotification);
		m_rootKeyEdit.setText(MidiNoteNumberToKeyName(info.rootKey), NotificationType::dontSendNotification);

		os.str("");
		os << (int)info.fineTune;
		m_fineTuneEdit.setText(os.str(), NotificationType::dontSendNotification);

		os.str("");
		os << (int)info.loopStart;
		m_loopStartEdit.setText(os.str(), NotificationType::dontSendNotification);

		os.str("");
		os << (int)info.loopEnd;
		m_loopEndEdit.setText(os.str(), NotificationType::dontSendNotification);
		
	}
	else if (poolIsSelected)
	{
		PoolID selectedPoolID = m_pViewModel->GetSelectedPoolID();
		m_poolNameEdit.setText(m_pSampleFolder->GetPoolName(selectedPoolID), NotificationType::dontSendNotification);
	}

}


void InfoPanel::labelTextChanged(Label * labelThatHasChanged)
{
	bool poolIsSelected = m_pViewModel->HasPoolSelection();
	bool sampleIsSelected = m_pViewModel->HasSampleSelection();

	string newText = labelThatHasChanged->getText().toStdString();
	//int value = atoi(labelThatHasChanged->getText().toStdString().c_str());

	if (sampleIsSelected)
	{
		SampleID selectedSampleID = m_pViewModel->GetSelectedSampleID();
		SampleInfo info = m_pSampleFolder->GetSampleInfo(selectedSampleID);
		if (labelThatHasChanged == &m_rawFilenameEdit)
		{
			m_pSampleFolder->SetSampleRawFilename(selectedSampleID, labelThatHasChanged->getText().toStdString());
		}
		else if (labelThatHasChanged == &m_highKeyEdit)
		{
			uint8_t newValue = KeyNameToMidiNoteNumber(newText);
			if (newValue != 255)
			{
				info.highKey = newValue;
			}
			info.lowKey = std::min(info.highKey, info.lowKey);
		}
		else if (labelThatHasChanged == &m_lowKeyEdit)
		{
			uint8_t newValue = KeyNameToMidiNoteNumber(newText);
			if (newValue != 255)
			{
				info.lowKey = newValue;
			}
			info.highKey = std::max(info.highKey, info.lowKey);
		}
		else if (labelThatHasChanged == &m_rootKeyEdit)
		{
			uint8_t newValue = KeyNameToMidiNoteNumber(newText);
			if (newValue != 255)
			{
				info.rootKey = newValue;
			}
		}
		else if (labelThatHasChanged == &m_fineTuneEdit)
		{
			int newValue = 0;
			try
			{
				newValue = stoi(newText);	// may throw
				if (newValue >= 0 || newValue < 256)
				{
					info.fineTune = static_cast<uint8_t>(newValue);
				}
			}
			catch (...)
			{}
		}
		else if (labelThatHasChanged == &m_loopStartEdit)
		{
			try
			{
				auto value = std::stoul(newText);	// may throw

				uint32_t constrainedValue = (value < 0) ? 0 : ((value > info.sampleLength) ? info.sampleLength : value);
				if (value != constrainedValue)
				{
					std::stringstream os;
					os << constrainedValue;
					m_loopStartEdit.setText(os.str(), NotificationType::dontSendNotification);
				}
				if (info.loopStart != constrainedValue)
				{
					info.loopStart = constrainedValue;
				}
			}
			catch (...)
			{
				std::stringstream os;
				os << info.loopStart;
				m_loopStartEdit.setText(os.str(), NotificationType::dontSendNotification);
			}
		}
		else if (labelThatHasChanged == &m_loopEndEdit)
		{
			try
			{
				auto value = std::stoul(newText);	// may throw

				uint32_t constrainedValue = (value > info.sampleLength) ? info.sampleLength : value;
				if (value != constrainedValue)
				{
					std::stringstream os;
					os << constrainedValue;
					m_loopEndEdit.setText(os.str(), NotificationType::dontSendNotification);
				}

				if (info.loopEnd != (uint32_t)constrainedValue)
				{
					info.loopEnd = constrainedValue;
				}
			}
			catch (...)
			{
				std::stringstream os;
				os << info.loopEnd;
				m_loopEndEdit.setText(os.str(), NotificationType::dontSendNotification);
			}
		}

		try 
		{
			m_pSampleFolder->SetSampleInfo(selectedSampleID, info);
		}
		catch (std::exception e)
		{
			AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon, "Solsa Error", e.what(), "OK");
			OnSelectionChanged();
		}

	}
	else if (poolIsSelected)
	{
		PoolID selectedPoolID = m_pViewModel->GetSelectedPoolID();
		if (labelThatHasChanged == &m_poolNameEdit)
		{
			m_pSampleFolder->SetPoolName(selectedPoolID, m_poolNameEdit.getText().toStdString());
		}
	}
}

/** Called when a sample is modified, including raw file name change.. */
void InfoPanel::OnSampleChanged(SampleID sampleID)
{
	sampleID;
	OnSelectionChanged();
}

/** @brief Creates a MidiKeyboardComponent tailored for the Solaris Sample Editor.

	@param state        the midi keyboard model that this component will represent
	@param orientation  whether the keyboard is horizonal or vertical
*/
MidiKeyboardComponentSolaris::MidiKeyboardComponentSolaris(SampleFolder* pSampleFolder, ViewModel* pViewModel,
					MidiKeyboardState & state, Orientation orientation)
	:MidiKeyboardComponent(state, orientation)
	, m_pSampleFolder(pSampleFolder)
	, m_pViewModel(pViewModel)
{
	setLowestVisibleKey(12);
	setScrollButtonsVisible(true);
	setKeyWidth(KEYWIDTH);
	setOctaveForMiddleC(4);		// Solaris designates C4 as middle C (note number 60). This is not the most common.

	m_pSelectionConnection = m_pViewModel->SelectionChangeEvent.Connect([this]() {	OnSelectionChanged(); });
	m_sampleChangeConnection = m_pSampleFolder->SampleChangeEvent.Connect([this](SampleID sampleID) {	OnSampleChanged(sampleID); });

}

bool IsBlackKey(uint8_t midiNoteNumber)
{
	uint8_t note = midiNoteNumber % 12;
	return (note == 1) || (note == 3) || (note == 6) || (note == 8) || (note == 10);
}

int MidiKeyboardComponentSolaris::GetKeyWidth(uint8_t midiNoteNumber)
{
	uint8_t w = (uint8_t)getKeyWidth();
	return IsBlackKey(midiNoteNumber) ? uint8_t((w * 0.7f) + 0.5f) : w;
}

void MidiKeyboardComponentSolaris::paint(Graphics & g)
{
	MidiKeyboardComponent::paint(g);

	// Draw left overlay. It marks the area to the left of the physical keys of the Solaris.
	int physicalKeyStart = static_cast<int>(getKeyStartPosition(36));
	g.setColour(Colours::grey);
	g.setOpacity(0.5f);
	Rectangle<int> rectLeft(0, 0, physicalKeyStart, getHeight());
	g.fillRect(rectLeft);

	// Draw right overlay. It marks the area to the right of the physical keys of the Solaris.
	int x = physicalKeyStart + ((5 * 7 + 1) * KEYWIDTH);
	Rectangle<int> rectRight(x, 0, getWidth() - x, getHeight());
	g.setColour(Colours::grey);
	g.setOpacity(0.5f);
	g.fillRect(rectRight);

	// Draw key range overlay. 
	g.setColour(Colours::blue);
	g.setOpacity(0.4f);
	float lowKeyStartPos = std::max(0.0f, getKeyStartPosition(m_lowKey));
	float highKeyStartPos = std::min(static_cast<float>(getWidth()), getKeyStartPosition(m_highKey) + GetKeyWidth(m_highKey));
	int width = std::max(0, static_cast<int>(highKeyStartPos - lowKeyStartPos));
	if (width > 0)
	{
		Rectangle<int> rectRange(static_cast<int>(lowKeyStartPos), 0, width, getHeight());
		g.drawRect(rectRange, 3);
	}
}

bool MidiKeyboardComponentSolaris::mouseDownOnKey(int midiNoteNumber, const MouseEvent & e)
{
	bool cmdDown = e.mods.isCommandDown();
	bool sftDown = e.mods.isShiftDown();
	if (cmdDown || sftDown)
	{
		SampleID selectedSampleID = m_pViewModel->GetSelectedSampleID();
		SampleInfo info = m_pSampleFolder->GetSampleInfo(selectedSampleID);
		if (cmdDown && sftDown)
		{
			info.rootKey = (uint8_t)midiNoteNumber;
			m_pSampleFolder->SetSampleInfo(selectedSampleID, info);
			repaint();
		}
		else if (cmdDown)
		{
			info.lowKey = (uint8_t)midiNoteNumber;
			info.highKey = std::max(info.highKey, info.lowKey);
			m_pSampleFolder->SetSampleInfo(selectedSampleID, info);
			repaint();
		}
		else if (sftDown)
		{
			info.highKey = (uint8_t)midiNoteNumber;
			info.lowKey = std::min(info.lowKey, info.highKey);
			m_pSampleFolder->SetSampleInfo(selectedSampleID, info);
			repaint();
		}
	}

	return false;	// returning false means that the MIDI note will not be played by JUCE
}

//==============================================================================
/** @brief Draws a white note in the given rectangle.

	isOver indicates whether the mouse is over the key, isDown indicates whether the key is
	currently pressed down.

	When doing this, be sure to note the keyboard's orientation.

	It seems it would be easier to just set the color for the particular notes we want to change 
	and then call the base class to do the drawing. But this doesn't work because the base class 
	fills all white notes at once.
*/
void MidiKeyboardComponentSolaris::drawWhiteNote(int midiNoteNumber,
	Graphics& g, Rectangle<float> area,
	bool isDown, bool isOver,
	Colour lineColour,
	Colour textColour)
{
	if (midiNoteNumber == m_rootKey)
	{
		Rectangle<int> rect(static_cast<int>(getKeyStartPosition(midiNoteNumber)), 0, KEYWIDTH, getHeight());
		g.setColour(Colours::red);
		g.setOpacity(0.5f);
		g.fillRect(rect);

		// Colour oldColourID = findColour(MidiKeyboardComponent::ColourIds::whiteNoteColourId);
		// setColour(MidiKeyboardComponent::ColourIds::whiteNoteColourId, Colours::red);
		// MidiKeyboardComponent::drawWhiteNote(midiNoteNumber, g, x, y, w, h, isDown, isOver, lineColour, textColour);
		// setColour(MidiKeyboardComponent::ColourIds::whiteNoteColourId, oldColourID);
	}
	MidiKeyboardComponent::drawWhiteNote(midiNoteNumber, g, area, isDown, isOver, lineColour, textColour);
}

/** Draws a black note in the given rectangle.

	isOver indicates whether the mouse is over the key, isDown indicates whether the key is
	currently pressed down.

	When doing this, be sure to note the keyboard's orientation.
*/
void MidiKeyboardComponentSolaris::drawBlackNote(int midiNoteNumber,
	Graphics& g, Rectangle<float> area,
	bool isDown, bool isOver,
	Colour noteFillColour)
{
	Colour colorID = noteFillColour;
	if (midiNoteNumber == m_rootKey)
	{
		colorID =Colours::red;
	}
	
	MidiKeyboardComponent::drawBlackNote(midiNoteNumber, g,area, isDown, isOver, colorID);
}

void MidiKeyboardComponentSolaris::OnSampleChanged(SampleID sampleID)
{
	sampleID;
	OnSelectionChanged();
}

void MidiKeyboardComponentSolaris::OnSelectionChanged()
{
	if (m_pViewModel->HasSampleSelection())
	{
		SampleID selectedSampleID = m_pViewModel->GetSelectedSampleID();
		SampleInfo info = m_pSampleFolder->GetSampleInfo(selectedSampleID);
		m_rootKey = info.rootKey;
		m_lowKey = info.lowKey;
		m_highKey = info.highKey;
		repaint();
	}
}
