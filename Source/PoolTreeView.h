/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#ifndef POOLTREEVIEW_H_INCLUDED
#define POOLTREEVIEW_H_INCLUDED

#include <vector>
#include <string>
#include <cassert>
#include "SampleFolder.h"
#include "../JuceLibraryCode/JuceHeader.h"
class ViewModel;

class PoolTreeRootItem : public TreeViewItem
{
public:
	PoolTreeRootItem(SampleFolder* pSampleFolder)
	: m_pSampleFolder(pSampleFolder) 
	{
		assert(m_pSampleFolder);
	}

	bool mightContainSubItems() override { return true; }

	bool isInterestedInDragSource(const DragAndDropTarget::SourceDetails& dragSourceDetails) override;
	void itemDropped(const DragAndDropTarget::SourceDetails & dragSourceDetails, int insertIndex) override;
private:
	SampleFolder*	m_pSampleFolder;

};


class PoolTreeSampleItem :	public TreeViewItem,
							public Label::Listener
{
public:
	PoolTreeSampleItem(SampleFolder* pSampleFolder, SampleID sampleID);

	bool mightContainSubItems() override { return false; }
	//void paintItem(Graphics & g, int width, int height) override;
	std::unique_ptr<Component> createItemComponent() override;
	void itemSelectionChanged(bool isNowSelected) override;
	SampleID GetSampleID() { return m_sampleID; }
	void itemClicked(const MouseEvent & event);
	void itemDoubleClicked(const MouseEvent & event);

	/** Called when a Label is about to delete its TextEditor and exit editing mode. */
	void editorHidden(Label*, TextEditor&) override
	{
		m_pItemLabel->setInterceptsMouseClicks(false, false);
	}

	var getDragSourceDescription() override
	{
		std::string s = "SampleItem:";
		s += m_sampleID.ToString();
		return var(s);
	}


private:
	// Inherited via Listener
	virtual void labelTextChanged(Label * labelThatHasChanged) override;

	SampleFolder::SampleChangeConnection	m_sampleChangedConnection;
	void OnSampleChanged(SampleID sampleID);	///< Called when a sample is modified, including raw file name change.

	SampleFolder*	m_pSampleFolder;
	SampleID		m_sampleID;
	std::unique_ptr<Label>	m_pItemLabel{ nullptr };
};

class PoolTreePoolItem : public TreeViewItem
{
public:
	PoolTreePoolItem(SampleFolder* pSampleFolder, PoolID poolID)
		:m_pSampleFolder(pSampleFolder), m_poolID(poolID){}

	bool mightContainSubItems() override { return true; }
	void paintItem(Graphics & g, int width, int height ) override;
	void itemClicked(const MouseEvent & event) override;
	void itemSelectionChanged(bool isNowSelected) override;
	std::string GetPoolFilePath();
	void RebuildView();
	PoolID GetPoolID() { return m_poolID; }
	
	void OnPoolChanged();
	void AddSamples(const std::vector<SampleID>& samplesToAdd);
	void RemoveSamples(const std::vector<SampleID>&  samplesToRemove);
	
	var getDragSourceDescription() override
	{
		std::string s = "PoolItem:";
		s += std::to_string(uintptr_t(m_poolID));
		return var(s);
	}

	bool isInterestedInDragSource(const DragAndDropTarget::SourceDetails& dragSourceDetails) override;
	void itemDropped(const DragAndDropTarget::SourceDetails & dragSourceDetails, int insertIndex) override;

private:
	PoolTreeSampleItem* GetSampleItemWithSampleID(SampleID sampleID);
	SampleFolder*	m_pSampleFolder;
	PoolID			m_poolID;
};

class PoolTreeView : public TreeView,
					public DragAndDropContainer

{
public:
	PoolTreeView(SampleFolder* pSampleFolder, ViewModel* pViewModel);
	~PoolTreeView();

	void OnSampleFolderChanged();
	ViewModel* GetSelectionModel();
	void OnPoolChanged(PoolID poolID) ;		///< Called when a sample is created or deleted, or a pool name is changed.
	void mouseDown(const MouseEvent& event) override;

private:
	void AddPools(const std::vector<PoolID>& poolsToAdd);
	void RemovePools(const std::vector<PoolID>& poolsToRemove);
	PoolTreePoolItem* GetPoolItemWithPoolID(PoolID poolIDToFind);
	TreeViewItem* GetSelectedItem();

	PoolTreeRootItem	m_rootItem;
	SampleFolder*		m_pSampleFolder{ nullptr };
	ViewModel*			m_pViewModel{ nullptr };

	SampleFolder::FolderChangeConnection m_folderChangeConnection;
	SampleFolder::PoolChangeConnection m_poolChangeConnection;

};
using PoolTreeViewUPtr = std::unique_ptr < PoolTreeView > ;


#endif  // POOLTREEVIEW_H_INCLUDED
