/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#ifndef AUDIOFORMATREADERLOOPINGSOURCE_H_INCLUDED
#define AUDIOFORMATREADERLOOPINGSOURCE_H_INCLUDED

#include <atomic>
#include "../JuceLibraryCode/JuceHeader.h"


/** @brief	A type of AudioSource that will read from an AudioFormatReader and optionally loop.

	This is just like the JUCE class AudioFormatReaderSource except that instead of only looping  
	the full length, you can define loop start and end points.\n
	\n
	When setting up AudioTransportSource, do not specify the use of a read-ahead buffer (BufferingAudioSource) 
	because that will try to reposition the current position and will mess up the looping. 
	See AudioFormatReaderLoopingSource::setNextReadPosition for more deatil.

	@see AudioFormatReaderSource, PositionableAudioSource, AudioTransportSource, BufferingAudioSource
*/
class JUCE_API  AudioFormatReaderLoopingSource : public PositionableAudioSource
{
public:
	//==============================================================================
	/** Creates an AudioFormatReaderSource for a given reader.

		@param sourceReader						the reader to use as the data source - this must 
												not be null
		@param deleteReaderWhenThisIsDeleted    if true, the reader passed-in will be deleted
												when this object is deleted; if false it will be
												left up to the caller to manage its lifetime
	*/
	AudioFormatReaderLoopingSource(AudioFormatReader* sourceReader,
		bool deleteReaderWhenThisIsDeleted);

	/** Destructor. */
	~AudioFormatReaderLoopingSource();

	//==============================================================================
	/** Toggles loop-mode.

		If set to true, it will continuously loop the input source at the loop points.
		(By default, the loop points are the start and end of the entire audio.)
		If false, it will just emit silence after the source has finished.

		@see isLooping
	*/
	void setLooping(bool shouldLoop);

	bool isLooping() const;

	/** Returns the reader that's being used. */
	AudioFormatReader* getAudioFormatReader() const noexcept { return m_reader; }

	//==============================================================================
	void prepareToPlay(int samplesPerBlockExpected, double sampleRate) override;

	void releaseResources() override;

	void getNextAudioBlock(const AudioSourceChannelInfo&) override;

	//==============================================================================
	void setNextReadPosition(int64 newPosition) override;

	int64 getNextReadPosition() const override;

	/** @copydoc PositionableAudioSource::getTotalLength */
	int64 getTotalLength() const override;

	void setLoopStartPosition(int64 startPosition);
	void setLoopEndPosition(int64 endPosition);

	int64 getLoopStartPosition() const { return m_loopStartPos;	};
	int64 getLoopEndPosition() const { return m_loopEndPos; };
private:
	//==============================================================================
	OptionalScopedPointer<AudioFormatReader> m_reader;

	std::atomic<int64> m_nextPlayPos{ 0 };
	std::atomic<bool> m_isLooping{ false };
	std::atomic<int64> m_loopStartPos{ 0 };
	std::atomic<int64> m_loopEndPos{ 0 };

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(AudioFormatReaderLoopingSource)
};



#endif  // AUDIOFORMATREADERLOOPINGSOURCE_H_INCLUDED
