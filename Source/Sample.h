/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#ifndef SAMPLE_H_INCLUDED
#define SAMPLE_H_INCLUDED

#include <memory>
#include <string>
#include "SampleFolderDefs.h"


class Sample
{
public:

	Sample(std::string newRawFilename, SampleInfo sampleInfo, bool overwriteRawFile = false, std::string restoreFilePath = "");
	Sample(std::istream& inStream);

	void Save(std::ostream& outStream) const;

	void SetRawFilename(std::string filename) noexcept;
	void SetSampleIndex(int index) noexcept;
	void SetSampleRate(int sampleRate) noexcept;
	void SetSampleLength(int sampleLength) noexcept;
	void SetLoopStart(int loopStart) noexcept;
	void SetLoopEnd(int loopEnd) noexcept;
	void SetRootKey(int rootKey) noexcept;
	void SetFineTune(int fineTune) noexcept;
	void SetLowKey(int lowKey) noexcept;
	void SetHighKey(int highKey) noexcept;


	std::string GetRawFilename() const noexcept;
	int GetSampleIndex() const noexcept;
	int GetSampleRate() const noexcept;
	int GetSampleLength() const noexcept;
	int GetLoopStart() const noexcept;
	int GetLoopEnd() const noexcept;
	int GetRootKey() const noexcept;
	int GetFineTune() const noexcept;
	int GetLowKey() const noexcept;
	int GetHighKey() const noexcept;

	bool Equals(const Sample* pOther) const;

	SampleInfo GetInfo() const;
	void SetInfo(const SampleInfo& info);
	SampID GetSampID() 
	{
		return this;
	}
private:
	void Load(std::istream& inStream);

	std::string	m_rawFilename;
	SampleInfo		m_info;
};
using SampleUPtr = std::unique_ptr < Sample > ;


#endif  // SAMPLE_H_INCLUDED
