/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#ifndef SOLARISAUDIOFORMAT_H_INCLUDED
#define SOLARISAUDIOFORMAT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

/**
	Reads and Writes Solaris format sample files.

	@see AudioFormat, SolarisAudioFormatReader, SolarisInputStream
*/
class JUCE_API  SolarisAudioFormat : public AudioFormat
{
public:
	//==============================================================================
	/** Creates a format object. */
	SolarisAudioFormat();

	/** Destructor. */
	~SolarisAudioFormat();

	/** Metadata property name used by Solaris readers and writers for maintaining
		sample index.

		@see AudioFormatReader::metadataValues, createWriterFor
	*/
	static const char* const metaSampleIndexName;

	/** Metadata property name used by Solaris readers and writers for maintaining 
		loop start position.

		@see AudioFormatReader::metadataValues, createWriterFor
	*/
	static const char* const metaLoopStartName;


	/** Metadata property name used by Solaris readers and writers for maintaining 
		loop end position.

		@see AudioFormatReader::metadataValues, createWriterFor
	*/
	static const char* const metaLoopEndName;

	/** Metadata property name used by Solaris readers and writers for maintaining
		root key.

		@see AudioFormatReader::metadataValues, createWriterFor
	*/
	static const char* const metaRootKeyName;

	/** Metadata property name used by Solaris readers and writers for maintaining
		fine tune.

	@see AudioFormatReader::metadataValues, createWriterFor
	*/
	static const char* const metaFineTuneName;



	/** Metadata property name used by Solaris readers and writers for maintaining
		low key.

		@see AudioFormatReader::metadataValues, createWriterFor
	*/
	static const char* const metaLowKeyName;


	/** Metadata property name used by Solaris readers and writers for maintaining
		high key.

		@see AudioFormatReader::metadataValues, createWriterFor
	*/
	static const char* const metaHighKeyName;


	/** Utility function to fill out the appropriate metadata for a BWAV file.

		This just makes it easier than using the property names directly, and it
		converts ints to strings.
	*/
	static StringPairArray createMetadata(
		//int sampleIndex,
		int loopStart,
		int loopEnd,
		int rootKey,
		int fineTune,
		int lowKey,
		int highKey);

	Array<int> getPossibleSampleRates() override;
	Array<int> getPossibleBitDepths() override;
	bool canDoStereo() override;
	bool canDoMono() override;

	//==============================================================================
	/** @copydoc AudioFormat::createReaderFor() */
	AudioFormatReader* createReaderFor(InputStream* sourceStream,
		bool deleteStreamIfOpeningFails) override;

	AudioFormatWriter* createWriterFor(OutputStream* streamToWriteTo,
		double sampleRateToUse,
		unsigned int numberOfChannels,
		int bitsPerSample,
		const StringPairArray& metadataValues,
		int qualityOptionIndex) override;

};


#endif  // SOLARISAUDIOFORMAT_H_INCLUDED
