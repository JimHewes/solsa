/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#include "AudioFormatReaderLoopingSource.h"


AudioFormatReaderLoopingSource::AudioFormatReaderLoopingSource(AudioFormatReader* const r,
	const bool deleteReaderWhenThisIsDeleted)
	: m_reader(r, deleteReaderWhenThisIsDeleted)
{
	jassert(m_reader->lengthInSamples > 0);
	m_loopEndPos = m_reader->lengthInSamples - 1;
	jassert(m_reader != nullptr);
}

AudioFormatReaderLoopingSource::~AudioFormatReaderLoopingSource() {}

int64 AudioFormatReaderLoopingSource::getTotalLength() const 
{
	return m_reader->lengthInSamples; 
}

/** @brief Sets the current position in the audio. Can be set while playing.

	@param[in] newPosition	the new position to set to.

	If you are using AudioFormatReaderLoopingSource in a chain, don't call this 
	function directly but rather call the most upstream setNextReadPosition. 
	This is most likely to be AudioTransportSource::setNextReadPosition().\n
	\n

	Note that if you set up the AudioTransportSource with a read-ahead buffer, then 
	BufferingAudioSource class will call this setNextReadPosition() to force the position 
	when it gets past the end of the sample, or past the loop end. This messes up the looping.
	Everything upstream (AudioTransportSource<--ResamplingAudioSource<--BufferingAudioSource) 
	believes that the position is incrementing infinitely when actually we're looping here.
	(I'm not sure I fully understand the intention of the looping feature in JUCE.)
	So you cannot use read-ahead buffering with this class unless you fix the way it reads handles 
	this. In order to do that without directly modifying JUCE code you would need to clone the 
	AudioTransportSource and BufferingAudioSource classes. Then modify 
	BufferingAudioSource::readBufferSection() to remove the line that calls this function.
*/
void AudioFormatReaderLoopingSource::setNextReadPosition(int64 newPosition) 
{
	m_nextPlayPos = newPosition;
}

void AudioFormatReaderLoopingSource::setLooping(bool shouldLoop) { m_isLooping = shouldLoop; }

/** @brief Reports whether loop-mode is turned on or not.

	This works differently from the way it is intended in JUCE because it supports looping of 
	any section not just the entire file. The streaming process normally gets stopped by the 
	AudioTransportSource class which is upstream. But it will not stop unless it sees that looping 
	is off and the play position is past the end. Since looping is always on in 
	AudioFormatReaderLoopingSource, the AudioTransportSource is tricked into thinking that looping 
	is off if the play position gets past the end of the loop. That way it will stop if the position 
	gets past the end of the file. This allows the cursor to be positioned past the loop section, 
	in which case it will play until the end and then stop.

	@retval	true if the current play position is before the loop end.
	@retval	false if the current play position is after the loop end.
	@see setLoopStartPosition, setLoopEndPosition
*/
bool AudioFormatReaderLoopingSource::isLooping() const
{
	return (m_nextPlayPos > m_loopEndPos) ? false : m_isLooping == true;
}

/** @brief Reports the current read position (i.e. play position)

	Unlike the corresponding JUCE function for getNextReadPosition, this does not wrap 
	on the length of the file if looping is turned on. This is because looping is 
	handled differently than in JUCE. In this class, the play position is always 
	within the length, otherwise playback gets stopped. 
	See isLooping() for more detail.

	@return	The current play position.
	@see isLooping
*/
int64 AudioFormatReaderLoopingSource::getNextReadPosition() const
{
//	return m_isLooping ? m_nextPlayPos % m_reader->lengthInSamples	: m_nextPlayPos;
	return m_nextPlayPos;
}

/** @brief Tells the source to prepare for playing.

	An AudioSource has two states: prepared and unprepared.
	
	The prepareToPlay() method is guaranteed to be called at least once on an 'unpreprared'
	source to put it into a 'prepared' state before any calls will be made to getNextAudioBlock().
	This callback allows the source to initialise any resources it might need when playing.
	
	Once playback has finished, the releaseResources() method is called to put the stream
	back into an 'unprepared' state.
	
	Note that this method could be called more than once in succession without
	a matching call to releaseResources(), so make sure your code is robust and
	can handle that kind of situation.
	
	@param samplesPerBlockExpected  the number of samples that the source
									will be expected to supply each time its
									getNextAudioBlock() method is called. This
									number may vary slightly, because it will be dependent
									on audio hardware callbacks, and these aren't
									guaranteed to always use a constant block size, so
									the source should be able to cope with small variations.

	@param sampleRate               the sample rate that the output will be used at - this
									is needed by sources such as tone generators.
	@see releaseResources, getNextAudioBlock
*/
void AudioFormatReaderLoopingSource::prepareToPlay(int /*samplesPerBlockExpected*/, double /*sampleRate*/) {}

/** @brief Allows the source to release anything it no longer needs after playback has stopped.

	This will be called when the source is no longer going to have its getNextAudioBlock()
	method called, so it should release any spare memory, etc. that it might have
	allocated during the prepareToPlay() call.
	
	Note that there's no guarantee that prepareToPlay() will actually have been called before
	releaseResources(), and it may be called more than once in succession, so make sure your
	code is robust and doesn't make any assumptions about when it will be called.
	
	@see prepareToPlay, getNextAudioBlock
*/
void AudioFormatReaderLoopingSource::releaseResources() {}

/** @brief Called repeatedly to fetch subsequent blocks of audio data.

	After calling the prepareToPlay() method, this callback will be made each
	time the audio playback hardware (or whatever other destination the audio
	data is going to) needs another block of data.
	
	It will generally be called on a high-priority system thread, or possibly even
	an interrupt, so be careful not to do too much work here, as that will cause
	audio glitches!

	@see AudioSourceChannelInfo, prepareToPlay, releaseResources
*/
void AudioFormatReaderLoopingSource::getNextAudioBlock(const AudioSourceChannelInfo& info)
{
	if (info.numSamples > 0)
	{
		// If loop end is earlier than loop start, then treat it as if looping is turned off.
		// Also note that in a Solaris, the loop end points to the last sample in a loop, not the sample after
		// the end of the loop. So the loop length is (end - start + 1)
		if (m_isLooping && m_loopStartPos <= m_loopEndPos && m_nextPlayPos <= m_loopEndPos)
		{
			int64 currentPos = m_nextPlayPos;
			int64 samplesLeft = info.numSamples;

			while (samplesLeft > 0)
			{
				int64 samplesToCopy = std::min(samplesLeft, m_loopEndPos - currentPos + 1);

				m_reader->read(info.buffer, 
					info.startSample + (info.numSamples - (int)samplesLeft),
					(int)samplesToCopy, 
					currentPos, 
					true, true);

				samplesLeft -= samplesToCopy;
				currentPos += samplesToCopy;
				if (currentPos > m_loopEndPos)
				{
					currentPos = m_loopStartPos;
				}
			}
			m_nextPlayPos = currentPos;
		}
		else
		{	// Unless I later change things to allowing looping to be disabled, this code should never be reached.
			m_reader->read(info.buffer, info.startSample, info.numSamples, m_nextPlayPos, true, true);
			m_nextPlayPos += info.numSamples;
		}
	}
}

/** @brief	Sets the position in the audio where the loop will start if looping is enabled.

	@param[in] startPosition	The position in the audio where the loop will start. 
								The range of allowable values is from the first sample in the file to 
								the sample end position. The sample start and end can be the same, giving 
								a loop of one sample.
								The default is the beginning of the audio.

	@see setLoopEndPosition, getLoopStartPosition
	*/
void AudioFormatReaderLoopingSource::setLoopStartPosition(int64 startPosition) { m_loopStartPos = startPosition; }

/** @brief	Sets the position in the audio where the loop will end if looping is enabled.

	@param[in] endPosition	The position in the audio where the loop will end; i.e the last sample in the loop
							( not one sample after the end). Thus the range of allowable values is from sample
							start position to the last sample in the file. The sample start and end can be the 
							same, giving a loop of one sample.
							The default is the last sample of the audio, so that the entire audio length loops.

	@see setLoopStartPosition, getLoopEndPosition
*/
void AudioFormatReaderLoopingSource::setLoopEndPosition(int64 endPosition) { m_loopEndPos = endPosition; }

