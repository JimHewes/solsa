/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#include <cassert>
#include <fstream>
#include <cstdint>
#include <string>
#include <utility>
#include <vector>
#include "SolarisInputStream.h"
#include "SampleFolderDefs.h"
#include "SampleFolder.h"

using std::string;
using std::endl;
using std::ios;
using std::vector;

static const int64 g_headerSizeInBytes = 32;

enum class HeaderIndex : std::int8_t {
	SampleRate = 0,
	SampleLength,
	LoopStart,
	LoopEnd,
	RootKey,
	FineTune,
	LowKey,
	HighKey
};


/* Copies the data from a SampleInfo structure to a fixed size header buffer.

	Since the header buffer is always 9 * 4  = 36 bytes the stream never changes size from the 
	point of view of the JUCE AduioFormatReader or AudioFormatWriter classes.
*/
static void CopySampleInfoToHeader(const SampleInfo& sampleInfo, HeaderBuffer& header)
{
	header.resize(32);	// ensure size is correct
	header[(int)HeaderIndex::SampleRate] =		sampleInfo.sampleRate;
	header[(int)HeaderIndex::SampleLength] =	sampleInfo.sampleLength;
	header[(int)HeaderIndex::LoopStart] =		sampleInfo.loopStart;
	header[(int)HeaderIndex::LoopEnd] =			sampleInfo.loopEnd;
	header[(int)HeaderIndex::RootKey] =			sampleInfo.rootKey;
	header[(int)HeaderIndex::FineTune] =		sampleInfo.fineTune;
	header[(int)HeaderIndex::LowKey] =			sampleInfo.lowKey;
	header[(int)HeaderIndex::HighKey] =			sampleInfo.highKey;

}

static void CopyHeaderToSampleInfo(const HeaderBuffer& header, SampleInfo& sampleInfo)
{
	sampleInfo.sampleRate =		static_cast<uint16_t>(header[(int)HeaderIndex::SampleRate]);
	sampleInfo.sampleLength =	header[(int)HeaderIndex::SampleLength];
	sampleInfo.loopStart =		header[(int)HeaderIndex::LoopStart];
	sampleInfo.loopEnd =		header[(int)HeaderIndex::LoopEnd];
	sampleInfo.rootKey =		static_cast<uint8_t>(header[(int)HeaderIndex::RootKey]);
	sampleInfo.fineTune =		static_cast<uint8_t>(header[(int)HeaderIndex::FineTune]);
	sampleInfo.lowKey =			static_cast<uint8_t>(header[(int)HeaderIndex::LowKey]);
	sampleInfo.highKey =		static_cast<uint8_t>(header[(int)HeaderIndex::HighKey]);
}


SolarisInputStream::SolarisInputStream(SampleFolder* sampleFolder, SampleID sampleID)
	:m_status(Result::ok())
	, m_header(static_cast<size_t>(g_headerSizeInBytes) / sizeof(uint32_t))

{
	SampleInfo sampleInfo = sampleFolder->GetSampleInfo(sampleID);
	CopySampleInfoToHeader(sampleInfo, m_header);

	std::string rawFilePath = sampleFolder->GetFolderPath();
	if (!rawFilePath.ends_with(std::string(File::getSeparatorString().text)))
	{
		rawFilePath += File::getSeparatorString().text;
	}
	rawFilePath += sampleFolder->GetSampleRawFilename(sampleID);

	m_rawFile.open(rawFilePath, std::ios::in | std::ios::binary);
	if (!m_rawFile)
	{
		m_status = Result::fail("Raw file could not be opened for input.");
	}
}


int64 SolarisInputStream::getTotalLength()
{
	assert(openedOk());
	m_rawFile.seekg(0,ios::end);
	int rawFileSize = (int)m_rawFile.tellg();
	setPosition(m_currentPosition);	// restore previous position
	return  g_headerSizeInBytes + rawFileSize;
}

bool SolarisInputStream::isExhausted()
{
	return m_currentPosition >= getTotalLength();
}

int64 SolarisInputStream::getPosition()
{
	return m_currentPosition;
}

bool SolarisInputStream::setPosition(int64 pos)
{
	assert(openedOk());

	if (pos != m_currentPosition)
	{
		if (pos >= g_headerSizeInBytes)
		{
			m_rawFile.seekg(pos - g_headerSizeInBytes,ios::beg);
		}
		m_currentPosition = pos;
	}

	return m_currentPosition == pos;
}

int SolarisInputStream::read(void* destBuffer,int maxBytesToRead)
{
	char* pDest = (char*)destBuffer;
	int numBytesToRead;
	int numBytesRead = 0;

	if (m_currentPosition < g_headerSizeInBytes)
	{
		int headerBytesLeft = int(g_headerSizeInBytes - m_currentPosition);
		numBytesToRead = std::min(maxBytesToRead,headerBytesLeft);
		char* pSrc = reinterpret_cast<char*>(&m_header[0]);
		memcpy(pDest, pSrc, numBytesToRead);
		pDest += numBytesToRead;
		m_currentPosition += numBytesToRead;
		maxBytesToRead -= numBytesToRead;
	}

	if (m_currentPosition >= g_headerSizeInBytes)
	{
		numBytesRead = 0;
		numBytesToRead = std::min(maxBytesToRead,int(getTotalLength() - m_currentPosition));
		if (numBytesToRead > 0)
		{
			m_rawFile.seekg(m_currentPosition - g_headerSizeInBytes, ios::beg);
			m_rawFile.read(pDest,numBytesToRead);
			numBytesRead += !m_rawFile ? 0 : numBytesToRead;
			m_currentPosition += numBytesRead;
		}
	}
	return numBytesRead;
}


/** Constructor for the SolarisOutputStream.

	@param[in] pSampleFolder	The SampleFolder model that has the data for samples.
	@param[in] sampleID			The sampleID of the sample to output to. The specified ID must refer to an existing sample.
*/
SolarisOutputStream::SolarisOutputStream(SampleFolder* pSampleFolder, SampleID sampleID)
	:m_status(Result::ok())
	, m_pSampleFolder(pSampleFolder)
	, m_sampleID(sampleID)
	, m_header(static_cast<size_t>(g_headerSizeInBytes) / sizeof(uint32_t))
{

	SampleInfo sampleInfo = m_pSampleFolder->GetSampleInfo(sampleID);
	CopySampleInfoToHeader(sampleInfo, m_header);

	std::string rawFilePath = m_pSampleFolder->GetFolderPath();
	if (!util::EndsWith(rawFilePath, std::string(File::getSeparatorString().text)))
	{
		rawFilePath += File::getSeparatorString().text;
	}
	rawFilePath += m_pSampleFolder->GetSampleRawFilename(sampleID);

	m_rawFile.open(rawFilePath, ios::out | ios::binary);
	if (!m_rawFile)
	{
		assert(false);
		m_status = Result::fail("Raw file could not be opened for output.");
	}
}

SolarisOutputStream::~SolarisOutputStream()
{
	flush();
}

int64 SolarisOutputStream::getPosition()
{
	return m_currentPosition;
}

bool SolarisOutputStream::setPosition(int64 pos)
{
	assert(openedOk());

	if (pos != m_currentPosition)
	{
		if (pos >= g_headerSizeInBytes)
		{
			m_rawFile.seekp(pos - g_headerSizeInBytes,ios::beg);
		}
		m_currentPosition = pos;
	}

	return m_currentPosition == pos;
}

bool SolarisOutputStream::write(const void *dataToWrite, size_t numberOfBytes)
{
	assert(dataToWrite != nullptr);
	char* pSrc = (char*)dataToWrite;

	size_t numBytesWritten = 0;

	if (m_currentPosition < g_headerSizeInBytes && numberOfBytes > 0)
	{
		size_t numBytesToWrite = std::min(size_t(g_headerSizeInBytes - m_currentPosition),numberOfBytes);
		char* pDest = reinterpret_cast<char*>(&m_header[0]) + m_currentPosition;
		memcpy(pDest,pSrc,numBytesToWrite);
		numberOfBytes -= numBytesToWrite;
		pSrc += numBytesToWrite;
		m_currentPosition += numBytesToWrite;
		numBytesWritten = numBytesToWrite;

		// Update sampleInfo to the sampleFolder model
		SampleInfo sampleInfo = m_pSampleFolder->GetSampleInfo(m_sampleID);;
		CopyHeaderToSampleInfo(m_header, sampleInfo);
		m_pSampleFolder->SetSampleInfo(m_sampleID, sampleInfo);
	}
	if (m_currentPosition >= g_headerSizeInBytes && numberOfBytes > 0)
	{
		m_rawFile.seekp(m_currentPosition - g_headerSizeInBytes, ios::beg);
		m_rawFile.write(pSrc,numberOfBytes);
		m_currentPosition += numberOfBytes;
		numBytesWritten += !m_rawFile ? 0 : numberOfBytes;
	}
	return true;
}

void SolarisOutputStream::flush()
{
	// Update sampleInfo to the sampleFolder model
	SampleInfo sampleInfo = m_pSampleFolder->GetSampleInfo(m_sampleID);
	CopyHeaderToSampleInfo(m_header, sampleInfo);
	m_pSampleFolder->SetSampleInfo(m_sampleID, sampleInfo);

	m_rawFile.flush();
}