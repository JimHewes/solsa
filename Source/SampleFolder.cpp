/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/
#include <cctype>	// for isdigit()
#include <sstream>
#include <algorithm>
#include <cassert>
#include "SampleFolder.h"
#include "SamplePool.h"
#include "../JuceLibraryCode/JuceHeader.h"

using std::string;
using std::vector;

std::vector<std::string> GetPoolFilepathsOnDisk(const string& folderPath);



/**	Constructor for the SampleFolder class.

	@param	folderPath	The full path to the folder that contains Solaris pool files.
						This must be a valid directory.
	@throws std::runtime_error	If the path specified is not a valid directory.
*/
SampleFolder::SampleFolder(const string& folderPath)
{
	SetFolderPath(folderPath);
}

/**	Changes the path of the sample folder.

	This sends out a notification for the FolderChangeEvent.

	@param	folderPath	The full path to the folder that contains Solaris pool files.
						This must be a valid directory.
	@throws std::runtime_error	If the path specified is not a valid directory.
*/
void SampleFolder::SetFolderPath(const string& folderPath)
{
	if (m_currentFolderPath != folderPath)
	{
		if (!File(folderPath).isDirectory())
		{
			throw std::runtime_error("Folder does not exist or is not a directory.");
		}

		LoadPoolsFromDisk(folderPath);
		m_currentFolderPath = folderPath;
		FolderChangeEvent.Notify();
	}
}


/** Returns a list of all pool files in the specified folder.

	The files returned are only the files that have a valid Solaris file name.
	The sequence of numbers in the filenames do not need to be continuous to be 
	included in the list.

	@param[in]	folderPath	The fill path to a folder on disk. The path must refer to 
							an actual directory that exists on disk.
	@returns	A list of full pathnames of all pool files in the specified folder. 
	*/
vector<string> GetPoolFilepathsOnDisk(const string& folderPath)
{

	File sampleFolder(folderPath);

	Array< File >	poolFileList;
	sampleFolder.findChildFiles(poolFileList,
		File::findFiles,
		false,					// don't search resursively
		"SamplePool-???.txt"	// partially filter out files we don't want
		);

	vector<string>	validPoolFileList;

	for (auto& poolFile : poolFileList)
	{
		// The three wildcard characters in the filename must be digits and they 
		// cannot be "000" since the Solaris won't recognize that.
		if (VerifyPoolFilename(poolFile.getFileName().toStdString()))
		{
			validPoolFileList.push_back(poolFile.getFullPathName().toStdString());
		}
	}

	return validPoolFileList;
}

void SampleFolder::LoadPoolsFromDisk(const string& folderPath)
{
	vector<string>	poolFileList = GetPoolFilepathsOnDisk(folderPath);

	m_poolList.clear();

	for (auto& poolFilepath : poolFileList)
	{
		m_poolList.push_back(std::make_unique<SamplePool>(poolFilepath));
	}

	// The poolList should already be sorted because the filenames were in order. But just to make sure.
	FixupPoolIndices();
}

std::string SampleFolder::GetFolderPath() const noexcept
{
	return m_currentFolderPath;
}


std::vector<PoolID> SampleFolder::GetPoolIDs() const
{
	std::vector<PoolID> poolIDs;
	for (auto& pPool : m_poolList)
	{
		poolIDs.push_back(pPool->GetPoolID());
	}
	return poolIDs;
}

bool SampleFolder::PoolIDIsValid(PoolID poolID) const
{
	return (GetPoolFromID(poolID) != nullptr);
}

uint16_t SampleFolder::GetFirstAvailblePoolIndex()
{
	// This is the way to do it with the std library, but just seems to convoluted. I'd also have to check for empty list.
	// auto pPool = std::max_element(m_poolList.begin(), m_poolList.end(),	[](SamplePoolUPtr& pElem1, SamplePoolUPtr& pElem2) { return pElem1->GetPoolIndex() < pElem2->GetPoolIndex();});

	uint16_t maxPoolIndex = 0;
	for (auto& pPool : m_poolList)
	{
		maxPoolIndex = std::max(maxPoolIndex, pPool->GetPoolIndex());
	}
	return maxPoolIndex + 1;
}

SamplePool*	SampleFolder::GetPoolFromID(PoolID poolID) const
{
	for (auto& pPool : m_poolList)
	{
		if (poolID == pPool->GetPoolID())
		{
			return pPool.get();
		}
	}
	return nullptr;
}

std::string SampleFolder::GetPoolName(PoolID poolID) const
{
	SamplePool* pPool = GetPoolFromID(poolID);
	assert(pPool != nullptr);
	return pPool->GetName();
}

void SampleFolder::SetPoolName(PoolID poolID, const std::string& newPoolName)
{
	SamplePool* pPool = GetPoolFromID(poolID);
	assert(pPool != nullptr);
	pPool->SetName(newPoolName);
	PoolChangeEvent.Notify(poolID);
}

std::string SampleFolder::GetPoolFilePath(PoolID poolID) const
{
	SamplePool* pPool = GetPoolFromID(poolID);
	assert(pPool != nullptr);
	return pPool->GetFilePath();
}

uint16_t SampleFolder::GetPoolIndex(PoolID poolID) const
{
	SamplePool* pPool = GetPoolFromID(poolID);
	assert(pPool != nullptr);
	return pPool->GetPoolIndex();
}

void SampleFolder::SetPoolIndex(PoolID poolID, uint16_t index)
{
	SamplePool* pPool = GetPoolFromID(poolID);
	assert(pPool != nullptr);
	pPool->SetPoolIndex(index);
	FolderChangeEvent.Notify();
}

/** Creates a new, empty sample pool in the current folder using the next availble index.

	@param[in] poolName		The name of the pool. This is optional. If not supplied, the name will be "New Pool".
							The name can be changed later using the SetName() function.

	@throws std::runtime_exception	If the pool file cannot be created. This may be due to a conflict
									in the index number or that the pool folder is not writable.
*/
PoolID SampleFolder::CreatePool(std::string poolName)
{
	uint16_t index = GetFirstAvailblePoolIndex();
	m_poolList.push_back(std::make_unique<SamplePool>(index, m_currentFolderPath, poolName));
	PoolID poolID =  m_poolList.back()->GetPoolID();
	FolderChangeEvent.Notify();
	return poolID;
}

/** Removes (deletes) a pool and all its contained samples and raw files from disk.

	All files are removed to the Trash/Recycle bin rather than being deleted permanently.

	@param[in] poolID	The ID of the pool to remove.
*/

void SampleFolder::RemovePool(PoolID poolID)
{
	SamplePool* pPoolToRemove = GetPoolFromID(poolID);
	assert(pPoolToRemove != nullptr);
	std::vector<SampleID> sampleIDs = pPoolToRemove->GetSampleIDs();
	
	// remove all the raw files used by this pool.
	for (auto& sampleID : sampleIDs)
	{
		File rawFile(pPoolToRemove->GetSampleRawFilepath(sampleID.sampID));
		rawFile.moveToTrash();
	}

	// remove the pool file
	File poolFile(pPoolToRemove->GetFilePath());
	poolFile.moveToTrash();

	// remove in-memory objects
	m_poolList.erase(
		std::remove_if(m_poolList.begin(), m_poolList.end(),
			[pPoolToRemove](SamplePoolUPtr& pPool) { return pPool.get() == pPoolToRemove;}),
		m_poolList.end());

	FolderChangeEvent.Notify();
}

/** @brief Moves a pool to a different place in the order of pools.

	This effectively changes the pool filename.

	@param[in] poolID		The ID of the pool to move. This is based on the index as
							seen in the pool filename.
	@param[in] newIndex		The index to change to. This is not	the ID but is the zero-based 
							index of the samples in the pool.

*/
void SampleFolder::MovePool(PoolID poolID, uint16_t newIndex)
{
	SamplePool* pSrcPool = GetPoolFromID(poolID);	// get a handle on the pool we want to change.
	assert(pSrcPool != nullptr);

	// First, to make room, we rename all pool files with an index greater than or equal to the newIndex.
	// We should not have a rename conflict because we're renaming files as "SamplePool-xxx.txt" and if there 
	// were another file in the way it would have already been made part of the list when loading.
	// It's only possible if the user created the file manually after starting the program. And it would 
	// happen on the first rename and cause an exception.
	std::for_each (m_poolList.rbegin(), m_poolList.rend(), [newIndex](SamplePoolUPtr& pPool)
	{
		if (pPool->GetPoolIndex() >= newIndex + 1)
		{
			pPool->SetPoolIndex(pPool->GetPoolIndex() + 1);
		}
	});
	pSrcPool->SetPoolIndex(newIndex + 1);

	FixupPoolIndices();

	FolderChangeEvent.Notify();
}

/** Sorts the pool list according to pool index and then renumbers indexes to remove spaces.

	@returns true if any pool indices were changed, false otherwise.
*/
bool SampleFolder::FixupPoolIndices()
{
	bool modified = false;
	if (!m_poolList.empty())
	{
		// Sort pool according to original pool index in the filename.
		sort(m_poolList.begin(), m_poolList.end(), [](SamplePoolUPtr& p1, SamplePoolUPtr& p2) { return p1->GetPoolIndex() < p2->GetPoolIndex(); });
		
		// Now re-number the pools to ensure numbering is continuous. This renames the txt files if necessary.
		for (uint16_t i = 0; i < m_poolList.size(); ++i)
		{
			if (m_poolList[i]->GetPoolIndex() != (i + 1))
			{
				m_poolList[i]->SetPoolIndex(i + 1);
				modified = true;
			}
		}
	}

	return modified;
}

std::vector<SampleID> SampleFolder::GetSampleIDs(PoolID poolID) const
{
	SamplePool* pPool = GetPoolFromID(poolID);
	assert(pPool != nullptr);
	return pPool->GetSampleIDs();
}

SampleInfo SampleFolder::GetSampleInfo(SampleID sampleID)
{
	SamplePool* pPool = GetPoolFromID(sampleID.poolID);
	assert(pPool != nullptr);
	return pPool->GetSampleInfo(sampleID.sampID);
}

void SampleFolder::SetSampleInfo(SampleID sampleID, SampleInfo sampleInfo)
{
	SamplePool* pPool = GetPoolFromID(sampleID.poolID);
	assert(pPool != nullptr);
	pPool->SetSampleInfo(sampleID.sampID, sampleInfo);
	SampleChangeEvent.Notify(sampleID);
}

SampleID SampleFolder::CreateSample(PoolID poolID, const std::string& rawFilename, SampleInfo sampleInfo, bool overwriteRawFile, std::string restoreFilePath)
{
	SamplePool* pPool = GetPoolFromID(poolID);
	assert(pPool != nullptr);
	SampleID result = pPool->CreateSample(rawFilename, sampleInfo, overwriteRawFile, restoreFilePath);
	PoolChangeEvent.Notify(poolID);
	return result;
}

bool SampleFolder::RawFileIsReadable(SampleID sampleID)
{
	SamplePool* pPool = GetPoolFromID(sampleID.poolID);
	assert(pPool != nullptr);
	return pPool->RawFileIsReadable(sampleID.sampID);
}

void SampleFolder::RemoveSample(SampleID sampleID, bool removeRawFile)
{
	SamplePool* pPool = GetPoolFromID(sampleID.poolID);
	assert(pPool != nullptr);
	pPool->RemoveSample(sampleID.sampID, removeRawFile);

	// I had thought to notify listeners before actually removing the sample to give them a chance 
	// to stop using it before we remove it. But that had a problem. At the moment, the notification 
	// only says that the pool changed in some way and it's up to the listener to figure out 
	// what changed by queryng the model. But in this case the sample hasn't been deleted yet so 
	// nothing has changed. So it may be better to work the opposite way---don't allow the sample 
	// to be removed while it's in use. This means that there might be a place (like SelectionModel) 
	// that is a view model where the state of the usage of the sample is kept and can be queried.
	// For example, sample-is-playing or sample-is-in-use. This is like putting a lock on a sample.
	// (Another idea is to have one notification for warning a sample will be removed and another 
	// notification to announce it has been removed. But that feels too complicated.)
	PoolChangeEvent.Notify(sampleID.poolID);
}

std::string SampleFolder::GetSampleRawFilename(SampleID sampleID) const
{
	SamplePool* pPool = GetPoolFromID(sampleID.poolID);
	assert(pPool != nullptr);
	return pPool->GetSampleRawFilename(sampleID.sampID);
}

void SampleFolder::SetSampleRawFilename(SampleID sampleID, const string& newRawFilename)
{
	SamplePool* pPool = GetPoolFromID(sampleID.poolID);
	assert(pPool != nullptr);
	pPool->SetSampleRawFilename(sampleID.sampID, newRawFilename);
	SampleChangeEvent.Notify(sampleID);

}


/** @brief Moves a sample to a different place in the same pool, or to a different pool.

	@param[in] sampleID		The ID of the sample to move. This is based on the index as 
							seen in the pool file.
	@param[in] destPoolID	The ID of the pool the sample will end up in. This ID is 
							based on the index as seen in the pool file.
	@param[in] newIndex		The index within the destination pool to change to. This is not 
							the ID but is the zero-based index of the samples in the pool. 
							Since the samples are always kept in sequential order, this 
							should always be one less than the real sample index in the pool 
							file, which starts at 1.
	*/
void SampleFolder::MoveSample(SampleID sourceSampleID, PoolID destPoolID, uint16_t newIndex)
{
	if (sourceSampleID.poolID == destPoolID)
	{
		SamplePool* pPool = GetPoolFromID(sourceSampleID.poolID);
		assert(pPool != nullptr);
		pPool->ChangeSampleIndex(sourceSampleID.sampID, newIndex);
		PoolChangeEvent.Notify(sourceSampleID.poolID);
	}
	else
	{
		SamplePool* pSrcPool = GetPoolFromID(sourceSampleID.poolID);
		assert(pSrcPool != nullptr);
		SamplePool* pDstPool = GetPoolFromID(destPoolID);
		assert(pDstPool != nullptr);

		pDstPool->AddSample(pSrcPool->RemoveSample(sourceSampleID.sampID), newIndex);
		PoolChangeEvent.Notify(sourceSampleID.poolID);
		PoolChangeEvent.Notify(destPoolID);

	}
}

bool SampleFolder::SampleExists(const SampleID& sampleID) const
{
	SamplePool* pPool = GetPoolFromID(sampleID.poolID);
	return (pPool && pPool->GetSampleFromID(sampleID.sampID));
}


