/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#include<cassert>
#include "TransportPanel.h"
#include "SampleFolder.h"
#include "ViewModel.h"
#include "SolarisInputStream.h"
#include "SolarisAudioFormat.h"
#include "AudioFormatReaderLoopingSource.h"

using std::make_unique;


inline AudioThumbnailComp::AudioThumbnailComp(
	AudioFormatManager& formatManager,
	AudioTransportSource& transportSource,
	SampleFolder* pSampleFolder,
	ViewModel* pViewModel)
	: m_transportSource(transportSource)
	, m_pSampleFolder(pSampleFolder)
	, m_pViewModel(pViewModel)
	, m_scrollbar(false)
	, m_thumbnailCache(5)
	, m_thumbnail(512, formatManager, m_thumbnailCache)
	, m_isFollowingTransport(false)
{
	assert(m_pSampleFolder);
	assert(m_pViewModel);

	m_thumbnail.addChangeListener(this);

	addAndMakeVisible(m_scrollbar);
	m_scrollbar.setRangeLimits(m_visibleRange);
	m_scrollbar.setAutoHide(false);
	m_scrollbar.addListener(this);

	m_currentPositionMarker.setFill(Colours::white.withAlpha(0.85f));
	addAndMakeVisible(m_currentPositionMarker);
}

inline AudioThumbnailComp::~AudioThumbnailComp()
{
	m_scrollbar.removeListener(this);
	m_thumbnail.removeChangeListener(this);
}

void AudioThumbnailComp::paint(Graphics& g)
{
	g.fillAll(Colours::darkgrey);

	if (m_pViewModel->HasSampleSelection())
	{
		// Draw loop
		SampleID sampleID = m_pViewModel->GetSelectedSampleID();
		SampleInfo info = m_pSampleFolder->GetSampleInfo(sampleID);

		auto visibleSampleStart = float(m_visibleRange.getStart() * info.sampleRate);
		auto visibleSampleEnd = float(m_visibleRange.getEnd() * info.sampleRate);
		auto visibleSampleLength = float(m_visibleRange.getLength() * info.sampleRate);

		// If loop overlaps visible area
		if (info.loopStart < visibleSampleEnd && info.loopEnd > visibleSampleStart)
		{
			// constrain
			Rectangle<float> rect;
			rect.setX(std::max(0.0f, info.loopStart - visibleSampleStart));
			if (info.loopStart > visibleSampleStart)
			{
				rect.setWidth(std::min(visibleSampleEnd - info.loopStart + 1.0f, info.loopEnd - info.loopStart + 1.0f));
			}
			else
			{
				rect.setWidth(std::min(info.loopEnd - info.loopStart + 1.0f, info.loopEnd - visibleSampleStart + 1.0f));
			}

			// Scale to component width
			auto scale = getWidth() / visibleSampleLength;
			rect.setX(float(rect.getX() * scale));
			rect.setWidth(float(rect.getWidth() * scale));

			rect.setHeight(float(getHeight() - m_scrollbar.getHeight()));

			g.setColour(Colours::lightgrey);
			g.setOpacity(0.3f);
			g.fillRect(rect);
		}
		
		g.setColour(Colour(0xFF8b9da9));	// similar to steel blue.

		if (m_thumbnail.getTotalLength() > 0.0)
		{
			Rectangle<int> thumbArea(getLocalBounds());
			thumbArea.removeFromBottom(m_scrollbar.getHeight() + 4);
			m_thumbnail.drawChannels(g, thumbArea.reduced(2),
				m_visibleRange.getStart(), m_visibleRange.getEnd(), 1.0f);
		}
		else
		{
			g.setFont(14.0f);
			g.drawFittedText("(No audio file selected)", getLocalBounds(), Justification::centred, 2);
		}

		// Draw range selection if the range is geater than zero.
		if (m_selectionRangeStart < m_selectionRangeEnd)
		{
			// If range overlaps visible area
			if (m_selectionRangeStart < visibleSampleEnd && m_selectionRangeEnd > visibleSampleStart)
			{
				// constrain
				Rectangle<float> rect;
				rect.setX(std::max(0.0f, m_selectionRangeStart - visibleSampleStart));
				if (m_selectionRangeStart > visibleSampleStart)
				{
					rect.setWidth(std::min(visibleSampleEnd - m_selectionRangeStart + 1.0f, float(m_selectionRangeEnd - m_selectionRangeStart)));
				}
				else
				{
					rect.setWidth(std::min(m_selectionRangeEnd - m_selectionRangeStart + 1.0f, float(m_selectionRangeEnd - visibleSampleStart)));
				}

				// Scale to component width
				auto scale = getWidth() / visibleSampleLength;
				rect.setX(float(rect.getX() * scale));
				rect.setWidth(float(rect.getWidth() * scale));

				rect.setHeight(float(getHeight() - m_scrollbar.getHeight()));

				g.setColour(Colours::lightblue);
				g.setOpacity(0.3f);
				g.fillRect(rect);
			}
		}
	}
}

inline void AudioThumbnailComp::setZoomFactor(double amount)
{
	if (m_thumbnail.getTotalLength() > 0)
	{
		const double newScale = jmax(0.001, m_thumbnail.getTotalLength() * (1.0 - jlimit(0.0, 0.99, amount)));
		const double timeAtCentre = xToTime(getWidth() / 2.0f);
		// DBG("setZoomFactor calling setVisibleRange with start position " << timeAtCentre - newScale * 0.5);
		setVisibleRange(Range<double>(timeAtCentre - newScale * 0.5, timeAtCentre + newScale * 0.5));
	}
}

inline void AudioThumbnailComp::setVisibleRange(Range<double> newRange)
{
	// DBG("setVisibleRange called with start position " << newRange.getStart() < ". Calling slider setCurrentRange.");
	m_visibleRange = newRange;
	m_scrollbar.setCurrentRange(m_visibleRange);
	updateCursorPosition();
	repaint();
}

void AudioThumbnailComp::SetScrollPosition(double startPosition)
{
	if (!(m_isFollowingTransport && m_transportSource.isPlaying()))
	{
		// DBG("SetScrollPosition calling setVisibleRange with start position " << startPosition);
		setVisibleRange(m_visibleRange.movedToStartAt(startPosition));
	}
}


/** Sets the audio source that the thumbnail will use.

	@param[in] pSource	The audio source that the thumbnail will use.
						THIS WILL BE DELETED AUTOMATICALLY BY THE AudioThumbnailComp OBJECT!!
						(i.e., internally by the JUCE AudioThumbnail object.)
						You may set this to nullptr to release the previous audio source so that
						the AudioThumbnailComp will stop displaying its waveform.
*/
void AudioThumbnailComp::SetAudioSource(InputSource * pSource)
{
	m_thumbnail.setSource(pSource);
	const Range<double> newRange(0.0, m_thumbnail.getTotalLength());
	m_scrollbar.setRangeLimits(newRange);
	setVisibleRange(newRange);

	startTimerHz(40);
}

void AudioThumbnailComp::SetSelectionRange(int64_t start, int64_t end, bool notify)
{
	assert(start >= 0);
	assert(end >= 0);
	assert(start <= end);

	m_selectionRangeStart = start;
	m_selectionRangeEnd = end;
	if (notify)
	{
		ChangedRangeEvent.Notify();
	}
}

void AudioThumbnailComp::mouseDown(const MouseEvent & e)
{
	if (e.mods.isShiftDown())
	{
		if (m_pViewModel->HasSampleSelection())
		{
			m_rangeSelectionInProgress = true;
			SampleID sampleID = m_pViewModel->GetSelectedSampleID();
			SampleInfo info = m_pSampleFolder->GetSampleInfo(sampleID);
			m_sampleRateCache = info.sampleRate;

			m_selectionRangeInitial = xToSample((float)e.x);
			m_selectionRangeStart = m_selectionRangeInitial;
			mouseDrag(e);
		}
	}
	if (e.mods.isCommandDown())
	{
		m_dragZoomInProgress = true;
		m_previousZoomDragOffset = 0;
	}
	else
	{
		mouseDrag(e);
	}
}

void AudioThumbnailComp::mouseDrag(const MouseEvent & e)
{
	if (canMoveTransport())
	{
		if (m_rangeSelectionInProgress)
		{
			// We only do range selection for when the mouse is inside the thumbnail component.
			// This prevents us from getting audio positions outside of the actual audio range.
			auto x = std::min(getWidth()-1, std::max(0, e.x));

			auto newRangePosition = xToSample((float)x);

			// Here we decide whether the user's mouse cursor represents the start or end of the range 
			// depending on whether it's to the left or the right of the initial mousedown.
			// (The mousedown is one end of the range.)
			if (newRangePosition < m_selectionRangeInitial)
			{
				m_selectionRangeStart = newRangePosition;
			}
			else
			{
				m_selectionRangeEnd = newRangePosition;
			}

			repaint();
			ChangedRangeEvent.Notify();
			
		}
		else if (m_dragZoomInProgress)
		{
			float zoomDragDelta = float(e.getDistanceFromDragStartY() - m_previousZoomDragOffset) / 30.0f;
			//DBG("Drag Zoom delta =" << zoomDragDelta);
			MouseWheelEvent.Notify(zoomDragDelta);
			m_previousZoomDragOffset = e.getDistanceFromDragStartY();
		}
		else
		{
			m_transportPosition = std::max(0.0, xToTime((float)e.x));
			SetPositionEvent.Notify(m_transportPosition);
		}
	}
}

void AudioThumbnailComp::mouseUp(const MouseEvent &)
{
	m_rangeSelectionInProgress = false;
	m_dragZoomInProgress = false;
	m_transportSource.start();
}

void AudioThumbnailComp::mouseWheelMove(const MouseEvent & event, const MouseWheelDetails & wheel)
{
	if (m_thumbnail.getTotalLength() > 0.0)
	{
		double newStart = m_visibleRange.getStart() - wheel.deltaX * (m_visibleRange.getLength()) / 10.0;
		newStart = jlimit(0.0, jmax(0.0, m_thumbnail.getTotalLength() - (m_visibleRange.getLength())), newStart);

		if (canMoveTransport())
		{
			setVisibleRange(Range<double>(newStart, newStart + m_visibleRange.getLength()));
		}

		if (wheel.deltaY != 0.0f)
		{
			if (event.mods.isCommandDown())
			{
				//DBG("Zoom distance =" << -wheel.deltaY);
				MouseWheelEvent.Notify(-wheel.deltaY);
			}
			else
			{
				auto l = m_visibleRange.getLength() * wheel.deltaY * 0.2;
				m_scrollbar.setCurrentRangeStart(m_scrollbar.getCurrentRangeStart() + l);
				//DBG("Range = " << l << ", deltaY = " << wheel.deltaY);
			}
		}

		repaint();
	}
}

/** Translates a time in the audio stream to an X position within the thumbnail.

	@param[in] time		The time (in seconds) within the audio stream. Must be >= 0.
	@returns			The X position in the thumbnail that corresponds to the given time in the audio stream.
*/

inline float AudioThumbnailComp::timeToX(const double time) const
{
	auto length = m_visibleRange.getLength();
	if (length == 0)
	{
		return 0.0f;
	}
	return getWidth() * (float)((time - m_visibleRange.getStart()) / length);
}

/** Translates an X position within the thumbnail to a time in the audio stream.

	@param[in] x	The X position within the thumbnail. Must be >= 0.
	@returns		The time (in seconds) of the position in the audio stream that corresponds to the thumbnail X position.
*/

inline double AudioThumbnailComp::xToTime(const float x) const
{
	return (x / getWidth()) * (m_visibleRange.getLength()) + m_visibleRange.getStart();
}

/** Translates an X position within the thumbnail to a sample in the audio stream.

	@param[in] x	The X position within the thumbnail. Must be >= 0.
	@returns		The sample position in the audio stream that corresponds to the thumbnail X position.

	@internal This depends on the m_sampleCache member variable being set correctly.
*/

uint32_t AudioThumbnailComp::xToSample(const float x) const
{
	auto time = (x / getWidth()) * (m_visibleRange.getLength()) + m_visibleRange.getStart();
	return static_cast<uint32_t>(time * m_sampleRateCache);
}

inline bool AudioThumbnailComp::canMoveTransport() const noexcept
{
	// Transport cannot be moved if we're playing OR if we're following transport.
	return !(m_isFollowingTransport && m_transportSource.isPlaying());
}

void AudioThumbnailComp::scrollBarMoved(ScrollBar * scrollBarThatHasMoved, double newRangeStart)
{
	if (scrollBarThatHasMoved == &m_scrollbar)
	{
		if (!(m_isFollowingTransport && m_transportSource.isPlaying()))
		{
			// DBG("scrollBarMoved called. Calling setVisibleRange with start position " << newRangeStart);
			setVisibleRange(m_visibleRange.movedToStartAt(newRangeStart));

			// update view model with new position
			if (m_pViewModel->HasSampleSelection())
			{
				// DBG("Setting view model with start position " << newRangeStart);
				m_pViewModel->SetThumbnailScrollPosition(m_pViewModel->GetSelectedSampleID(), newRangeStart);
			}
		}
	}
}

inline void AudioThumbnailComp::timerCallback()
{
	if (canMoveTransport())
	{
		updateCursorPosition();
	}
	else
	{
		setVisibleRange(m_visibleRange.movedToStartAt(m_transportSource.getCurrentPosition() - (m_visibleRange.getLength() / 2.0)));
	}
}

inline void AudioThumbnailComp::updateCursorPosition()
{
	m_currentPositionMarker.setVisible(true);

	if (m_transportSource.isPlaying())
	{
		m_transportPosition = m_transportSource.getCurrentPosition();
	}
	//DBG("updateCursorPosition:  pos = " << m_transportPosition);
	float pos = timeToX(m_transportPosition);
#if 0
	int64 nextReadPos = m_transportSource.getNextReadPosition();
	int64 totalLength = m_transportSource.getTotalLength();
	DBG("pos = " << pos << ", nextPos = " << nextReadPos << ", len = " << totalLength);
#endif

	m_currentPositionMarker.setRectangle(Rectangle<float>(pos - 0.75f, 0,
		1.5f, (float)(getHeight() - m_scrollbar.getHeight())));
}

void AudioThumbnailComp::SetCursorPosition(double position)
{
	m_transportPosition = position;
	updateCursorPosition();
}


TransportPanel::TransportPanel(
	SampleFolder* pSampleFolder, 
	AudioDeviceManager* pAudioDeviceManager, 
	AudioFormatManager& audioFormatManager, 
	ViewModel* pViewModel,
	PropertiesFile* pPropertiesFile)
	: m_pSampleFolder(pSampleFolder)
	, m_pViewModel(pViewModel)
	, m_pPropertiesFile(pPropertiesFile)
{
	assert(m_pViewModel);
	assert(m_pSampleFolder);
	assert(pAudioDeviceManager);

	m_playButton.setButtonText("Play");
	m_playButton.addListener(this);
	addAndMakeVisible(m_playButton);
	m_playButton.setEnabled(false);		// Don't enable the play button until a sample is selected.

	pAudioDeviceManager->addAudioCallback(&m_audioSourcePlayer);
	m_audioSourcePlayer.setSource(&m_transportSource);

	m_pAudioThumbnailComp = std::make_unique<AudioThumbnailComp>(audioFormatManager, m_transportSource, pSampleFolder, pViewModel);
	addAndMakeVisible(m_pAudioThumbnailComp.get());

	// register for event notification
	m_pSelectionConnection = m_pViewModel->SelectionChangeEvent.Connect([this]() {	OnSelectionChanged(); });
	m_mouseWheelConnection = m_pAudioThumbnailComp->MouseWheelEvent.Connect([this](float deltaY) { OnThumbnailMouseWheelMoved(deltaY);});
	m_setPositionConnection = m_pAudioThumbnailComp->SetPositionEvent.Connect([this](double newPosition)
	{	
		m_transportSource.setPosition(newPosition);
		if (m_pViewModel->HasSampleSelection())
		{
			m_pViewModel->SetPlayPosition(m_pViewModel->GetSelectedSampleID(), newPosition);
		}
	});
	m_changeRangeConnection = m_pAudioThumbnailComp->ChangedRangeEvent.Connect([this]() { OnSelectionRangeChanged();});


	m_sampleChangeConnection = m_pSampleFolder->SampleChangeEvent.Connect([this](SampleID) { OnSampleChanged(); });

	addAndMakeVisible(m_zoomSlider);
	m_zoomSlider.setRange(0, 1, 0);
	m_zoomSlider.setSliderStyle(Slider::LinearHorizontal);
	m_zoomSlider.setTextBoxStyle(Slider::NoTextBox, false, 80, 20);
	m_zoomSlider.setSkewFactor(2);
	m_zoomSlider.addListener(this);

	addAndMakeVisible(m_zoomLabel);
	m_zoomLabel.setText("Zoom:", dontSendNotification);
	m_zoomLabel.setFont(FontOptions(15.00f, Font::plain));
	m_zoomLabel.setJustificationType(Justification::centredRight);
	m_zoomLabel.setEditable(false, false, false);
	m_zoomLabel.setColour(TextEditor::textColourId, Colours::black);
	m_zoomLabel.setColour(TextEditor::backgroundColourId, Colour(0x00000000));

	addAndMakeVisible(m_followTransportButton);
	m_followTransportButton.setButtonText("Follow Transport");
	m_followTransportButton.addListener(this);

	addAndMakeVisible(m_loopOnButton);
	m_loopOnButton.setButtonText("Loop On");
	m_loopOnButton.setToggleState(m_loopEnabled, NotificationType::dontSendNotification);
	m_loopOnButton.addListener(this);

	addAndMakeVisible(m_volumeLabel);
	m_volumeLabel.setText("Volume:", dontSendNotification);
	m_volumeLabel.setFont(FontOptions(15.00f, Font::plain));
	m_volumeLabel.setJustificationType(Justification::centredRight);
	m_volumeLabel.setEditable(false, false, false);
	m_volumeLabel.setColour(TextEditor::textColourId, Colours::black);
	m_volumeLabel.setColour(TextEditor::backgroundColourId, Colour(0x00000000));

	addAndMakeVisible(m_volumeSlider);
	m_volumeSlider.setRange(0, 100, 1);
	m_volumeSlider.setSliderStyle(Slider::LinearHorizontal);
	m_volumeSlider.setTextBoxStyle(Slider::NoTextBox, false, 80, 20);
	m_volumeSlider.addListener(this);
	int v = m_pPropertiesFile->getIntValue("volume");
	m_volumeSlider.setValue(v);
	//m_transportSource.setGain(static_cast<float>(std::stoi(m_pPropertiesFile->getValue("volume").toStdString())));


	m_rangeStartLabel.setEditable(false, false, false);
	addAndMakeVisible(m_rangeStartLabel);
	
	m_rangeEndLabel.setEditable(false, false, false);
	addAndMakeVisible(m_rangeEndLabel);

	m_rangeLengthLabel.setEditable(false, false, false);
	addAndMakeVisible(m_rangeLengthLabel);
	OnSelectionRangeChanged();
}

TransportPanel::~TransportPanel()
{
	m_transportSource.setSource(nullptr);
	m_audioSourcePlayer.setSource(nullptr);
	m_pCurrentAudioFileSource.reset();
	m_zoomSlider.removeListener(this);
	m_followTransportButton.removeListener(this);
	m_loopOnButton.removeListener(this);

}

void TransportPanel::resized()
{
	Rectangle<int> r(getLocalBounds());
	
	Rectangle<int> controls = r.removeFromBottom(100);
	m_pAudioThumbnailComp->setBounds(r);
	auto x = controls.getX();
	auto y = controls.getY();
	auto w = controls.getWidth();
	//Rectangle<int> zoom(Rectangle<int>(x + 50, y, controls.getWidth() - 50, 25));
	m_zoomLabel.setBounds(x, y, 50, 25);
	m_zoomSlider.setBounds(x + 50, y, w - 50, 25);
	y += 25;
	m_followTransportButton.setBounds(x + 2, y, 132, 25);
	m_loopOnButton.setBounds(x + 142, y, 100, 25);
	m_volumeLabel.setBounds(x + 242, y, 60, 25);
	m_volumeSlider.setBounds(x + 300, y, 160, 25);


	y += 29;
	m_playButton.setBounds(20, y, 90, 24);

	m_rangeStartLabel.setBounds(130, y, 140, 24);
	m_rangeEndLabel.setBounds(280, y, 140, 24);
	m_rangeLengthLabel.setBounds(430, y, 140, 24);

}


void TransportPanel::paint(Graphics& g)
{
	g.setColour(Colours::darkgrey);
	g.drawRect(getLocalBounds(), 1);
}

void TransportPanel::buttonClicked(Button* button)
{
	if (button == &m_playButton)
	{
		m_isPlaying ? StopPlaying() : StartPlaying();
	}
	else if (button == &m_followTransportButton)
	{
		m_pAudioThumbnailComp->setFollowsTransport(m_followTransportButton.getToggleState());

		if (m_pViewModel->HasSampleSelection())
		{
			m_pViewModel->SetFollowTransport(m_pViewModel->GetSelectedSampleID(), m_followTransportButton.getToggleState());
		}
	}
	else if (button == &m_loopOnButton)
	{
		m_loopEnabled = m_loopOnButton.getToggleState();
		if (m_pViewModel->HasSampleSelection())
		{
			m_pViewModel->SetLoopOn(m_pViewModel->GetSelectedSampleID(), m_loopOnButton.getToggleState());
		}
	}
}

void TransportPanel::sliderValueChanged(Slider * sliderThatWasMoved)
{
	if (sliderThatWasMoved == &m_zoomSlider)
	{
		// DBG("sliderValueChanged handler called. Calling thumbnail setZoomFactor with level " << m_zoomSlider.getValue());
		m_pAudioThumbnailComp->setZoomFactor(m_zoomSlider.getValue());

		// update view model with new position
		if (m_pViewModel->HasSampleSelection())
		{
			// DBG("Setting view model with zoom level " << m_zoomSlider.getValue());
			m_pViewModel->SetThumbnailZoomLevel(m_pViewModel->GetSelectedSampleID(), m_zoomSlider.getValue());
		}
	}
	if (sliderThatWasMoved == &m_volumeSlider)
	{
		double gain = m_volumeSlider.getValue() / m_volumeSlider.getMaximum();
		m_transportSource.setGain(static_cast<float>(gain));
		m_pPropertiesFile->setValue("volume", static_cast<int>(m_volumeSlider.getValue()));
	}

}


/** @brief Called by AudioTransportSource when it starts or stops playing.

	Called by AudioTransportSource when it sees that the position of its own 
	audio source has gone past that source's length and the source is not looping.\n
	\n
	This will call TransportPanel::StopPlaying() which will let go of the audio source 
	(so that it can be edited).

	@param[in] broadcaster	A pointer to the AudioTransportSource.
*/
void TransportPanel::changeListenerCallback(juce::ChangeBroadcaster *broadcaster)
{
	broadcaster;
	// DBG("changeListenerCallback called.");
	if (m_isPlaying && !m_transportSource.isPlaying())
	{
		// DBG("changeListenerCallback stopping playback.");
		StopPlaying();
	}
}


void TransportPanel::StopPlaying()
{
	m_transportSource.stop();
	m_transportSource.removeChangeListener(this);

	if (m_pViewModel->HasSampleSelection())
	{
		m_pViewModel->SetPlayPosition(m_pViewModel->GetSelectedSampleID(), m_transportSource.getCurrentPosition());
	}
	
	// unload the source.
	m_transportSource.setSource(nullptr);
	m_pCurrentAudioFileSource.reset();
	m_pCurrentAudioFileReader.reset();

	m_isPlaying = false;
	m_playButton.setButtonText("Play");
	m_loopOnButton.setEnabled(true);
}

void TransportPanel::StartPlaying()
{
	assert(!m_isPlaying);


	// The selection could be either a sample or a pool. So check for Sample.
	if (m_pViewModel->HasSampleSelection())
	{

		SampleID selectedSampleID = m_pViewModel->GetSelectedSampleID();
		if (m_pSampleFolder->RawFileIsReadable(selectedSampleID))
		{

			SolarisAudioFormat solarisAudioFormat;

			// The AudioFormatReader created will delete the SolarisInputStream so don't delete it.
			SolarisInputStream* pSolarisInputStream = new SolarisInputStream(m_pSampleFolder, selectedSampleID);
			m_pCurrentAudioFileReader.reset(solarisAudioFormat.createReaderFor(pSolarisInputStream, true));

			if (m_pCurrentAudioFileReader != nullptr)
			{
				m_pCurrentAudioFileSource = make_unique<AudioFormatReaderLoopingSource>(m_pCurrentAudioFileReader.get(), false);
				m_pCurrentAudioFileSource->setLooping(m_loopEnabled);
				m_pCurrentAudioFileSource->setLoopStartPosition(m_loopStartPos);
				m_pCurrentAudioFileSource->setLoopEndPosition(m_loopEndPos);

				// Unlike the JUCE demo, we don't use use the read ahead buffer. The reason is because it messes with the looping 
				// of the sample. If we get past the end of the loop (or the sample) it tries to impose its own position on 
				// the reader, and it knows nothing about looping except to loop the whole sample.
				// If you want read-ahead buffering, you need remove the line that does this in 
				// BufferingAudioSource::readBufferSection(). If you don't want to modify JUCE code directly you will need to clone 
				// AudioTransportSource as well as BufferingAudioSource because it is all constructed and controlled by 
				// AudioTransportSource.
				m_transportSource.setSource(m_pCurrentAudioFileSource.get(),
					0,											// tells it not to buffer ahead
					nullptr,									// this is the background thread to use for reading-ahead
					m_pCurrentAudioFileReader->sampleRate);		// allows for sample rate correction

				m_transportSource.setPosition(m_pViewModel->GetPlayPosition(selectedSampleID));
				m_transportSource.addChangeListener(this);
				m_transportSource.start();

				m_isPlaying = true;
				m_playButton.setButtonText("Stop");
				m_loopOnButton.setEnabled(false);

			}
		}
	}
}


void TransportPanel::OnSelectionChanged()
{
	if (m_isPlaying)
	{
		StopPlaying();
	}

	// The selection could be either a sample or a pool. So check for Sample.
	if (m_pViewModel->HasSampleSelection())
	{
		SampleID selectedSampleID = m_pViewModel->GetSelectedSampleID();
		if (m_pSampleFolder->RawFileIsReadable(selectedSampleID))
		{
			// We also need to give the audio thumbnail display access to the file. It takes a pointer 
			// to an audio source, but it wants to delete it (which I don't like, but it's the way the 
			// library works). So we need to create a different one.
			// The SolarisInputSource is a small class that exists only as a way for the thumbnail to get 
			// a SolarisInputSource. We could have given the thumbnail a AudioFormatReaderSource directly 
			// but then it would keep the file open all the time which we don't want.
			m_pAudioThumbnailComp->SetAudioSource(new SolarisInputSource(m_pSampleFolder, selectedSampleID));

			SampleInfo info = m_pSampleFolder->GetSampleInfo(selectedSampleID);
			m_loopStartPos = info.loopStart;
			m_loopEndPos = info.loopEnd;
			if (m_loopStartPos == 0 && m_loopEndPos == 0)
			{
				m_loopEndPos = info.sampleLength - 1;
			}

			//DBG("Got zoom value from the view model: " << m_pViewModel->GetThumbnailZoomLevel(selectedSampleID));
			//DBG("Got scroll position value from the view model: " << m_pViewModel->GetThumbnailScrollPosition(selectedSampleID));
			//DBG("Got cursor position value from the view model: " << m_pViewModel->GetPlayPosition(selectedSampleID));

			// We need to set the zoom slider synchronously because it changes the scroll position and if we send it 
			// asynchronously it will happen after we set the scroll position here and will set a position we don't want.
			m_zoomSlider.setValue(m_pViewModel->GetThumbnailZoomLevel(selectedSampleID), juce::sendNotificationSync);
			m_pAudioThumbnailComp->SetScrollPosition(m_pViewModel->GetThumbnailScrollPosition(selectedSampleID));
			m_transportSource.setPosition(m_pViewModel->GetPlayPosition(selectedSampleID));
			m_pAudioThumbnailComp->SetCursorPosition(m_pViewModel->GetPlayPosition(selectedSampleID));
			m_followTransportButton.setToggleState(m_pViewModel->GetFollowTransport(selectedSampleID), juce::sendNotificationSync);
			m_loopOnButton.setToggleState(m_pViewModel->GetLoopOn(selectedSampleID), juce::sendNotificationSync);

			m_playButton.setEnabled(true);
		}
		else
		{
			m_pAudioThumbnailComp->SetAudioSource(nullptr);

			m_playButton.setEnabled(false);
		}
	}
	else
	{
		m_pAudioThumbnailComp->SetAudioSource(nullptr);
		m_playButton.setEnabled(false);
	}

	m_pAudioThumbnailComp->SetSelectionRange(0, 0);	// reset selection range. Will notify OnSelectionRangeChanged() to redisplay labels.
}

void TransportPanel::OnSampleChanged()
{
	assert(m_pSampleFolder);
	assert(m_pViewModel);
	if (m_pViewModel->HasSampleSelection())
	{
		SampleID sampleID = m_pViewModel->GetSelectedSampleID();
		SampleInfo info =  m_pSampleFolder->GetSampleInfo(sampleID);
		m_loopStartPos = info.loopStart;
		m_loopEndPos = info.loopEnd;
		if (m_isPlaying)
		{
			assert(m_pCurrentAudioFileSource);
			if (m_pCurrentAudioFileSource->getLoopStartPosition() != info.loopStart)
			{
				m_pCurrentAudioFileSource->setLoopStartPosition(info.loopStart);
			}
			if (m_pCurrentAudioFileSource->getLoopEndPosition() != info.loopEnd)
			{
				m_pCurrentAudioFileSource->setLoopEndPosition(info.loopEnd);
			}
		}
		repaint();
	}
	
}

void TransportPanel::OnSelectionRangeChanged()
{
	assert(m_pViewModel);

	bool visible = false;
	if (m_pViewModel->HasSampleSelection())
	{
		auto start = m_pAudioThumbnailComp->GetSelectionRangeStart();
		auto end = m_pAudioThumbnailComp->GetSelectionRangeEnd();
		if (start < end)
		{
			std::ostringstream os;
			os << "Range start: " << m_pAudioThumbnailComp->GetSelectionRangeStart();
			m_rangeStartLabel.setText(os.str(), dontSendNotification);

			os.str("");
			os << "Range end: " << m_pAudioThumbnailComp->GetSelectionRangeEnd();
			m_rangeEndLabel.setText(os.str(), dontSendNotification);

			os.str("");
			os << "Range length: " << m_pAudioThumbnailComp->GetSelectionRangeEnd() - m_pAudioThumbnailComp->GetSelectionRangeStart();
			m_rangeLengthLabel.setText(os.str(), dontSendNotification);
			visible = true;
		}
	}
	m_rangeStartLabel.setVisible(visible);
	m_rangeEndLabel.setVisible(visible);
	m_rangeLengthLabel.setVisible(visible);

}
