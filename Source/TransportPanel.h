/*
/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#ifndef TRANSPORTPANEL_H_INCLUDED
#define TRANSPORTPANEL_H_INCLUDED

#include "SampleFolderDefs.h"
#include "ViewModel.h"
#include "SolarisInputStream.h"
#include "Notifier.h"
#include "../JuceLibraryCode/JuceHeader.h"

class SampleFolder;
class AudioFormatReaderLoopingSource;
class ViewModel;

//==============================================================================
/**
	A type of InputSource that represents a Solaris Audio file.

	@see InputSource
*/
class JUCE_API  SolarisInputSource : public InputSource
{
public:
	//==============================================================================
	/** Creates a SolarisInputSource for a file.
		If the useFileTimeInHashGeneration parameter is true, then this object's
		hashCode() method will incorporate the file time into its hash code; if
		false, only the file name will be used for the hash.
	*/
	SolarisInputSource(SampleFolder* pSampleFolder, SampleID sampleID, bool useFileTimeInHashGeneration = false)
		:m_pSampleFolder(pSampleFolder)
		, m_sampleID(sampleID)
		, m_useFileTimeInHashGeneration(useFileTimeInHashGeneration)
	{
		assert(m_pSampleFolder);
	}

	/** Destructor. */
	~SolarisInputSource() {};

	/** Creates a SolarisInputStream based on the SampleFolder and SampleID given in the constructor. 

		@returns	A pointer to an InputStream which is actually a SolarisInputStream.
	*/
	InputStream* createInputStream() override
	{
		SolarisInputStream* pStream = new SolarisInputStream(m_pSampleFolder, m_sampleID);
		return pStream->openedOk() ? pStream : nullptr;
	}

	/** Creates an input stream for the specified sibling file that's in the same folder as the original input source.
		This is not called given the way that SolarisInputSource will be used. So we don't bother to define it properly.
	*/
	InputStream* createInputStreamFor(const String& relatedItemPath) override
	{
		relatedItemPath;
		assert(false);
		return nullptr;
	}

	int64 hashCode() const override
	{
		String hashString = m_pSampleFolder->GetPoolFilePath(m_sampleID.GetPoolID());
		hashString += m_pSampleFolder->GetSampleRawFilename(m_sampleID).c_str();
		return hashString.hashCode64();
	}

private:
	//==============================================================================
	SampleFolder*	m_pSampleFolder;
	SampleID		m_sampleID;
	bool m_useFileTimeInHashGeneration;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(SolarisInputSource)
};


class AudioThumbnailComp : public Component,
	public ChangeListener,
	private ScrollBar::Listener,
	private Timer
{
public:
	AudioThumbnailComp(
		AudioFormatManager& formatManager, 
		AudioTransportSource& transportSource, 
		SampleFolder* pSampleFolder, 
		ViewModel* pViewModel);
	~AudioThumbnailComp();

	void paint(Graphics& g) override;

	void resized() override
	{
		m_scrollbar.setBounds(getLocalBounds().removeFromBottom(20).reduced(2));
	}

	void changeListenerCallback(ChangeBroadcaster*) override
	{
		// this method is called by the thumbnail when it has changed, so we should repaint it..
		repaint();
	}

	void setZoomFactor(double amount);
	void setVisibleRange(Range<double> newRange);
	void SetScrollPosition(double startPosition);
	void setFollowsTransport(bool shouldFollow)
	{
		m_isFollowingTransport = shouldFollow;
	}

	void SetAudioSource(InputSource* pSource);
	uint32_t GetSelectionRangeStart()
	{
		return (uint32_t)m_selectionRangeStart;
	}
	uint32_t GetSelectionRangeEnd()
	{
		return (uint32_t)m_selectionRangeEnd;
	}

	void SetSelectionRange(int64_t start, int64_t end, bool notify = true);

	void mouseDown(const MouseEvent& e) override;
	void mouseDrag(const MouseEvent& e) override;
	void mouseUp(const MouseEvent&) override;
	void mouseWheelMove(const MouseEvent& event, const MouseWheelDetails& wheel) override;
	void updateCursorPosition();
	void SetCursorPosition(double position);

	Notifier<void(float)> MouseWheelEvent;
	using MouseWheelConnection = decltype(MouseWheelEvent)::ConnectionType;	///< MouseWheelEvent.Connect() returns this type

	Notifier<void(double)> SetPositionEvent;
	using SetPositionConnection = decltype(SetPositionEvent)::ConnectionType;	///< SetPositionEvent.Connect() returns this type

	Notifier<void(void)> ChangedRangeEvent;
	using ChangeRangeConnection = decltype(ChangedRangeEvent)::ConnectionType;	///< ChangedRangeEvent.Connect() returns this type

private:

	float timeToX(const double time) const;
	double xToTime(const float x) const;
	uint32_t xToSample(const float x) const;

	bool canMoveTransport() const noexcept;

	void scrollBarMoved(ScrollBar* scrollBarThatHasMoved, double newRangeStart) override;

	void timerCallback() override;

	SampleFolder*			m_pSampleFolder;
	ViewModel*				m_pViewModel;
	AudioTransportSource&	m_transportSource;
	AudioThumbnailCache		m_thumbnailCache;
	AudioThumbnail			m_thumbnail;

	/** @brief The range (in seconds) of the audio file that is visible in the thumbnail. */
	Range<double>	m_visibleRange;
	ScrollBar		m_scrollbar;
	bool			m_isFollowingTransport;
	double			m_transportPosition{ 0 };

	DrawableRectangle m_currentPositionMarker;

	int64		m_selectionRangeStart{ 0 };	///< The sample that starts the range selection.
	int64		m_selectionRangeEnd{ 0 };	///< The sample one after the end of the selection range.
	bool		m_rangeSelectionInProgress{ false };
	bool		m_dragZoomInProgress{ false };
	int			m_previousZoomDragOffset{ 0 };
	uint16_t	m_sampleRateCache{ 0 };

	/** @brief The sample position of the range on the first mouse down. This is used to allow a mouse drag to
		either change the range start or range end depending on whether the drag position is to the left 
		or the right of the initial position. */
	int64		m_selectionRangeInitial{ 0 };

};

class TransportPanel :	public Component,
						public Button::Listener,
						public ChangeListener,
						private Slider::Listener
{
public:
	TransportPanel(
		SampleFolder* pSampleFolder, 
		AudioDeviceManager* pAudioDeviceManager, 
		AudioFormatManager& audioFormatManager, 
		ViewModel* pViewModel,
		PropertiesFile* pPropertiesFile);
	~TransportPanel();

	void resized() override;
	void paint(Graphics& g) override;
	void changeListenerCallback(juce::ChangeBroadcaster *);

	uint32_t GetSelectionRangeStart()
	{
		return m_pAudioThumbnailComp->GetSelectionRangeStart();
	}

	uint32_t GetSelectionRangeEnd()
	{
		return m_pAudioThumbnailComp->GetSelectionRangeEnd();
	}

protected:
	void buttonClicked(Button* button) override;
	void sliderValueChanged(Slider* sliderThatWasMoved) override;

private:
	void StopPlaying();
	void StartPlaying();
	void OnSelectionChanged();
	void OnSampleChanged();
	void OnSelectionRangeChanged();

	void OnThumbnailMouseWheelMoved(float deltaY)
	{
		m_zoomSlider.setValue(m_zoomSlider.getValue() - deltaY/10);
	}


	SampleFolder*		m_pSampleFolder;
	ViewModel*			m_pViewModel;
	TextButton			m_playButton;
	PropertiesFile*		m_pPropertiesFile;
	bool				m_isPlaying{ false };	///< If true, the audio is playing.

	AudioSourcePlayer m_audioSourcePlayer;
	AudioTransportSource m_transportSource;
	std::unique_ptr<AudioFormatReader> m_pCurrentAudioFileReader;
	std::unique_ptr<AudioFormatReaderLoopingSource> m_pCurrentAudioFileSource;
	ViewModel::SelectionChangeConnection m_pSelectionConnection;
	SampleFolder::SampleChangeConnection m_sampleChangeConnection;

	std::unique_ptr<AudioThumbnailComp> m_pAudioThumbnailComp;
	Label m_zoomLabel;
	Slider m_zoomSlider;
	ToggleButton m_followTransportButton;
	ToggleButton m_loopOnButton;
	Label m_volumeLabel;
	Slider m_volumeSlider;

	AudioThumbnailComp::MouseWheelConnection m_mouseWheelConnection;
	AudioThumbnailComp::SetPositionConnection m_setPositionConnection;
	AudioThumbnailComp::ChangeRangeConnection m_changeRangeConnection;

	int64 m_loopStartPos{ 0 };
	int64 m_loopEndPos{ 0 };
	bool m_loopEnabled{ true };

	Label m_rangeStartLabel;
	Label m_rangeEndLabel;
	Label m_rangeLengthLabel;
};

#endif  // TRANSPORTPANEL_H_INCLUDED
