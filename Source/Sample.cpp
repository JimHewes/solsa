/*
========================================================================================

This file is part of Solsa, a sample pool editor for the John Bowen Solaris synthesizer.
Copyright (c) 2016 - Jim Hewes

Solsa is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Solsa is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Solsa.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================================
*/

#include <cassert>
#include <cstdint>
#include <stdlib.h>
#include <string>
#include <ostream>
#include "Sample.h"
#include "SampleFolderDefs.h"
#include "Utility.h"

using std::string;
using std::endl;

Sample::Sample(std::string newRawFilename, SampleInfo sampleInfo, bool overwriteRawFile, std::string restoreFilePath)
	:m_info(sampleInfo)
	,m_rawFilename(newRawFilename)
{
	overwriteRawFile;
}

Sample::Sample(std::istream& inStream)
{
	Load(inStream);
}

/** Parses text values from an input stream.

	This function does not initilize the Sample class. Any member variables not referenced 
	by the stream are not changed. Only variables mentioned in the stream are set.

	@param[in] inStream		A text stream containing the name/value pairs in the format of a Solaris pool file.
*/
void Sample::Load(std::istream& inStream)
{

	std::string line;

	getline(inStream,line);
	assert(util::Trim(line) == "[Sample]");

	while (getline(inStream,line))
	{
		auto parts = util::SplitString(line,"=",true);
		// don't assert (parts.size() == 2); The caller may have added blank lines as padding. So just ignore.
		if (parts.size() == 2)
		{
			string leftSide = util::Trim(parts[0]);	// trims white space from left and right sides.
			string rightSide = util::Trim(parts[1]);

			if (leftSide == "filename")
			{
				m_rawFilename = rightSide;
			}
			else
			{
				// Get right side as an integer value.
				int value = atoi(rightSide.c_str());

				if (leftSide == "sampleindex")
				{
					m_info.sampleIndex = static_cast<uint16_t>(value);
				}
				else if (leftSide == "samplerate")
				{
					m_info.sampleRate = static_cast<uint16_t>(value);
				}
				else if (leftSide == "samplelength")
				{
					m_info.sampleLength = value;
				}
				else if (leftSide == "loopstart")
				{
					m_info.loopStart = value;
				}
				else if (leftSide == "loopend")
				{
					m_info.loopEnd = value;
				}
				else if (leftSide == "rootkey")
				{
					m_info.rootKey = static_cast<uint8_t>(value);
				}
				else if (leftSide == "finetune")
				{
					m_info.fineTune = static_cast<uint8_t>(value);
				}
				else if (leftSide == "lowkey")
				{
					m_info.lowKey = static_cast<uint8_t>(value);
				}
				else if (leftSide == "highkey")
				{
					m_info.highKey = static_cast<uint8_t>(value);
				}
			}
		}
	}

}

void Sample::Save(std::ostream& outStream) const
{
	outStream << "[Sample]" << endl
		<< "sampleindex = " << (int)m_info.sampleIndex << endl
		<< "filename = " << m_rawFilename << endl
		<< "samplerate = " << (int)m_info.sampleRate << endl
		<< "samplelength = " << m_info.sampleLength << endl
		<< "loopstart = " << m_info.loopStart << endl
		<< "loopend = " << m_info.loopEnd << endl
		<< "rootkey = " << (int)m_info.rootKey << endl
		<< "finetune = " << (int)m_info.fineTune << endl
		<< "lowkey = " << (int)m_info.lowKey << endl
		<< "highkey = " << (int)m_info.highKey << endl;
}


//void Sample::SetFolder(std::string folder)
//{
//	m_folder = folder;
//}
void Sample::SetRawFilename(std::string filename) noexcept
{
	m_rawFilename = filename;
}

void Sample::SetSampleIndex(int index) noexcept
{
	m_info.sampleIndex = static_cast<uint16_t>(index);
}

void Sample::SetSampleRate(int sampleRate) noexcept
{
	m_info.sampleRate = static_cast<uint16_t>(sampleRate);
}

void Sample::SetSampleLength(int sampleLength) noexcept
{
	m_info.sampleLength = static_cast<uint16_t>(sampleLength);
}

void Sample::SetLoopStart(int loopStart) noexcept
{
	m_info.loopStart = static_cast<uint16_t>(loopStart);
}

void Sample::SetLoopEnd(int loopEnd) noexcept
{
	m_info.loopEnd = static_cast<uint16_t>(loopEnd);
}


void Sample::SetRootKey(int rootKey) noexcept
{
	m_info.rootKey = static_cast<uint8_t>(rootKey);
}

void Sample::SetFineTune(int fineTune) noexcept
{
	m_info.fineTune = static_cast<uint8_t>(fineTune);
}

void Sample::SetLowKey(int lowKey) noexcept
{
	m_info.lowKey = static_cast<uint8_t>(lowKey);
}

void Sample::SetHighKey(int highKey) noexcept
{
	m_info.highKey = static_cast<uint8_t>(highKey);
}


std::string Sample::GetRawFilename() const noexcept
{
	return m_rawFilename;
}

int Sample::GetSampleIndex() const noexcept
{
	return m_info.sampleIndex;
}

int Sample::GetSampleRate() const noexcept
{
	return m_info.sampleRate;
}

int Sample::GetSampleLength() const noexcept
{
	return m_info.sampleLength;
}

int Sample::GetLoopStart() const noexcept
{
	return m_info.loopStart;
}

int Sample::GetLoopEnd() const noexcept
{
	return m_info.loopEnd;
}

int Sample::GetRootKey() const noexcept
{
	return m_info.rootKey;
}

int Sample::GetFineTune() const noexcept
{
	return m_info.fineTune;
}

int Sample::GetLowKey() const noexcept
{
	return m_info.lowKey;
}

int Sample::GetHighKey() const noexcept
{
	return m_info.highKey;
}

/** @brief	Determines if two samples have the same values.

	This function does not determine if two sample objects are the SAME sample object. 
	(That is, it does not tell if the pointer passed in points to this object.)

	@param[in] pOther	A native pointer to the other Sample object to compare with.

	@returns	true if all internal values of the sample are the same (including the raw filename).
*/
bool Sample::Equals(const Sample* pOther) const
{
	return
		m_rawFilename ==	pOther->m_rawFilename &&
		m_info.sampleIndex ==	pOther->m_info.sampleIndex &&
		m_info.sampleRate ==	pOther->m_info.sampleRate &&
		m_info.sampleLength ==	pOther->m_info.sampleLength &&
		m_info.loopStart ==		pOther->m_info.loopStart &&
		m_info.loopEnd ==		pOther->m_info.loopEnd &&
		m_info.rootKey ==		pOther->m_info.rootKey &&
		m_info.fineTune ==		pOther->m_info.fineTune &&
		m_info.lowKey ==		pOther->m_info.lowKey &&
		m_info.highKey ==		pOther->m_info.highKey;
}

SampleInfo Sample::GetInfo() const
{
	return m_info;
}


void Sample::SetInfo(const SampleInfo& info)
{
	m_info = info;
}

